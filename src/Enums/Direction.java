package Enums;

public enum Direction {
	
	NONE, NORTH, EAST, SOUTH, WEST, NORTH_WEST, NORTH_EAST;

}
