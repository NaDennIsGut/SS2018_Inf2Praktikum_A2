package Enums;

public enum CardType {
	MOVE_ONE, MOVE_TWO, MOVE_THREE, BACK_UP, ROTATE_LEFT, ROTATE_RIGHT, U_TURN;
}
