package Interface;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface iDataAccess {
	
	public void save(Object object, String path) throws FileNotFoundException, IOException;
	
	public Object load(String path) throws FileNotFoundException, IOException;

}
