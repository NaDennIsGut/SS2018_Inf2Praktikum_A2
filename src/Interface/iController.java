package Interface;

import java.util.ArrayList;

import Backend.Player;

/**
 * 
 * @author Leila Taraman
 *
 */

public interface iController {
	/**
	 * This method returns a List with everey single move a player has made since the last time he announced the end of programming.
	 * @return
	 * 			Example: ArrayList(ArrayList(PositionUpdate1(Player1:"PosX_PosY:ArchX_ArchY:Spawn:Direction | Player2:"PosX_PosY:ArchX_ArchY:Spawn:Direction),Update2...)))
	 */
	
	public ArrayList<String>getAllPlayerMovementLog();
	
	/**
	 * This method chooses a course from different courses and delivers a transfer parameter "name" of the type String
	 * @param name: String
	 * @return String
	 */
	public String chooseCourse(String name);
	
	
	/**
	 * This method chooses the program cards and delivers a transfer parameter "priorityNumber" of the type int
	 * @param priorityNumber
	 * @return boolean
	 */
	public boolean chooseProgramCard(int priorityNumber, int registerPhase);
	
	
	/**
	 * This method adds players to the game and delivers a transfer parameter "name" of the type String
	 * @param name: String
	 * @return boolean
	 */
	public boolean addPlayer(String name);
	
	
	/**
	 * This method determines the direction in which you will look, when you get spawned, 
	 * returns a boolean and delivers a transfer parameter "direction" of the type String
	 * @param direction: String
	 * @return boolean
	 */
	public boolean chooseFacingDirection(String direction);	

	
	/**
	 * This method starts the game and returns a boolean
	 * @return boolean
	 */
	public boolean startGame();
	
	
	/**
	 * This method announces the end of the programming phase and returns a boolean
	 * @return boolean
	 */
	public boolean announceEndOfProgramming();
	
	/**
	 * This method gives the information about name,  programCards-priority, programCards-type and lastTouchedFlags of the current player
	 * @return String
	 */
	public String getCurrentPlayerInfo();
	
	/**
	 * This method gives the information if a player want to power down or not
	 * @param decisionPowerDown: boolean
	 * @return boolean
	 */
	public boolean askForPowerDown(boolean decisionPowerDown);
	
	/**
	 * Load the game
	 * @param path: String
	 * @return: boolean - true: it was successful to load
	 */
	public boolean load(String path);
	
	/**
	 * Save the game
	 * @param path: String
	 * @return: boolean - true: it was successful to save
	 */
	public boolean save(String path);

	/**
	 * Creates a String containing the  player positions and , archive token positions , spawnNumber and faceDirection.
	 * @return String containing that info
	 */
	public String getAllPlayerMapInfo();
	
	/**
	 * returns the chosen board represented as a string
	 * @param board The board to be returned as a string 
	 * <p>(dockingBay = return the Docking Bay)</p>
	 * <p>(gameBoard = return the Game Board without the Docking Bay)</p>
	 * <p>(all = return all boards in one String separated by single characters)</p>
	 * @return String 
	 */
	public String getBoardString(String board);
	
	/**
	 * Selects the passed game mode.
	 * @param mode Name of the Game mode
	 * @return If the game mode was successfully selected
	 */
	public boolean selectGameMode(String mode);
	
	/**
	 * Set the number of ai players
	 * @param aiCount number of ai players
	 * @return how many ai players are selected
	 */
	public int setAIPlayers(int aiCount);
	

	/**
	 * This method actually returns the winning player
	 * @return
	 */
	
	public Player winningPlayer();
}
