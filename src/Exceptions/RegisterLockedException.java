package Exceptions;

public class RegisterLockedException extends Exception {
	public RegisterLockedException (String message) {
		super(message);
	}
}
