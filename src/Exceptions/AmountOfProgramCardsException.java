package Exceptions;

public class AmountOfProgramCardsException extends Exception {
	
	public AmountOfProgramCardsException(String message) {
		super (message);
	}
}
