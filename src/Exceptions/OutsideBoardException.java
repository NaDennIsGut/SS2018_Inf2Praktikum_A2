package Exceptions;

public class OutsideBoardException extends Exception {
	public OutsideBoardException(String message) {
		super(message);
	}
}
