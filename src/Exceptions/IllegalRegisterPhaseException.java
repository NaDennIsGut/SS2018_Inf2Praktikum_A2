package Exceptions;

public class IllegalRegisterPhaseException extends Exception {
	public IllegalRegisterPhaseException (String message) {
		super (message);
	}
}
