package Exceptions;

public class IllegalProgramSizeException extends Exception {
	public IllegalProgramSizeException (String message) {
		super(message);
	}

}
