package Exceptions;

public class IllegalProgramCardException extends Exception {
	public IllegalProgramCardException (String message) {
		super (message);
	}
}
