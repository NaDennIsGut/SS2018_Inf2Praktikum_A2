package Exceptions;

public class IllegalSpawnException extends Exception {
	public IllegalSpawnException (String message) {
		super(message);
	}
}
