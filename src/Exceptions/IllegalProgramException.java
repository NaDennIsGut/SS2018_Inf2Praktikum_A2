package Exceptions;

public class IllegalProgramException extends Exception {
	public IllegalProgramException (String message) {
		super (message);
	}
}
