package Exceptions;

public class IllegalPositionException extends Exception {
	public IllegalPositionException (String message) {
		super (message);
	}
}
