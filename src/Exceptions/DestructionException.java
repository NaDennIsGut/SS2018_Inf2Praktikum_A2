package Exceptions;

public class DestructionException extends Exception {
	public DestructionException (String message){
		super(message);
	}
}
