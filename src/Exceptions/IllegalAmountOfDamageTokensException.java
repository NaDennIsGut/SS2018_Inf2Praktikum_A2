package Exceptions;

public class IllegalAmountOfDamageTokensException extends Exception {
 public IllegalAmountOfDamageTokensException(String message) {
	 super(message);
 }
}
