package GUI;

import java.io.IOException;

import Frontend.Events;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class GameGUI extends Application{
	
	 

	public static void main(String[] args) {
		launch();
	}

	
	@Override
	public void start(Stage primaryStage){
		
		try {
			
			//----------Welcome Scene---------------
			BorderPane rootWelcome = FXMLLoader.load(getClass().getResource("WelcomeWindow.fxml"));			
			
			Scene welcome = new Scene(rootWelcome);

			primaryStage.setScene(welcome);
			primaryStage.setTitle("Robo Rally");
//			primaryStage.setMaximized(true);
			primaryStage.show();
			

		
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
