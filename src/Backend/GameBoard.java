package Backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;

import Enums.Direction;
import Enums.FieldType;
import Exceptions.IllegalParameterException;


/**
 * 
 * @author Leila Taraman, Arthur Huber
 * 
 *          
 *          Class GameBoard with inner class GameField. 
 */

public class GameBoard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GameField[][] gameboard = new GameField[12][12];
	private GameField[][] dockingbay = new GameField[12][4];
	private String courseName;
	
/** TODO
 * Fill and print GameBoard in Constructor
 * @throws IllegalParameterException 
 */	
	public GameBoard()  {
		setCourseName("DizzyDash");
		try {
			this.loadGameBoard("GameBoardCourseDizzyDash.csv");
		} catch (IllegalParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.loadDockingBay("DockingBayGameBoardCourseDizzyDash.csv");
		} catch (IllegalParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
/**
 * This method loads and splits the csv file
 * The several cases of the switchCase include all field types of the game board. The first letters of each field types and directions are shortcuts in the csv file.
 * 
 * @param path
 * @throws IllegalParameterException
 */
	public void loadGameBoard(String path) throws IllegalParameterException {
	        String line = "";
	        String cvsSplitBy = ",";
	        BufferedReader br = null;
	        int x,y = 0;
//	        int index = 0;
	        
	        try  {
	        	br = new BufferedReader(new FileReader(path));
	            while ((line = br.readLine()) != null) {
	            	x=0;
	                // use comma as separator
	                String[] zeile = line.split(cvsSplitBy);
	                for (String feld : zeile) { //test
	                	switch(feld) {  //boolean[] walls {north ->true, false, east ->true, false, south ->true, false, west ->true, false} ? int[]laser ? =>{north, east, south, west} 0 = false, 1 = true 
	                	case "E":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.NONE, new boolean[]{false,false,false,false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "WN":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.NORTH, new boolean[] {true, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "WE":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.EAST, new boolean[] {false, true, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "WS":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.SOUTH, new boolean[] {false, false, true, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "WW":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.WEST, new boolean[] {false, false, false, true}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBRRF":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST, Direction.EAST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBF":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_FAST, Direction.EAST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBFN":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_FAST, Direction.NORTH, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBFS":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_FAST, Direction.SOUTH, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBFW":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_FAST, Direction.WEST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBRRFN":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST, Direction.NORTH, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBRRFS":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST, Direction.SOUTH, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBRRFW":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST, Direction.WEST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "CBFLV":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_FAST, Direction.EAST, new boolean[] {false, false, false, false}, new int[]{1,0,0,0}, 0); //Doppel-FeldTyp => CB und Laser!!! Laser-Vertikal
	                		break;
	                	case "CBFWLV":
	                		this.gameboard[x][y] = new GameField(FieldType.CONVEYOR_BELT_FAST, Direction.WEST, new boolean[] {false, false, false, false}, new int[]{0,0,1,0}, 0); //Doppel-FeldTyp => CB und Laser!!! Laser-Vertikal
	                		break;
	                	case "GRR":
	                		this.gameboard[x][y] = new GameField(FieldType.GEAR_ROT_RIGHT, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "GRL":
	                		this.gameboard[x][y] = new GameField(FieldType.GEAR_ROT_LEFT, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "GRRLV":      	
	                		this.gameboard[x][y] = new GameField(FieldType.GEAR_ROT_RIGHT, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{1,0,0,0}, 0);//Doppel-FeldTyp=>Gear und Laser!!! Laser-Vertikal
	                		break;
	                	case "GRRLVS":
	                		this.gameboard[x][y] = new GameField(FieldType.GEAR_ROT_RIGHT, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,1,0}, 0);//Doppel-FeldTyp=>Gear und Laser!!! Laser-Vertikal
                		    break;
//	                	case "WSGRRLV":
//	                		this.gameboard[x][y] = new GameField(FieldType.GEAR_ROT_RIGHT_LASER_V, Direction.SOUTH, new boolean[] {false, false, true, false}, new int[]{0,0,0,0}, index);//Doppel-FeldTyp=>Gear und Laser!!! Laser-Vertikal
//	                		break;
	                	case "WSLV":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.SOUTH, new boolean[] {false, false, true, false}, new int[]{0,0,1,0}, 0); 
	                		break;
	                	case "WWLH":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.WEST, new boolean[] {false, false, false, true}, new int[]{0,0,0,1}, 0);
	                		break;
	                	case "WELH":      	
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.EAST, new boolean[] {false, true, false, false}, new int[]{0,1,0,0}, 0);
	                		break;
	                	case "WNLV":      	
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.NORTH, new boolean[] {true, false, false, false}, new int[]{1,0,0,0}, 0);
	                		break;
	                	case "RO":
	                		this.gameboard[x][y] = new GameField(FieldType.REPAIR_ONE, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "RT":
	                		this.gameboard[x][y] = new GameField(FieldType.REPAIR_TWO, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);
	                		break;
	                	case "LVN":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{1,0,0,0}, 0);
	                		break;
	                	case "LVS":
	                		this.gameboard[x][y] = new GameField(FieldType.EMPTY, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,1,0}, 0);
	                		break;
	                	case "F1":
	                		this.gameboard[x][y] = new GameField(FieldType.FLAG, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 1);
	                		break;
	                	case "F2":
	                		this.gameboard[x][y] = new GameField(FieldType.FLAG, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 2);
	                		break;
	                	case "F3":
	                		this.gameboard[x][y] = new GameField(FieldType.FLAG, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 3);
	                		break;
//	                	case "S":
//	                		this.gameboard[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, index);
//	                		break;
//	                	default:
//	                		throw new RuntimeException("Field is not available");
	                	}
	                	x++;
//	                	index++;		   
	                }
	                y++;
	            }

	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	        	if(br != null) {
	        		try {
	        			br.close();
	        		} catch (IOException e){
	        			e.printStackTrace();
	        		}
	        	}
	        }
	}
	
	
	/**
	 * This method loads and splits the csv file 
	 * The several cases of the switchCase include all field types of the DockingBay. The first letters of each field
	 * types and directions are shortcuts in the csv file.
	 * 
	 * @param path
	 * @throws IllegalParameterException
	 */
	public void loadDockingBay(String path) throws IllegalParameterException {
		String line = "";
		String cvsSplitBy = ",";
		BufferedReader br = null;
		int x, y = 0;
//		int index = 0;

		try {
			br = new BufferedReader(new FileReader(path));
			while ((line = br.readLine()) != null) {
				x = 0;
				// use comma as separator
				String zeile[] = line.split(cvsSplitBy);
				for (String feld : zeile) {
					switch(feld) {  //boolean[] walls {north -> true, false, east ->true, false, south ->true, false, west ->true, false} ? int[]laser ? =>{north, east, south, west} 0 = false, 1 = true 
                	case "E":
                		this.dockingbay[x][y] = new GameField(FieldType.EMPTY, Direction.NONE, new boolean[]{false,false,false,false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "WN":
                		this.dockingbay[x][y] = new GameField(FieldType.EMPTY, Direction.NORTH, new boolean[] {true, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "WW":
                		this.dockingbay[x][y] = new GameField(FieldType.EMPTY, Direction.WEST, new boolean[] {false, false, false, true}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "CBS":
                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT, Direction.EAST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "CBSW":
                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT, Direction.WEST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "CBRRSW":
                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_RIGHT, Direction.WEST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "CBRRS":
                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_RIGHT, Direction.SOUTH, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "CBRLS":
                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_LEFT, Direction.SOUTH, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
                	case "CBRLSE":
                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_LEFT, Direction.EAST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
                		break;
//                	case "CBRLSW":
//                		this.dockingbay[x][y] = new GameField(FieldType.CONVEYOR_BELT_ROT_LEFT, Direction.WEST, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 0);//
//                		break;
                	case "WNW":
                		this.dockingbay[x][y] = new GameField(FieldType.EMPTY, Direction.NORTH, new boolean[] {true, false, false, true}, new int[]{0,0,0,0}, 0); //
                		break;
                	case "WNE":
                		this.dockingbay[x][y] = new GameField(FieldType.EMPTY, Direction.NORTH, new boolean[] {true, true, false, false}, new int[]{0,0,0,0}, 0);
                		break;
                	case "S1":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 1);
                		break;
                	case "WWS2":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, true}, new int[]{0,0,0,0}, 2);
                		break;
                	case "S3":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 3);
                		break;
                	case "S4":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 4);
                		break;
                	case "WWS5":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.WEST, new boolean[] {false, false, false, true}, new int[]{0,0,0,0}, 5);
                		break;	
                	case "WWS6":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.WEST, new boolean[] {false, false, false, true}, new int[]{0,0,0,0}, 6);
                		break;
                	case "S7":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 7);
                		break;
                	case "S8":
                		this.dockingbay[x][y] = new GameField(FieldType.SPAWN, Direction.NONE, new boolean[] {false, false, false, false}, new int[]{0,0,0,0}, 8);
                		break;
                	
//                	default:
//                		throw new RuntimeException("Field is not available");
                	}
					x++;
//					index++;
				}
				y++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
        		try {
        			br.close();
        		} catch (IOException e){
        			e.printStackTrace();
        		}
		   }
		}

	}
	
	
	public GameField[][] getGameboard() {
		return gameboard;
	}

	public void setGameboard(GameField[][] gameboard) {
		this.gameboard = gameboard;
	}
	
	public GameField[][] getDockingbay() {
		return gameboard;
	}

	public void setDockingbay(GameField[][] dockingbay) {
		this.dockingbay = dockingbay;
	}
	
	
	/**
	 * Returns the type of a field
	 * 
	 * @param x Coordinate
	 * @param y	Coordinate
	 * @param getFromDockingBay decide from which board the information comes
	 * @return The FieldType of the GameField at the given coordinates
	 * @throws IndexOutOfBoundsException if invalid coordinates are given
	 */
	public FieldType fieldTypeAt(int x, int y, boolean getFromDockingBay) throws IndexOutOfBoundsException{
		if(getFromDockingBay) {
			if(x < 0 || y < 0 || x >= dockingbay.length || y >= dockingbay[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return dockingbay[x][y].getType();
		} else {
			if(x < 0 || y < 0 || x >= gameboard.length || y >= gameboard[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return gameboard[x][y].getType();
		}
		
	}
	
	/**
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 * @param type of the field
	 * @return true if no exception was thrown
	 * @throws IndexOutOfBoundsException if either coordinate is bigger than 11 or smaller than 0
	 * @throws NullPointerException if the type enum is null
	 */
	public boolean setFieldTypeAt(int x, int y, FieldType type) throws IndexOutOfBoundsException, NullPointerException{
		if(x<0 || y<0 || x>11 || y>11) {
			throw new IndexOutOfBoundsException("The field at " + x + "/" + y + " doesn't exist!");
		}
		if(type == null) {
			throw new NullPointerException("The received Field Type is null!");
		}
		gameboard[x][y].setType(type);
		return true;
	}
	
	/**
	 * Returns the pusher direction of a field
	 * 
	 * @param x Coordinate
	 * @param y Coordinate
	 * @param getFromDockingBay decide from which board the information comes
	 * @return The Direction of the pusher on a GameField at the given coordinates
	 * @throws IndexOutOfBoundsException if invalid coordinates are given
	 */
	public Direction fieldDirectionAt(int x, int y, boolean getFromDockingBay)  throws IndexOutOfBoundsException{
		if(getFromDockingBay) {
			if(x < 0 || y < 0 || x >= dockingbay.length || y >= dockingbay[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return dockingbay[x][y].getDirection();
		} else {
			if(x < 0 || y < 0 || x >= gameboard.length || y >= gameboard[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return gameboard[x][y].getDirection();
		}
	
	}
	
	/**
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 * @param direction of the field
	 * @return true if no exception was thrown
	 * @throws IndexOutOfBoundsException if either coordinate is bigger than 11 or smaller than 0
	 * @throws NullPointerException if the direction enum is null
	 */
	public boolean setFieldDirectionAt(int x, int y, Direction direction)  throws IndexOutOfBoundsException, NullPointerException{
		if(x<0 || y<0 || x>11 || y>11) {
			throw new IndexOutOfBoundsException("The field at " + x + "/" + y + " doesn't exist!");
		}
		if(direction == null) {
			throw new NullPointerException("The received Direction is null!");
		}
		gameboard[x][y].setDirection(direction);
		return true;
	}
	
	
	/**
	 * Returns the wall array of a field
	 * 
	 * @param x Coordinate
	 * @param y Coordinate
	 * @param getFromDockingBay decide from which board the information comes
	 * @return A boolean array of the walls of the GameField at the given coordinates
	 * @throws IndexOutOfBoundsException if invalid coordinates are given
	 */
	public boolean[] fieldWallsAt(int x, int y, boolean getFromDockingBay)  throws IndexOutOfBoundsException{
		if(getFromDockingBay) {
			if(x < 0 || y < 0 || x >= dockingbay.length || y >= dockingbay[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return dockingbay[x][y].getWalls();
		} else {
			if(x < 0 || y < 0 || x >= gameboard.length || y >= gameboard[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return gameboard[x][y].getWalls();
		}
		
	}
	
	/**
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 * @param walls of the field
	 * @return true if no exception was thrown
	 * @throws IndexOutOfBoundsException if either coordinate is bigger than 11 or smaller than 0
	 * @throws NullPointerException if the wall array is null
	 * @throws IllegalArgumentException if the length of the wall array length is not 4
	 */
	public boolean setFieldWallsAt(int x, int y, boolean[] walls)  throws IndexOutOfBoundsException, NullPointerException, IllegalArgumentException{
		if(x<0 || y<0 || x>11 || y>11) {
			throw new IndexOutOfBoundsException("The field at " + x + "/" + y + " doesn't exist!");
		}
		if(walls == null) {
			throw new NullPointerException("The received walls array is null!");
		}
		if(walls.length != 4) {
			throw new IllegalArgumentException("The received wall array has a length of " + walls.length + ". but only a length of 4 is allowed!");
		}
		gameboard[x][y].setWalls(walls);
		return true;
	}
	
	/**
	 * Returns the laser array of a field
	 * 
	 * @param x Coordinate
	 * @param y Coordinate
	 * @param getFromDockingBay decide from which board the information comes
	 * @return An int array of the lasers of the GameField at the given coordinates
	 * @throws IndexOutOfBoundsException if invalid coordinates are given
	 */
	public int[] fieldLasersAt(int x, int y, boolean getFromDockingBay)  throws IndexOutOfBoundsException{
		if(getFromDockingBay) {
			if(x < 0 || y < 0 || x >= dockingbay.length || y >= dockingbay[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return dockingbay[x][y].getLasers();
		} else {
			if(x < 0 || y < 0 || x >= gameboard.length || y >= gameboard[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return gameboard[x][y].getLasers();
		}
	}
	
	/**
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 * @param lasers of the field
	 * @return true if no exception was thrown
	 * @throws IndexOutOfBoundsException if either coordinate is bigger than 11 or smaller than 0
	 * @throws NullPointerException if the wall array is null
	 * @throws IllegalArgumentException if the length of the wall array length is not 4
	 */
	public boolean setFieldLasersAt(int x, int y, int[] lasers)  throws IndexOutOfBoundsException, NullPointerException, IllegalArgumentException{
		if(x<0 || y<0 || x>11 || y>11) {
			throw new IndexOutOfBoundsException("The field at " + x + "/" + y + " doesn't exist!");
		}
		if(lasers == null) {
			throw new NullPointerException("The received lasers array is null!");
		}
		if(lasers.length != 4) {
			throw new IllegalArgumentException("The received lasers array has a length of " + lasers.length + ". but only a length of 4 is allowed!");
		}
		gameboard[x][y].setLasers(lasers);
		return true;
	}
	
	/**
	 * Returns the index of a field
	 * 
	 * @param x Coordinate
	 * @param y Coordinate
	 * @param getFromDockingBay decide from which board the information comes
	 * @return The index of the GameField at the given coordinates
	 * @throws IndexOutOfBoundsException if invalid coordinates are given
	 */
	public int fieldIndexAt(int x, int y, boolean getFromDockingBay)  throws IndexOutOfBoundsException{
		if(getFromDockingBay) {
			if(x < 0 || y < 0 || x >= dockingbay.length || y >= dockingbay[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return dockingbay[x][y].getIndex();
		} else {
			if(x < 0 || y < 0 || x >= gameboard.length || y >= gameboard[x].length) {
				throw new IndexOutOfBoundsException("The field at x=" + x + " y=" + y + " doesn't exist!");
			}
			return gameboard[x][y].getIndex();
		}
		
	}

	/**
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 * @param index of the field
	 * @return true if no exception was thrown
	 * @throws IndexOutOfBoundsException if either coordinate is bigger than 11 or smaller than 0
	 */
	public boolean setFieldLasersAt(int x, int y, int index)  throws IndexOutOfBoundsException{
		if(x<0 || y<0 || x>11 || y>11) {
			throw new IndexOutOfBoundsException("The field at " + x + "/" + y + " doesn't exist!");
		}
		gameboard[x][y].setIndex(index);
		return true;
	}
	
	/**
	 * This method gives spawnPosition of the spawnPointNumbers from the docking bay
	 * @param spawnPointNumber
	 * @return int[] spawnPos
	 */
	public int[] getSpawnPos(int spawnPointNumber) {
		int[] spawnPos = new int[2];
		
		for(int x = 0; x < dockingbay.length; x++) {
			for(int y = 0; y < dockingbay[x].length; y++ ) {
				if(dockingbay[x][y].getType().equals(FieldType.SPAWN) && dockingbay[x][y].getIndex() == spawnPointNumber) {
					spawnPos[0] = x;
					spawnPos[1] = y;
					 return spawnPos;
				}
			}
			
		}
		
		return spawnPos;
		
	}

	
	/**
	 * Combines the information of all fields in the chosen board
	 * @return String
	 */
	public String getBoardInfo(boolean getFromDockingBay) {
		String retString = "";
		char[] separators = {':'};
		GameField[][] targetField ;
		
		if(getFromDockingBay) {
			targetField = dockingbay;
		} else {
			targetField = gameboard;
		}
		
		for(int x=0; x<targetField.length; x++) {
			for(int y=0; y<targetField[0].length; y++) {
				retString += targetField[x][y].getInfo() + separators[0];
			}
		}
		return retString;
	}
	
	/**
	 * Combines the output from getBoardInfo for all boards into one String
	 * @return String
	 */
	public String getAllBoardInfo() {
		String retString = "";
		char[] separator = {';'};
		retString += getBoardInfo(false) + separator[0];
		retString += getBoardInfo(true) + separator[0];
		return retString;
	}
	
	
	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}


	private class GameField implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
		 * Class GameField - is a composition as inner class from GameBoard
		 */

		private FieldType type;
		private Direction direction;
		private boolean[] walls = new boolean[4];
		private int[] lasers = new int[4];
		private int index;
		
		/**
		 * Create a new GameField object.
		 * If an attribute isn't needed for certain field types a default value can be passed.
		 * Parameters for optional unused Attributes:
		 * Direction: NONE
		 * flagNumber, spawnNumber: 0
		 * @param type
		 * @param direction
		 * @param pusherDirection
		 * @param walls
		 * @param lasers
		 * @param flagNumber
		 * @param spawnNumber
		 * @throws NullPointerException, IllegalArgumentException. passes the Exceptions from the setters
		 */
		public GameField(FieldType type, Direction direction, boolean[] walls, int [] lasers, int index) throws NullPointerException, IllegalArgumentException{
			setType(type);
			setDirection(direction);
			setWalls(walls);
			setLasers(lasers);
			setIndex(index);
		}

		public FieldType getType() {
			return type;
		}

		public Direction getDirection() {
			return direction;
		}

		public boolean[] getWalls() {
			return walls;
		}

		public int[] getLasers() {
			return lasers;
		}

		public int getIndex() {
			return index;
		}

		/**
		 * @param type
		 * @throws NullPointerException if the passed type is null.
		 */
		public void setType(FieldType type) throws NullPointerException{
			if(type == null) {
				throw new NullPointerException("The received field type is null!");
			}
			this.type = type;
		}

		/**
		 * Only returns true if attribute was set
		 * @param direction
		 * @throws NullPointerException if the passed direction is null
		 */
		public void setDirection(Direction direction) throws NullPointerException {
			if(direction == null) {
				throw new NullPointerException("The received field direction is null!");
			}
			this.direction = direction;
		}

		/**
		 * @param walls
		 * @throws NullPointerException if the passed array is null.
		 * @throws IllegalArgumentException if the passed array doesn't have a size of 4.
		 */
		public void setWalls(boolean[] walls) throws NullPointerException, IllegalArgumentException {
			if(walls == null) {
				throw new NullPointerException("The received wall array is null!");
			}
			if(walls.length != 4) {
				throw new IllegalArgumentException("The received wall array doesn't have a length of 4!");
			}
			this.walls = walls;
		}

		/**
		 * @param lasers
		 * @throws NullPointerException if the passed array is null.
		 * @throws IllegalArgumentException if the passed array doesn't have a size of 4.
		 */
		public void setLasers(int[] lasers) throws NullPointerException, IllegalArgumentException {
			if(lasers == null) {
				throw new NullPointerException("The received laser array is null!");
			}
			if(lasers.length != 4) {
				throw new IllegalArgumentException("The received laser array doesn't have a length of 4!");
			}
			this.lasers = lasers;
		}

		public void setIndex(int index) {
			this.index = index;
		}
	
		/**
		 * Combines all Attributes into a String and returns it
		 * <p>ORDER: type - direction - walls - lasers - index </p>
		 * @return String representation of this field
		 */
		public String getInfo() {
			String retString = "";
			//Lowest index is lowest separator level
			char[] separators = {',', '.'};
			
			retString += type.toString() + separators[0];
			
			retString += direction.toString() + separators[0];
			
			for(int i=0; i<walls.length; i++) {
				retString += walls[i];
				retString += separators[1];
			}
			retString += separators[0];
			
			for(int i=0; i<lasers.length; i++) {
				retString += lasers[i];
				retString += separators[1];
			}
			retString += separators[0];
			
			retString += index;
			
			return retString;
		}
	}
}
