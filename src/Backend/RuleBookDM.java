package Backend;

import java.util.ArrayList;

import Enums.FieldType;
import Exceptions.GameOverException;
import Exceptions.IllegalSpawnException;

public class RuleBookDM extends RuleBook {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean checkGameOver(GameBoard gb, ArrayList<Player>playerlist) throws NullPointerException  {
		boolean gameOver = false;

		
		// check if only one player is left in the game
		if (playerlist.size() == 1) {
			return gameOver = true;
		}

		return gameOver;

	}
	
	@Override
	public boolean checkFlagTouch(Player player, GameBoard gb) throws GameOverException, NullPointerException {
		return false;
	}
	
	@Override
	public boolean checkSpawn(ArrayList<Player> playerlist, GameBoard gb)
			throws NullPointerException, IllegalSpawnException {
		boolean correct = false;

		// chek for null
		for (Player player : playerlist) {
			if (player.equals(null)) {
				throw new NullPointerException("Given list of players contains a null object");
			}
		}

		// check spawn

		int x, y;

		for (Player player : playerlist) {
			x = player.getPos()[0];
			y = player.getPos()[1];

			if (gb.fieldTypeAt(x, y, false).equals(FieldType.PIT)) {
				correct = false;
//				if (gb.fieldTypeAt(x+1, y, false) != FieldType.PIT) {
//					player.setPos(x+1, y);
//					correct = true;
//				} else if (gb.fieldTypeAt(x, y+1, false) != FieldType.PIT) {
//					player.setPos(x, y+1);
//					correct = true;
//				} else if (gb.fieldTypeAt(x-1, y, false) != FieldType.PIT) {
//					player.setPos(x-1, y);
//					correct = true;
//				} else if (gb.fieldTypeAt(x, y-1, false) != FieldType.PIT) {
//					player.setPos(x, y-1);
//					correct = true;
//				} else {
//					throw new IllegalSpawnException(player.getName() + " is on a wrong field");
//				}
			} else {
				correct = true;
			}
		}

		return correct;

	}

}
