package Backend;

import java.util.ArrayList;


import Enums.CardType;
import Enums.Direction;
import Exceptions.BotException;
import Interface.iController;
import javafx.scene.image.ImageView;
/**
 * Bot Class
 * Has additional functionality to play the game without human input.
 * @author Arthur Huber
 */
public class Bot extends Player {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maps;
	private ArrayList<ProgramCard> programResult;
	private ArrayList<ProgramCard> tempHand;
	private iController gm;
	
	public Bot(int spawnNumber, int lifeTokens, int posX, int posY, String name, Direction faceDirection,
			String activeMap) throws NullPointerException, IllegalArgumentException {
		super(spawnNumber, lifeTokens, posX, posY, name, faceDirection, activeMap);
		
	}
	

	public Bot(int spawnNumber, int lifeTokens, int posX, int posY, String name, Direction faceDirection,
			String activeMap, iController gm) throws NullPointerException, IllegalArgumentException {
		this(spawnNumber, lifeTokens, posX, posY, name, faceDirection, activeMap);
		this.gm = gm;
		maps = gm.getBoardString("all");
	}
	
	public boolean runRoutine(int allowedCards) throws BotException{
		
		
		programResult = new ArrayList<ProgramCard>();
		tempHand = new ArrayList<ProgramCard>();
		tempHand.addAll(programCards);
		
		if(createProgram(allowedCards, pos[0], pos[1], faceDirection, determineTarget())) {
			for(ProgramCard card : programResult) {
				gm.chooseProgramCard(programCards.indexOf(card), programResult.indexOf(card));
			}
			return true;
		}
		return false;
			
	}
	
public boolean debugRunRoutine(int allowedCards) throws BotException{
		
		
		programResult = new ArrayList<ProgramCard>();
		tempHand = new ArrayList<ProgramCard>();
		tempHand.addAll(programCards);
		
		if(createProgram(allowedCards, pos[0], pos[1], faceDirection, determineTarget())) {
//			for(ProgramCard card : programResult) {
//				gm.chooseProgramCard(programCards.indexOf(card), programResult.indexOf(card));
//			}
			return true;
		}
		return false;
			
	}
	
	//This probably won't be the public accessible one because of excess parameters
	/**
	 * When called the bot will try to create a program that will 
	 * @param allowedCards How many cards the bot is allowed to play
	 * @return The program array that the bot created with the first index being the first card to play. 
	 */
	private boolean createProgram(int allowedCards, int posX, int posY, Direction rot, int[] targetPos) throws BotException{
		//Var setup
		int[] expectedCoords = {posX, posY};
		Direction expectedRot = rot;
		
		int targetDistXSigned = targetPos[0] - expectedCoords[0];
		int targetDistYSigned = targetPos[1] - expectedCoords[1];
		
		
		///Default selection
		char targetAxis = 'x';
		int targetDistance = targetDistXSigned;
		
		if(allowedCards == 0) {
			//All cards have been played. Hurray!
			debugPrintResultMessage(targetPos, expectedCoords, expectedRot);
			
			return true;
		} else {
			
			
			//To get the correct SIGNED distance always do (target position) - (our position)
			
			//FIND SHORTEST AXIS DISTANCE
			
			//If x axis has smaller distance and is not 0
			if(Math.abs(targetDistXSigned) < Math.abs(targetDistYSigned) && targetDistXSigned != 0) {
				targetAxis = 'x';
				targetDistance  = targetDistXSigned;
				
			//If y axis has smaller distance and is not 0
			} else if(Math.abs(targetDistXSigned) < Math.abs(targetDistYSigned) && targetDistYSigned != 0) {
				targetAxis = 'y';
				targetDistance  = targetDistYSigned;
				
				//If the distances in both axis are 0 then waste a card
			} else if(targetDistXSigned == 0 && targetDistYSigned == 0){ 
				
				ProgramCard wasteCard = wasteTurn();
				if(wasteCard != null) {
					programResult.add(wasteCard);
					tempHand.remove(wasteCard);
					//Update expected Bot position and rotation
					expectedCoords = updateExpectedPos(wasteCard, expectedCoords[0], expectedCoords[1], expectedRot);
					expectedRot = updateExpectedRot(wasteCard, expectedRot);
					
					if(createProgram(allowedCards - 1, expectedCoords[0], expectedCoords[1], expectedRot, targetPos)) {
						return true;
					}
				} else { //If we haven't played a waste card but still have to play cards then something is fishy. Throw an exception.
					throw new BotException("I can't play any cards! This is not supposed to happen!");
				}
		
			} else {//If both are equal and not 0 just choose x as default
				targetAxis = 'x';
				targetDistance  = targetDistXSigned;
			}
			//Now we know in which direction to go and how far
			
			
			
			//Figure out if and how much we have to rotate
			int rotateThisMuch = determineRotationDifference(targetDistance, targetAxis, expectedRot);
			
			
			//Do the rotation and add the card to the result
			ProgramCard turnCard = makeTurn(rotateThisMuch);
			if(turnCard != null) {
				
				programResult.add(turnCard);
				tempHand.remove(turnCard);
				//This is wrong. We can't simply return... I think..
				
				//Update expected Bot position and rotation
				expectedCoords = updateExpectedPos(turnCard, expectedCoords[0], expectedCoords[1], expectedRot);
				expectedRot = updateExpectedRot(turnCard, expectedRot);
				
				if(createProgram(allowedCards - 1, posX, posY, expectedRot, targetPos)) {
					return true;
				}
			} else if(rotateThisMuch == 0){ //If we haven't played a turn card because we don't need to then play movement cards
				//call makeMove with the absolute value of the distance because we don't care about negative numbers
				
				
				ProgramCard moveCard = makeMove(Math.abs(targetDistance));
				
				if(moveCard != null) { //If we can play a movement card then go further into the recursion
					
					//*************************LOOK FOR OBSTACLES***********************
					
					//"If there is an obstacle, try to rotate right and move OR rotate left and move"
					//(Right now only walls are seen as obstacles)
					
					//Can we move at least one field?
					if(lookUpWalls(expectedCoords, expectedRot, 1) == 0) {
						
						//Have rotate right card? //Wall right?
						ProgramCard evadeRotCard = findCardAfterType(CardType.ROTATE_RIGHT);
						Direction tempExpectedRot = updateExpectedRot(evadeRotCard, expectedRot);
						if( evadeRotCard != null && lookUpWalls(expectedCoords, tempExpectedRot, 1) > 0) {
							
								//Play Card
								programResult.add(evadeRotCard);
								tempHand.remove(evadeRotCard);
								expectedRot = updateExpectedRot(evadeRotCard, expectedRot);
								allowedCards--;
							
						} else if (findCardAfterType(CardType.ROTATE_LEFT) != null && lookUpWalls(expectedCoords, updateExpectedRot(findCardAfterType(CardType.ROTATE_LEFT), expectedRot), 1) > 0){ //Have rot left card?
							//No rot right card
							evadeRotCard = findCardAfterType(CardType.ROTATE_LEFT);
							//Wall left?
							tempExpectedRot = updateExpectedRot(evadeRotCard, expectedRot);
							
								//Play Card
								programResult.add(evadeRotCard);
								tempHand.remove(evadeRotCard);
								expectedRot = updateExpectedRot(evadeRotCard, expectedRot);
								allowedCards--;
							
						} else { //Can't evade for whatever reason: Waste a card
							ProgramCard wasteCard = wasteTurn();
							if(wasteCard != null) {
								programResult.add(wasteCard);
								tempHand.remove(wasteCard);
								//Update expected Bot position and rotation
								expectedCoords = updateExpectedPos(turnCard, expectedCoords[0], expectedCoords[1], expectedRot);
								expectedRot = updateExpectedRot(turnCard, expectedRot);
								
								if(createProgram(allowedCards - 1, expectedCoords[0], expectedCoords[1], expectedRot, targetPos)) {
									return true;
								}
							} else { //If we haven't played a waste card but still have to play cards then something is fishy. Throw an exeption.
								throw new BotException("I can not play ANY cards! This is not supposed to happen!");
							}
							
						}
						
					}
					//This check is only for evasion
					if(allowedCards >= 1) {
						
						programResult.add(moveCard);
						tempHand.remove(moveCard);
						//Update expected Bot position and rotation
						expectedCoords = updateExpectedPos(moveCard, expectedCoords[0], expectedCoords[1], expectedRot);
						expectedRot = updateExpectedRot(moveCard, expectedRot);
						
						if(createProgram(allowedCards - 1, expectedCoords[0], expectedCoords[1], expectedRot, targetPos)) {
							return true;
						}
					} else {
						//All cards are played because of evasion
						debugPrintResultMessage(targetPos, expectedCoords, expectedRot);
						return true;
					}
				} else if(targetDistance == 0){ //If we haven't played a card and the target distance has been closed then this axis is finished. We should change the axis
					
				} else { //If we can't play a card but we haven't closed the distance  then we have to try to waste a card
					ProgramCard wasteCard = wasteTurn();
					if(wasteCard != null) {
						programResult.add(wasteCard);
						tempHand.remove(wasteCard);
						//Update expected Bot position and rotation
						expectedCoords = updateExpectedPos(turnCard, expectedCoords[0], expectedCoords[1], expectedRot);
						expectedRot = updateExpectedRot(turnCard, expectedRot);
						
						if(createProgram(allowedCards - 1, expectedCoords[0], expectedCoords[1], expectedRot, targetPos)) {
							return true;
						}
					} else { //If we haven't played a waste card but still have to play cards then something is fishy. Throw an exeption.
						throw new BotException("I can not play ANY cards! This is not supposed to happen!");
					}
				}
			} else { //If we haven't played a turn card but we still have to turn then waste a card.
				
				//TODO this may not be a permanent solution.
				ProgramCard wasteCard = wasteTurn();
				if(wasteCard != null) {
					programResult.add(wasteCard);
					tempHand.remove(wasteCard);
					//Update expected Bot position and rotation
					expectedCoords = updateExpectedPos(wasteCard, expectedCoords[0], expectedCoords[1], expectedRot);
					expectedRot = updateExpectedRot(turnCard, expectedRot);
					
					if(createProgram(allowedCards - 1, expectedCoords[0], expectedCoords[1], expectedRot, targetPos)) {
						return true;
					}
				} else { //If we haven't played a waste card but still have to play cards then something is fishy. Throw an exception.
					throw new BotException("I can't play any cards! This is not supposed to happen!");
				}
			}
			
			
		}
		System.out.println("Should we be able to reach this last return????");
		return false;
	}
	
	/**
	 * Searches through the map to find the position of the next flag the bot has to touch. This will only search on Game Board, NOT Docking Bay.
	 * @return An array of size 2 that contains the coordinates of the flag. returns null if the the next Flag can't be found.
	 */
	private int[] findNextFlagPosition() {
		//The are no Flag in Docking Bay so we are only searching in Game Board
		
		//Determine next Flag number
		int targetFlag = getLastTouchedFlag()+1;
		
		
		String gameBoardString = maps.split(";")[0];
		String[] fields = gameBoardString.split(":");
		
		for(int x=0; x<12; x++) {
			for(int y=0; y<12; y++) {
				//ORDER: type - direction - walls - lasers - index
				String singleField[] = fields[(y*12)+x].split(",");
				if(singleField[0].equals("FLAG") && singleField[4].equals(""+targetFlag)) {
					int[] retCoords = {y,x};
					return retCoords;
				}
			}
		}
		return null;
	}
	
	/**
	 * Based on the bots rotation and the parameter this method finds out how much the bot has to turn and in what direction (cw or ccw)
	 * @param distance Used to decide if the bot  has to go in positive or negative direction of the axis.
	 * @param axis Which axis the bot wants to go in. This parameter can only be 'x' or 'y'. 
	 * @return An int representing how many times the bot has to turn in a certain direction. Negative numbers mean counter-clock-wise(ccw), positive clock-wise(cw), o not rotation.
	 * @throws IllegalArgumentException If the passed axis is anything else than x or y
	 * <br> <br>
	 * Examples: <br> -1 := 90� ccw <br>
	 * 0 := no turning <br>
	 * 2 := 180 cw
	 */
	private int determineRotationDifference(int distance, char axis, Direction rot) throws IllegalArgumentException{
		//We don't need to turn if we don't have to walk a distance
		if(distance == 0) {
			return 0;
		}
		Direction targetDirection;
		
		switch(axis) {
		//Either East or West
		case 'x':
			if(distance > 0) {
				targetDirection = Direction.EAST;
			} else {
				targetDirection = Direction.WEST;
			}
			break;
			
		//Either North or South
		case 'y':
			if(distance > 0) {
				targetDirection = Direction.SOUTH;
			} else {
				targetDirection = Direction.NORTH;
			}
			break;
		default:
			targetDirection = Direction.NONE;
			throw new IllegalArgumentException("I received an invalid axis!");
		}
		
		//Now we know in which direction we want to go
		//We have to figure out the difference 
		
		//Trying to be clever and work with enum ordinals...
		
		//Special cases
		if(rot.ordinal() == targetDirection.ordinal()) {
			return 0;
		} else if(Math.abs((rot.ordinal()-1) - (targetDirection.ordinal()-1)) == 2) {
			return 2;
		} else if((rot.ordinal()-1) - (targetDirection.ordinal()-1) < 2) {
			return -1;
		} else {
			return 1;
		}
	}
	
	//TODO Correct javadoc for recursiveness
	/**
	 * Looks through the bots hand and tries to choose fitting cards to turn the bot correctly according to the received parameter. The method will at least try to get as close as possible to the right result.
	 * @param turnDirections Decides how to turn. 0 means no turn, negative numbers mean counter-clock-wise rotation, positive numbers mean clock-wise-rotation.
	 * @param allowedCards How many cards the method is allowed to play.
	 * @return An array of program cards that tries to correctly fulfill the rotation. Returns null if no rotation is needed OR if no rotation cards are available. 
	 */
	private ProgramCard makeTurn(int turnDirection) {
		if(turnDirection == 0) {	//No rotation
			return null;
		} else if(turnDirection % 2 == 0) {	//U-Turn
			return findCardAfterType(CardType.U_TURN);
		} else if(turnDirection < 0) {	//Counter-clock-wise
			return findCardAfterType(CardType.ROTATE_LEFT);
		} else if(turnDirection > 0) { //Clock-wise
			return findCardAfterType(CardType.ROTATE_RIGHT);
		} 
		return null;
	}
	
	//TODO Correct javadoc for recursiveness
	/**
	 * Tries to play to play moving program cards in the bot's hand to close the received distance as much as possible while not overshooting.
	 * @param distance The distance that has to be closed.
	 * @param allowedCards How many cards the method is allowed to play.
	 * @return The cards the method has determined are best to play. This can be less cards than are needed to play and even null if the method can't find any cards to play.
	 */
	private ProgramCard makeMove(int distance) {
		if(distance > 2) {
			if(findCardAfterType(CardType.MOVE_THREE) != null) {
				return findCardAfterType(CardType.MOVE_THREE);
			} 
			if(findCardAfterType(CardType.MOVE_TWO) != null) {
				return findCardAfterType(CardType.MOVE_TWO);
			}
			return findCardAfterType(CardType.MOVE_ONE);
		}
		if(distance == 2) {
			if(findCardAfterType(CardType.MOVE_TWO) != null) {
				return findCardAfterType(CardType.MOVE_TWO);
			}
			return findCardAfterType(CardType.MOVE_ONE);
		}
		if(distance == 1) {
			return findCardAfterType(CardType.MOVE_ONE);
		}
		return null;
	}
	
	//TODO Correct javadoc for recursiveness
	/**
	 * Tries to play cards from the bot's hand, that move the bot as little as possible.
	 * @param allowedCards How many cards the method is allowed to play.
	 * @return The cards the method has determined are best to play. This array always contains exactly as many cards as specified by the parameter.
	 */
	private ProgramCard wasteTurn() {
		if(findCardAfterType(CardType.ROTATE_LEFT) != null) {
			return findCardAfterType(CardType.ROTATE_LEFT);
		}
		if(findCardAfterType(CardType.ROTATE_RIGHT) != null) {
			return findCardAfterType(CardType.ROTATE_RIGHT);
		}
		if(findCardAfterType(CardType.U_TURN) != null) {
			return findCardAfterType(CardType.U_TURN);
		}
		if(findCardAfterType(CardType.BACK_UP) != null) {
			return findCardAfterType(CardType.BACK_UP);
		}
		if(findCardAfterType(CardType.MOVE_ONE) != null) {
			return findCardAfterType(CardType.MOVE_ONE);
		}
		if(findCardAfterType(CardType.MOVE_TWO) != null) {
			return findCardAfterType(CardType.MOVE_TWO);
		}
		if(findCardAfterType(CardType.MOVE_THREE) != null) {
			return findCardAfterType(CardType.MOVE_THREE);
		}
		return null;
	}
	
	/**
	 * Figures out the position the bot will be at according to the specified card.
	 * @param playedCard Should be a movement card.
	 * @param x
	 * @param y
	 * @return The expected position
	 */
	private int[] updateExpectedPos(ProgramCard playedCard, int x, int y, Direction rot) {
		int[] retPos = {x, y};
		int walkDistance = 0;
		//Null check
		if(playedCard == null) {
			return new int[] {x, y};
		}
		switch(playedCard.getType().name()) {
		case "MOVE_ONE":
			walkDistance = 1;
			break;
		case "MOVE_TWO":
			walkDistance = 2;
			break;
		case "MOVE_THREE":
			walkDistance = 3;
			break;
		case "BACK_UP":
			walkDistance = -1;
			break;
		default:
			return retPos;
		}
		
		switch(rot.name()) {
		case "NORTH":	//-y
			retPos[1] = retPos[1] - walkDistance;
			return retPos;
		case "EAST":	//+x
			retPos[0] += walkDistance;
			return retPos;
		case "SOUTH":	//+y
			retPos[1] += walkDistance;
			return retPos;
		case "WEST":	//-x
			retPos[0] -= walkDistance;
			return retPos;
			default:
				return retPos;
		}
	}
	
	/**
	 * Figures out the rotation the bot will have according to the specified card.
	 * @param playedCard Should be a rotation card
	 * @param dir
	 * @return
	 */
	private Direction updateExpectedRot(ProgramCard playedCard, Direction rot) {
		Direction retDir = rot;
		int rotation = 0;
		//Null check
		if(playedCard == null) {
			return retDir;
		}
		//ROTATE_LEFT, ROTATE_RIGHT, U_TURN;
		switch(playedCard.getType().name()) {
		case "ROTATE_LEFT":
			rotation = -1;
			break;
		case "ROTATE_RIGHT":
			rotation = 1;
			break;
		case "U_TURN":
			rotation = 2;
			break;
		default:
			return retDir;
		}
		//Do some magick with ordinals so we don't have to work with switch()
		int dirOrd = (retDir.ordinal()-1) + rotation;
		if(dirOrd < 0) {
			dirOrd += 4;
		} else {
			dirOrd = dirOrd % 4;
		}
		dirOrd++;
		retDir = Direction.values()[dirOrd];
		return retDir;
	}
	
	/**
	 * Searches the bot's hand for a card with the specified card type
	 * @param type The type the card should have
	 * @return A card with the type if the method finds one or null if no such card exists.
	 */
	private ProgramCard findCardAfterType(CardType type) {
		for(ProgramCard card : tempHand) {
			if(card.getType().equals(type)) {
				return card;
			}
		}
		return null;
	}


	
	/**
	 * Decides the target the bot will try to move to based on where the bot is right now. This can be anything from a flag, another game board, other players, etc.
	 * @return
	 */
	private int[] determineTarget() throws BotException{
		
		//We always want to be in gameBoard
		if(activeMap.toLowerCase().equals("dockingbay")) {
			return switchBoard("GameBoard");
		}
		if(activeMap.toLowerCase().equals("gameboard") || activeMap.toLowerCase().equals("gamefield") || activeMap.toLowerCase().equals("dizzydash")) {
			return findNextFlagPosition();
		}
		
		throw new BotException("I can't decide wat to do next!");
		
	}
	
	/**
	 * Sets a target position so that the bot is forced to move onto another board of the course. This only works if the bot is not already on the specified board.
	 * The target will be set either Straight up or straight down from the bot.
	 * @param boardName
	 * @return
	 */
	private int[] switchBoard(String boardName) {
		if(boardName.equals(activeMap)) {
			//If we already are on this board return our current position as we don't have to move anywhere.
			return pos;
		} else if(boardName.equals("DockingBay")){
			int[] targetPos = {pos[0], 20};
			return targetPos;
		} else if(boardName.equals("GameBoard")){
			int[] targetPos = {pos[0], -20};
			return targetPos;
		} else { 
			//If boardName is unknown return our position so that the bot will try not to move
			return pos;
		}
	}
	
	/**
	 * Looks up how many fields the bot can move through before hitting a wall.
	 * @param coords Position from where to start look
	 * @param rot In which direction to look
	 * @param range How far to look
	 * @return How many fields can be moved through without hitting a wall. Returns the range when there is no wall within it.
	 */
	private int lookUpWalls(int[] coords, Direction rot, int range) throws BotException{
		String gameBoardString = maps.split(";")[0];
		String[] fields = gameBoardString.split(":");
		int stepLength = 0;
		int walkableDistance = 0;
		
		/*
		 * When moving vertically, index changes by 12
		 * When moving horizontally, index changes by  1
		 *
		 * 1:
		 * Where is our start position?
		 * Convert two axis position to one axis pos
		 * 2:
		 * In what direction do we have to look up?
		 * Also set step length
		 * 
		 */
		//Prepare position
		int indexPos = (coords[1] * 12) + coords[0];
		
		//set step length
		if(rot.equals(Direction.NORTH) || rot.equals(Direction.SOUTH)) {
			//Vertical -> step 12
			stepLength = 12;
		} else if(rot.equals(Direction.WEST) || rot.equals(Direction.EAST)) {
			//Horizontal -> step 1
			stepLength = 1;
		} else {
			throw new BotException("I don't recognize this direction!");
		}
		/*
		 * Difference between directions!
		 * Example: When looking north we have to look for walls on OUR field only in the "North spot" AND in the NEXT field in the "South spot"
		 * 
		 * North:
		 * Our pos: north
		 * nextField : south
		 * 
		 * South:
		 * Our pos: south
		 * nextField : North
		 * 
		 * East:
		 * Our pos: East
		 * nextField : west
		 * 
		 * West:
		 * Our pos: west
		 * nextField : east
		 * 
		 */
		
		for(int i=0; i<range; i++) {
			int[] expectedCoords = coords;
			
			switch(rot.name()) {
			case "NORTH":
				expectedCoords[1] = expectedCoords[1] + (i*1);
				
				//"Does this field have a wall in it's north spot or does the field after this have a wall in it's south spot?"
				if(getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[0].split("\\.")[0].equals("true") || getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[1].split("\\.")[2].equals("true") ) {
					return walkableDistance;
				} else {
					walkableDistance++;
				}
			break;
			case "EAST":
				expectedCoords[0] = expectedCoords[0] + (i*1);
				
				//"Does this field have a wall in it's north spot or does the field after this have a wall in it's south spot?"
				if(getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[0].split("\\.")[1].equals("true") || getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[1].split("\\.")[3].equals("true") ) {
					return walkableDistance;
				} else {
					walkableDistance++;
				}
			break;
			case "SOUTH":
				expectedCoords[1] = expectedCoords[1] - (i*1);
				
				//"Does this field have a wall in it's north spot or does the field after this have a wall in it's south spot?"
				if(getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[0].split("\\.")[2].equals("true") || getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[1].split("\\.")[0].equals("true") ) {
					return walkableDistance;
				} else {
					walkableDistance++;
				}
			break;
			case "WEST":
				expectedCoords[0] = expectedCoords[0] - (i*1);
				
				//"Does this field have a wall in it's north spot or does the field after this have a wall in it's south spot?"
				if(getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[0].split("\\.")[3].equals("true") || getNextFieldWalls(new int[] {expectedCoords[1], expectedCoords[0]}, rot)[1].split("\\.")[1].equals("true") ) {
					return walkableDistance;
				} else {
					walkableDistance++;
				}
			break;
			}
		}
		return walkableDistance;
	}
	
	/**
	 * THIS HAS TO BE CALLED WITH X AND Y SWAPPED, OTHERWISE IT WON#T WORK CORRECTLY !
	 * @param pos
	 * @param rot
	 * @return The wall arrays of the next two fields in the given position and direction. index 0 is the start field. index 1 is the next field
	 */
	private String[] getNextFieldWalls(int[] pos, Direction rot) throws BotException{
		int index = pos[0] + (pos[1]*12);
		int[] lookAheadPos = {pos[0], pos[1]};
		String[] gameBoardString = maps.split(";");
		String[] fields = gameBoardString[0].split(":");
		
		if((pos[0] > 11 || pos[0] < 0 || pos[1] > 11 || pos[1] < 0) && !activeMap.equals("dockingBay")) {
			throw new BotException("This " + pos[0] + "|" + pos[1] + " position is wrong!");
		}
		
		
		String[] retWalls = new String[2];
		String[] currentField = fields[pos[0] + (pos[1]*12)].split(",");
		retWalls[0] = currentField[2];
		//.split("\\.")
		//nextField depends on rot
		String[] nextField;
		int offset = 0;
		switch(rot.name()) {
		case "NORTH":
			offset = 12;
			lookAheadPos[1] = lookAheadPos[1] + 1;
			break;
		case "EAST":
			offset = 1;
			lookAheadPos[0] = lookAheadPos[0] + 1;
			break;
		case "SOUTH":
			offset = -12;
			lookAheadPos[1] = lookAheadPos[1] + -1;
			break;
		case "WEST":
			offset = -1;
			lookAheadPos[0] = lookAheadPos[0] + 1;
			break;
		}
		
		//If the next field field doesn't exist because the index is out of bounds just add an dummy field with no walls
		if(lookAheadPos[0] > 11 || lookAheadPos[0] < 0 || lookAheadPos[1] > 11 || lookAheadPos[1] < 0){
			retWalls[1] = "false.false.false.false.";
			return retWalls;
		}
		
		nextField = fields[index+offset].split(",");
		retWalls[1] = nextField[2];
		return retWalls;
		
	}
	
	
	private void debugPrintResultMessage(int[] targetPos, int[] expectedCoords, Direction expectedRot){
		System.out.println("I am at " + pos[1] + "|" + pos[0]);
		System.out.println("My next target is at " + targetPos[1] + "|" + targetPos[0]);
		System.out.println("I played the following cards:");
		for(ProgramCard card : programResult) {
			System.out.println("" + card.getPriorityNumber() + "   " + card.getType());
		}
		System.out.println("I expect to be at " + expectedCoords[1] + "|" + expectedCoords[0]);
		System.out.println("I expect to look " + expectedRot);
		System.out.println("\n\nMap overview\n");
		System.out.println("T - target");
		System.out.println("B - Current bot pos");
		System.out.println("b - expected bot pos");
		System.out.println();
		
		String gameBoardString = maps.split(";")[0];
		String[] fields = gameBoardString.split(":");
		debugDrawField(targetPos, expectedCoords, expectedRot);
//		for(int x=0; x<12; x++) {
//			for(int y=0; y<12; y++) {
//				//ORDER: type - direction - walls - lasers - index
////				String singleField[] = fields[(y*12)+x].split(",");
//				
//				if(x == targetPos[1] && y == targetPos[0]) {
//					System.out.print("T");
//				} else if(x == expectedCoords[1] && y == expectedCoords[0]) {
//					System.out.print("b");
//				} else if(x == pos[1] && y == pos[0]) {
//					System.out.print("B");
//				} else {
//					System.out.print("O");
//				}
//			}
//			System.out.println();
//		}
	}
	
	public void debugSetMaps(String maps) {
		this.maps = maps;
	}
	
	
	/**
	 * Draws the game Field with walls, 
	 */
	private void debugDrawField(int[] targetPos, int[] expectedCoords, Direction expectedRot) {
		//Field with walls is 25x25
		char[][] map = new char[25][25];
		for(int x=0; x<map.length; x++) {
			for(int y=0; y<map[x].length; y++) {
				map[x][y] = ' ';
				if(x % 2 != 0 && y % 2 != 0 ) {
					map[x][y] = 'O';
				}
			}
		}
		
		String gameBoardString = maps.split(";")[0];
		String[] fields = gameBoardString.split(":");
		
		//Walls are in line/column 0,2,4...
		for(int x=1; x<map.length; x = x+2) {
			for(int y=1; y<map[x].length; y = y+2) {
				//ORDER: type - direction - walls - lasers - index
				String singleField[] = fields[((y/2)*12)+(x/2)].split(",");
				String[] walls = singleField[2].split("\\.");
				//map[x][y] = 'O';
				if(walls[0].equals("true")) {
					//North
					map[y][x-1] = '-';
				}
				if(walls[1].equals("true")) {
					//East
					map[y+1][x] = '|';
				}
				if(walls[2].equals("true")) {
					//South
					map[y][x+1] = '-';
				}
				if(walls[3].equals("true")) {
					//West
					map[y-1][x] = '|';
				}
				
				if(x/2 == targetPos[1] && y/2 == targetPos[0]) {
					map[y][x] = 'T';
				} else if(x/2 == expectedCoords[1] && y/2 == expectedCoords[0]) {
					map[y][x] = 'b';
				} else if(x/2 == pos[1] && y/2 == pos[0]) {
					map[y][x] = 'B';
				}
			}
			//System.out.println();

		}
		for(int x=0; x<map.length; x++) {
			for(int y=0; y<map[x].length; y++) {
				System.out.print(map[y][x]);
			}
			System.out.println();

		}
	}
	
}
