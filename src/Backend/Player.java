package Backend;

import java.io.Serializable;
import java.util.ArrayList;
import Enums.Direction;
import Exceptions.IllegalParameterException;
import Exceptions.IllegalProgramSizeException;

/**
 * Player Class
 * Contains all actions a player can do.
 * @author Arthur Huber
 */
public class Player implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String name;
	protected int spawnNumber;
	protected int[] pos;
	protected Direction faceDirection;
	protected ProgramSheet programSheet;
	protected ArrayList<ProgramCard> programCards;
	protected int[] archiveTokenPos;
	protected int lastTouchedFlag;
	//On which gameBoard (GameField, Dockingbay?) the player currently is.
	protected String activeMap;
	
	/**
	 * Initializes all attributes according to parameters.
	 * @param spawnNumber
	 * @param lifeTokens
	 * @param posX
	 * @param posY
	 * @param name
	 * @param faceDirection
	 * @throws NullPointerException, IllegalArgumentException if one of the setters throws one.
	 */
	public Player(int spawnNumber, int lifeTokens, int posX, int posY, String name, Direction faceDirection, String activeMap) throws NullPointerException, IllegalArgumentException {
		this.pos = new int[2];
		this.lastTouchedFlag = 0;
		this.archiveTokenPos = new int[2];
		this.faceDirection = Direction.NONE;
		
		programSheet = new ProgramSheet(lifeTokens);
		programCards = new ArrayList<ProgramCard>();
		
		setSpawnNumber(spawnNumber);
		setLifeTokens(lifeTokens);
		setPos(posX, posY);
		setName(name);
		setFaceDirection(faceDirection);
		
		setArchiveTokenPos(posX, posY);
		setActiveMap(activeMap);
	}
	
	public Player(int lifeTokens, int posX, int posY, String name, Direction faceDirection, String activeMap) throws NullPointerException, IllegalArgumentException {
		this.pos = new int[2];
		this.lastTouchedFlag = 0;
		this.archiveTokenPos = new int[2];
		this.faceDirection = Direction.NONE;
		
		programSheet = new ProgramSheet(lifeTokens);
		programCards = new ArrayList<ProgramCard>();
		
		setLifeTokens(lifeTokens);
		setPos(posX, posY);
		setName(name);
		setFaceDirection(faceDirection);
		
		setArchiveTokenPos(posX, posY);
		setActiveMap(activeMap);
	}
	
	/**
	 * Checks if the Power Down token has been already set and sets it.
	 * If the token is already set then false is returned.
	 *
	 * @return True if the token was successfully set. Otherwise false.
	 */
	public boolean announcePowerDown() {
		if(!isPowerDownToken()) {
			setPowerDownToken(true);
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * Places a card in the chosen place of the list in programSheet and removes the played card in the players hand.
	 * If there is already a card in that position then it is replaced by that new card and the old card
	 * is put back in the hand of the player.
	 *
	 * @param ProgramCard object of the card to be placed.
	 * @param registerNumber of the chosen register to place the card in.
	 * @throws IndexOutOfBoundsException if the registerNumber is out of range (index < 0 || index > size())
	 * @throws NullPointerException if the passed card is null
	 * @throws IllegalArgumentException if the passed card is not also part of the player's hand (i.e. programCards) 
	 */
	public void playCard(ProgramCard card, int registerNumber) throws IndexOutOfBoundsException, NullPointerException, IllegalArgumentException{
		//Write card lists into temp lists
		ArrayList<ProgramCard> tempProgram = programSheet.getProgram();
		ArrayList<ProgramCard> tempHand = getProgramCards();
		ProgramCard backCard;
	
		//Check if entered parameters are valid
		if(registerNumber < 0 || registerNumber > 5) {
			throw new IndexOutOfBoundsException("This Register doesn't exist!");
		}
		if(card == null) {
			throw new NullPointerException("My received programCard is null!");
		}
		if(!tempHand.contains(card)) {
			throw new IllegalArgumentException("The received card is not part of my hand!");
		}
		
		//Move the card from the hand into the register and if there already was a card in that register move IT back into the player's hand.
		backCard = tempProgram.get(registerNumber);
		tempProgram.set(registerNumber, card);
		tempHand.remove(card);
		if(backCard != null) {
			tempHand.add(backCard);
		}
	}
	
	/**
	 * Clears the list and fills it with 5x null.
	 * @throws NullPointerException, IllegalProgramSizeException. Forwards Exceptions from setProgram
	 */
	public void clearRegisters() throws NullPointerException, IllegalProgramSizeException{
		ArrayList<ProgramCard> tempProg = programSheet.getProgram();
		tempProg.clear();
		for(int i=0; i<5; i++) {
			tempProg.add(null);
		}
		programSheet.setProgram(tempProg);
	}
	
	//=========================getters/setters
	/**
	 * @return The cards that are in the registers of the programSheet
	 */
	public ArrayList<ProgramCard> getProgram(){
		return programSheet.getProgram();
	}
	
	public int[] getPos() {
		return pos;
	}

	/**
	 * Sets position in of the player robot on the gameBoard
	 * @param playerPosition
	 * @throws NullPointerException if the passed array is null
	 */
	public void setPos(int[] pos) throws NullPointerException, IllegalArgumentException{
		if(pos == null ) {
			throw new NullPointerException("The passed position array was null!");
		}
		if(pos[0] < 0 || pos[1] < 0 || pos[0] > 11 || pos[1] > 11) {
			throw new IllegalArgumentException("I cannot go to x=" + pos[0] + " y=" + pos[1] + "!");
		}
		this.pos = pos;
	}


	/**
	 * Sets position in of the player robot on the gameBoard
	 * @param x, y
	 */
	public void setPos(int x, int y) throws IllegalArgumentException{
	
		
		if(x < 0 || y < 0 || x > 11 || y > 11) {
			throw new IllegalArgumentException("I cannot go to x=" + x + " y=" + y + "!");
		}
		this.pos[0] = x;
		this.pos[1] = y;
	}
	
	public Direction getFaceDirection() {
		return faceDirection;
	}
	
	
	/**
	 * Sets the direction the player robot is facing.
	 * @param faceDirection
	 * @throws NullPointerException if the direction is null
	 * @throws IllegalArgumentException if the direction is NONE
	 */
	public void setFaceDirection(Direction faceDirection) throws NullPointerException, IllegalArgumentException{
		if(faceDirection == null ) {
			throw new NullPointerException("The passed direction was null!");
		}
		if(faceDirection.equals(Direction.NONE)) {
			throw new IllegalArgumentException("The passed diretion was NONE!");
		}
		this.faceDirection = faceDirection;
	}

	/**
	 * 
	 * @return The cards the player currently holds. 
	 */
	public ArrayList<ProgramCard> getProgramCards() {
		return programCards;
	}

	
	/**
	 * Replaces the programCards attribute with the parameter.
	 * @param programCards
	 * @throws NullPointerException if the passed list is null
	 */
	public void setProgramCards(ArrayList<ProgramCard> programCards) {
		if(programCards == null ) {
			throw new NullPointerException("The passed list was null!");
		}
		this.programCards = programCards;
	}

	public int getSpawnNumber() {
		return spawnNumber;
	}
	
	/**
	 * Sets spawnNumber attribute. spawnNumber has to be > 0.
	 * @param spawnNumber
	 */
	public void setSpawnNumber(int spawnNumber) {
		this.spawnNumber = spawnNumber;
	}

	public String getName() {
		return name;
	}
	
	/**
	 * Sets name attribute when name != null and at least of length 1
	 * @param spawnNumber
	 * @throws NullPointerException if the passed name is null.
	 * @throws IllegalArgumentException if the passed string contains no characters
	 */
	public void setName(String name) throws NullPointerException, IllegalArgumentException{
		if(name == null) {
			throw new NullPointerException("The passed name was null!");
		}
		if(name.length() == 0) {
			throw new IllegalArgumentException("The passed name was an emtpy String!");
		}
		this.name = name;
	}
	
	public int getLifeTokens() {
		return programSheet.getLifeTokens();
	}
	
	/**
	 * Calls setLifeToken of ProgramSheet to set lifeTokens.
	 * 
	 * @param lifeTokens
	 */
	public void setLifeTokens(int lifeTokens) {
		programSheet.setLifeTokens(lifeTokens);
	}
	
	public int getDamageTokens() {
		return programSheet.getDamageTokens();
	}

	/**
	 * Calls setDamageToken of ProgramSheet to set damageTokens.
	 * 
	 * @param damageTokens
	 */
	public void setDamageTokens(int damageTokens) {
		programSheet.setDamageTokens(damageTokens);

	}
	
	public boolean isPowerDownToken() {
		return programSheet.isPowerDownToken();
	}

	/**
	 * Calls setPowerDownToken of ProgramSheet to set the Power Down Token.
	 * 
	 * @param powerDownToken
	 */
	public void setPowerDownToken(boolean powerDownToken) {
		programSheet.setPowerDownToken(powerDownToken);

	}
	
	public int[] getArchiveTokenPos() {
		return archiveTokenPos;
	}

	/**
	 * Sets the position of the archive token.
	 * @param archiveTokenPos
	 * @throws NullPointerException if the passed array is null
	 */
	public void setArchiveTokenPos(int[] archiveTokenPos) throws NullPointerException{
		if(archiveTokenPos == null ) {
			throw new NullPointerException("The passed position array was null!");
		}
		this.archiveTokenPos = archiveTokenPos;
	}

	/**
	 * Sets position in of the archive token on the gameBoard
	 * @param x, y
	 */
	public void setArchiveTokenPos(int x, int y) {
		this.archiveTokenPos[0] = x;
		this.archiveTokenPos[1] = y;
	}

	public int getLastTouchedFlag() {
		return lastTouchedFlag;
	}

	/**
	 * Sets the index of the Flag the player robot touched last.
	 * @param lastTouchedFlag
	 */
	public void setLastTouchedFlag(int lastTouchedFlag) {
		this.lastTouchedFlag = lastTouchedFlag;
	}
	
	public boolean isSecondPowerdownToken() {
		return programSheet.secondPowerdownToken;
	}
	
	public void setProgram(ArrayList<ProgramCard> program) throws NullPointerException, IllegalProgramSizeException {
		programSheet.setProgram(program);
	}
	
	/**
	 * Calls setSecondPowerdownToken of programSheet to set the Second Powerdown Token
	 * @param secondPowerdownToken
	 */
	public void setSecondPowerdownToken(boolean secondPowerdownToken) {
		programSheet.setSecondPowerdownToken(secondPowerdownToken);
	}
	
	/**
	 * Converts all attributes into Strings returns them as a list of String lists. The contents of programSheet.getInfo are also added. Using this kind of nested lists is useful for the card lists
	 * since this keeps the outer list at a constant size. The order of strings is: name, spawn number, position, face direction, programCards, archive token pos, last touched flag, |-> FROM PROGRAM_SHEET |-> life tokens, damage tokens, power down token, cards in program
	 * @return The finished list.
	 */
	public String getInfo(){
		String retString = "";
		
		//name 0
		retString += name + ",";
		
		//spawn number 1
		retString += spawnNumber + ",";
		
//		//position
//		retString +=  Integer.toString(pos[0]) + " ; " + Integer.toString(pos[1]) + ",";
		
		
//		//face direction
//		retString +=  faceDirection.toString() + ",";
		
		//program cards hand 2
		
		for(ProgramCard card : programCards) {
			retString += card.getInfo() + ";";
		}
		retString += ",";
		
//		//archive token position
//		retString +=  Integer.toString(archiveTokenPos[0]) + " ; " + Integer.toString(archiveTokenPos[1]) + ",";
		
		//last touched flag 3
		retString += lastTouchedFlag + ",";
		
		retString += programSheet.getInfo();
		
		return retString;
	}
	
	/**
	 * Returns the actual size of the program without null places. Don't use this to check the order!
	 * @return The number of programCard objects in the program list
	 */
	public int getActualProgramSize(){
		int size = 0;
		programSheet.getProgram();
		for(ProgramCard card : programSheet.getProgram()) {
			if(card != null) {
				size++;
			}
		}
		return size;
	}
	
	
    public String toCsvString() {
																	 
		String csv = name + ";" + spawnNumber + ";" + pos[0] + ";" + pos[1] + ";" + faceDirection + ";" + programSheet.toCsvString()  + archiveTokenPos[0] + ";" + archiveTokenPos[1] + ";" + lastTouchedFlag + ";" + activeMap + ";" + programCards.size() + ";";
//		csv += "\n";
		for(ProgramCard programCard : programCards) {
//			csv += ";";
			csv += programCard.toCsvString();
		}
		
		csv = csv.substring(0, csv.length()-1);
		return csv;
	}
    
	public String getActiveMap() {
		return activeMap;
	}

	public void setActiveMap(String activeMap) throws NullPointerException, IllegalArgumentException{
		if(activeMap == null) {
			throw new NullPointerException("Received map name is null!");
		}
		if(activeMap.length() == 0) {
			throw new IllegalArgumentException("Received map name has no length!");
		}
		this.activeMap = activeMap;
	}




	/**
	 * ProgramSheet Class
	 * Contains the cards players want to play in order.
	 * @author Arthur Huber
	 */
	private class ProgramSheet implements Serializable{
		
		/**
		 * 
		 */
		protected static final long serialVersionUID = 1L;
		protected int lifeTokens;
		protected int damageTokens;
		protected boolean powerDownToken;
		protected boolean secondPowerdownToken;
		protected ArrayList<ProgramCard> program;
		
		/**
		 * Initializes attributes and fill program list with 5x null.
		 * 5 times null.
		 * @param lifeTokens
		 * @param damageTokens
		 * @param powerDownToken
		 */
		public ProgramSheet(int lifeTokens) {
			setLifeTokens(lifeTokens);
			setDamageTokens(0);
			setPowerDownToken(false);
			setSecondPowerdownToken(false);
			
			program = new ArrayList<ProgramCard>();

			//Initialising the list with 5x null should make it easier to implement Player.playerCard().
			for(int i=0; i<5; i++) {
				program.add(null);
			}
		}
		
		//=========================getters/setters
		public ArrayList<ProgramCard> getProgram() {
			return program;
		}

		/**
		 * Replaces program with the parameter.
		 * @param program The list of ProgramCards
		 * @throws NullPointerException if Player tries to input more than 5 cards
		 */
		public void setProgram(ArrayList<ProgramCard> program) throws NullPointerException, IllegalProgramSizeException{
			if(program == null) {
				throw new NullPointerException("The receid program list is null!");
			}
			if(program.size() > 5) {
				throw new IllegalProgramSizeException("Received program is bigger than 5 Instructions!");
			}
			this.program = program;
		}
		
		public boolean isPowerDownToken() {
			return powerDownToken;
		}

		/**
		 * Sets the Power Down Token.
		 * 
		 * @param powerDownToken
		 */
		public void setPowerDownToken(boolean powerDownToken) {
			this.powerDownToken = powerDownToken;
		}
		
		public int getLifeTokens() {
			return lifeTokens;
		}

		/**
		 * Sets the number of Life Tokens
		 * @param lifeTokens
		 */
		public void setLifeTokens(int lifeTokens) {
			this.lifeTokens = lifeTokens;
		}

		public int getDamageTokens() {
			return damageTokens;
		}

		/**
		 * Sets the number of Damage Tokens
		 * @param damageTokens
		 */
		public void setDamageTokens(int damageTokens) {
			System.out.println("My damage Tokens have been set to " + damageTokens + ". My name is " + name);
			this.damageTokens = damageTokens;
		}

		public boolean isSecondPowerdownToken() {
			return secondPowerdownToken;
		}

		/**
		 * Sets the second Power Down Token
		 * @param secondPowerdownToken
		 */
		public void setSecondPowerdownToken(boolean secondPowerdownToken) {
			this.secondPowerdownToken = secondPowerdownToken;
		}
		
		/**
		 * Converts all attributes into Strings returns them as a list of String lists. Using this kind of nested lists is useful for the card lists
		 * since this keeps the outer list at a constant size. The order of strings is: life tokens, damage tokens, power down token, cards in program
		 * @return The finished list.
		 */
		public String getInfo(){
			String retString = "";
			
			//Add life tokens 4
			retString += Integer.toString(lifeTokens) + ",";
			
			//damage tokens 5
			retString += Integer.toString(damageTokens) + ",";
			
			//power down token 6
			retString += Boolean.toString(powerDownToken) + ",";

			//program
			
			for(ProgramCard card : program) {
				if(card == null) {
					retString += "null" + ";";
				} else {
					retString +=  card.getInfo() + ";";
				}
			}
			retString += ",";
			
			return retString;
		}
		
            public String toCsvString() {
			
			String csv = lifeTokens + ";" + damageTokens + ";" + powerDownToken + ";" + secondPowerdownToken + ";";
			for(ProgramCard programCard : program) {
				if(programCard!=null) {
				csv += programCard.toCsvString();
				} else {
					csv += "noPrioNumber;noCard" + ";";
				}
			}
			return csv;
			
		}
	}
}