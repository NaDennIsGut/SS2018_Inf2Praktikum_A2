package Backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import Enums.CardType;
import Enums.Direction;
import Enums.FieldType;
import Exceptions.AmountOfPlayersException;
import Exceptions.AmountOfProgramCardsException;
import Exceptions.DestructionException;
import Exceptions.GameOverException;
import Exceptions.IllegalAmountOfDamageTokensException;
import Exceptions.IllegalPositionException;
import Exceptions.IllegalProgramCardException;
import Exceptions.IllegalProgramException;
import Exceptions.IllegalRegisterPhaseException;
import Exceptions.IllegalSpawnException;
import Exceptions.OutsideBoardException;
import Exceptions.RegisterLockedException;

/**
 * Class RuleBook Contains all the rules for the course DizzyDash
 * 
 * @author Dennis
 *
 */

public class RuleBook implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 */


	// --------Methods-------------//
	/**
	 * Checks if the cards the game master turned over are correct.
	 * 
	 * @param registerPhase
	 *             current register Phase - can only be 1 - 5
	 * @param playerlist
	 *             list of all the players still in the game
	 * @param maybe
	 *             list of cards the game master thinks need to be turned over
	 * @return true if the game master took the correct cards from each players
	 *         program
	 * @throws IllegalProgramCardException
	 *              if the game master took the wrong card from someone
	 * @throws AmountOfProgramCardsException
	 *              if the amount of program cards doenst match the amount of
	 *             players
	 * @throws RegisterLockedException
	 *              if the game master took a card from a player who is currently
	 *             powered down or not allowed to play a card due to his amount of
	 *             damage tokens
	 * @throws AmountOfPlayersException
	 *              playerlist doesn't contain every player
	 * @throws IllegalRegisterPhaseException
	 *             registerPhase is not between 1 and 5
	 */
	public boolean checkCardOrder(int registerPhase, ArrayList<Player> playerlist, ArrayList<ProgramCard> maybe)
			throws IllegalProgramCardException, AmountOfProgramCardsException, RegisterLockedException,
			AmountOfPlayersException, IllegalRegisterPhaseException {

		boolean correct = false;
		if (registerPhase < 0 || registerPhase > 4) {
			throw new IllegalRegisterPhaseException("Register phase can only be 0 to 4");
		}

		// this part removes any player from the list that currently cant play a card.
		// it does that so the method can still continue to compare the remaining
		// players.

		for (Player player : playerlist) {
			if (registerPhase == 0 && player.getDamageTokens() == 9 || player.isPowerDownToken()) {
				throw new RegisterLockedException(
						player.getName() + " is locked or powered down in this register phase");
			} else if (registerPhase == 1 && player.getDamageTokens() == 8 || player.isPowerDownToken()) {
				throw new RegisterLockedException(
						player.getName() + " is locked or powered down in this register phase");
			} else if (registerPhase == 2 && player.getDamageTokens() == 7 || player.isPowerDownToken()) {
				throw new RegisterLockedException(
						player.getName() + " is locked or powered down in this register phase");
			} else if (registerPhase == 3 && player.getDamageTokens() == 6 || player.isPowerDownToken()) {
				throw new RegisterLockedException(
						player.getName() + " is locked or powered down in this register phase");
			} else if (registerPhase == 4 && player.getDamageTokens() == 5 || player.isPowerDownToken()) {
				throw new RegisterLockedException(
						player.getName() + " is locked or powered down in this register phase");
			}
		}

		if (playerlist.get(0).equals(null)) {
			throw new AmountOfPlayersException("There must be 2 to 8 Players playing!");
		}
		if (maybe.get(0).equals(null)) {
			throw new NullPointerException("Maybe list is empty!");

		}

		for (int i = 0; i < maybe.size(); i++) {
			for (int e = 0; e < playerlist.size(); e++) {
				if (maybe.get(i).equals(playerlist.get(e).getProgram().get(registerPhase))) {					
					return correct = true;
				} else {
					throw new IllegalProgramCardException(
							playerlist.get(i).getName() + " tried to play an illegal card!");
				}

			} // end of inner loop

		} // end of outer loop
		return correct;
	}

	/**
	 * Gives information about the needed amount of players
	 * 
	 * @param gameBoard
	 *             the current gameboard
	 * @return  int[int min amount of players, int max amount of players]
	 */

	public int[] lookUpAllowedPlayers() {
		int[] allowedPlayers = { 2, 8 };
		return allowedPlayers;
	}

	/**
	 * Checks if enough players play and if everyone typed in a name
	 * 
	 * @param playerlist
	 *             list of the players
	 * @return true if the amount is correct and every name is there
	 * @throws AmountOfPlayersException
	 *              thrown if less than 2 or more than 8 players try to play
	 */

	public boolean checkSettings(ArrayList<Player> playerlist) {
		boolean correctSettings = true;
		if (playerlist.size() == 0) {
		return false;
		}

		// first check if the correct amount of player try to play
		if (playerlist.size() < this.lookUpAllowedPlayers()[0] || playerlist.size() > this.lookUpAllowedPlayers()[1]) {
			return false;
			

		}

		// second check if every player typed in a name

		for (Player Player : playerlist) {
			if (Player.getName() == "") {
				correctSettings = false;
				return correctSettings;
			}
		}

		return correctSettings;
	}

	/**
	 * This method checks if a player is actually allowed to announce a power down.
	 * Only a player with damage tokens may chose to power down When a robot is
	 * powered down two of his damage tokens are removed!
	 * 
	 * @param player
	 *             the player who announced a power down
	 * @return true if the player has any damage tokens
	
	 * @throws IllegalAmountOfDamageTokensException
	 *              thrown if a player has less than 0 or more than 10 tokens
	 */

	public boolean CheckPowerDown(Player player)  {
		if (player.getDamageTokens() > 0 ) {
			return true;
		}
		 if (player.isPowerDownToken() == true) {
			return true;
		} 
		return false; 
	}

	/**
	 * Tells how many program cards the player gets depending on his damage tokens
	 * 
	 * @param player
	 *             player you want to deal program cards
	 * @return  the amount of program cards the player should get
	 * @throws IllegalAmountOfDamageTokensException
	 *              thrown if a player has less than 0 or more than 10 tokens
	 * @throws DestructionException
	 *             thrown if a player has 10 damage tokens - check for destruction
	 * @throws AmountOfProgramCardsException - if a player has the wrong amount of cards
	 */

	public int amountProgramCards(Player player) throws IllegalAmountOfDamageTokensException, DestructionException, AmountOfProgramCardsException {
		
		if (player.getDamageTokens() < 0 || player.getDamageTokens() > 10) {
			throw new IllegalAmountOfDamageTokensException("Players can only have between 0 and 9 damage tokens! Bute he has " + player.getDamageTokens());
		} else if (player.getDamageTokens() == 10) {
			throw new DestructionException(player.getName() + " has 10 tokens - check for destruction!");
		}
		int amount;
		if (player.getDamageTokens() == 0) {
			return amount = 9;
		} else if (player.getDamageTokens() == 1) {
			return amount = 8;
		} else if (player.getDamageTokens() == 2) {
			return amount = 7;
		} else if (player.getDamageTokens() == 3) {
			return amount = 6;
		} else if (player.getDamageTokens() == 4) {
			return amount = 5;
		} else if (player.getDamageTokens() == 5) {
			return amount = 4;
		} else if (player.getDamageTokens() == 6) {
			return amount = 3;
		} else if (player.getDamageTokens() == 7) {
			return amount = 2;
		} else if (player.getDamageTokens() == 8) {
			return amount = 1;
		} else if (player.getDamageTokens() == 9) {
			return amount = 0;
		} else {
			throw new AmountOfProgramCardsException(player.getName() + " doesn't have the right amount of cards!");
		}

	}

	/**
	 * checks whether the given player actually got the right amount of cards
	 * 
	 * @param player
	 *             the player who needs to be checked
	 * @return  true if the amount of program cards in the players hand is correct
	 * @throws AmountOfProgramCardsException
	 *              if the amount of program cards in the players hand is wrong
	 */

	public boolean checkAmountOfProgramCards(Player player) throws AmountOfProgramCardsException {
		boolean correctAmount = true;

		if (player.getDamageTokens() == 0) {
			if (player.getProgramCards().size() == 9) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 1) {
			if (player.getProgramCards().size() == 8) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 2) {
			if (player.getProgramCards().size() == 7) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 3) {
			if (player.getProgramCards().size() == 6) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 4) {
			if (player.getProgramCards().size() == 5) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 5) {
			if (player.getProgramCards().size() == 4) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 6) {
			if (player.getProgramCards().size() == 3) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");

			}

		} else if (player.getDamageTokens() == 7) {
			if (player.getProgramCards().size() == 2) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");
			}

		} else if (player.getDamageTokens() == 8) {
			if (player.getProgramCards().size() == 1) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(
						player.getName() + " doesn't have the right amount of program cards!");
			}

		} else if (player.getDamageTokens() == 9) {
			if (player.getProgramCards().size() == 0) {
				return correctAmount = true;
			} else {
				throw new AmountOfProgramCardsException(player.getName() + " has an illegal amount of program cards!");
			}

		} else if (player.getProgram().get(0) == null) {
			throw new AmountOfProgramCardsException(
					player.getName() + "'s List of Program Cards contains a null object");
		}

		return correctAmount;

	}

	/**
	 * Checks if the player put the allowed amount of cards in his program.
	 * Depending on his damage tokens.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @return  true if the size of the players program is correct
	 * @throws IllegalProgramException
	 *              thrown if the size of the players program is wrong
	 */

	public boolean checkPlayersProgram(Player player) throws IllegalProgramException {

		if (player.getDamageTokens() < 4) {
			if(player.getActualProgramSize() == 5) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize() + " cards. Thats not enough!");
			}
		}
		if (player.getDamageTokens() == 4) {
			if (player.getActualProgramSize() == 5) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize()
						+ " cards for his program. That's not enough!");
			}
		} else if (player.getDamageTokens() == 5) {
			if (player.getActualProgramSize() == 4) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize()
						+ " cards for his program. That's not enough!");
			}
		}
		if (player.getDamageTokens() == 6) {
			if (player.getActualProgramSize() == 3) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize()
						+ " cards for his program. That's not enough!");
			}
		}
		if (player.getDamageTokens() == 7) {
			if (player.getActualProgramSize() == 2) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize()
						+ " cards for his program. That's not enough!");
			}
		}
		if (player.getDamageTokens() == 8) {
			if (player.getActualProgramSize() == 1) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize()
						+ " cards for his program. That's not enough!");
			}
		}
		if (player.getDamageTokens() == 9) {
			if (player.getActualProgramSize() == 0) {
				return true;
			} else {
				throw new IllegalProgramException(player.getName() + " chose " + player.getActualProgramSize()
						+ " cards for his program. That's not enough!");
			}
		}

		return false;

	}

	/**
	 * Tells the gamemaster how many life tokens each player gets at the beginning
	 * of the game
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @return  the amount of life tokens each player gets.
	 */

	public int amountLifeTokens(Player player) {
		return 3; // Platzhalter

	}

	/**
	 * Checks if a player is moved by a board element. It must be called after every
	 * player has been moved by their program card.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param playerlist
	 *             list of all the players in the game
	 * @param gb
	 *             the gameboard
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 * @throws IllegalPositionException
	 *              if the player is outside the gameboard
	 * @throws NullPointerException
	 *              playerlist contains a null object
	 * @throws OutsideBoardException 
	 */
	public List<ArrayList<Object>> movedByBoardElement(Player player, ArrayList<Player> playerlist, GameBoard gb)
			throws IllegalPositionException, NullPointerException, OutsideBoardException {
		
		List <ArrayList<Object>> content = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> playerMove = new ArrayList();
		int[] newPosition = player.getPos();
		Direction newDirection = player.getFaceDirection();
		boolean collision = false;
		if(player.getActiveMap().equals("dockingBay")) {
			playerMove.add(newPosition); 
			playerMove.add(newDirection); 
			playerMove.add(collision);
			content.add(playerMove);
			return content;
		}
		
		
		
		if(outsideBoard(player.getPos()[0],player.getPos()[1])) {
			throw new OutsideBoardException("Player is outside the board. He cant move!");
		}
		// check for null
		for (Player doedel : playerlist) {
			if (doedel.equals(null)) {
				throw new NullPointerException("The given playerlist contains a null object");
			}
		}

		// check for illegal position
		for (Player doedel : playerlist) {
			if (doedel.getPos()[0] > 11 || player.getPos()[1] > 11) {
				throw new IllegalPositionException(
						player.getName() + " is outside the gameboard. Please check for destruction!");
			}
		}

		int x = player.getPos()[0];
		int y = player.getPos()[1];

		// if the field is a normal conveyer belt
		if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT)) {
			if (gb.fieldDirectionAt(x, y, false).equals(Direction.NORTH)) {
				return moveNorth(player, gb, playerlist, false, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.EAST)) {
				return moveEast(player, gb, playerlist, false, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.SOUTH)) {
				return moveSouth(player, gb, playerlist, false, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.WEST)) {
				return moveWest(player, gb, playerlist, false, false);
			}
		}
		// if the field is conveyer belt fast
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_FAST)) {
			if (gb.fieldDirectionAt(x, y, false).equals(Direction.NORTH)) {
				return moveNorth(player, gb, playerlist, false, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.EAST)) {
				return moveEast(player, gb, playerlist, false, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.SOUTH)) {
				return moveSouth(player, gb, playerlist, false, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.WEST)) {
				return moveWest(player, gb, playerlist, false, false);
			}
		}
		// if the field is a normal rotate left belt
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_LEFT)) {
			return rotateLeft(player);
		}
		// if the field is a normal rotate right belt
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_RIGHT)) {
			return rotateRight(player);
		}
		// if the player is on a gear rotate left
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.GEAR_ROT_LEFT)) {
			return rotateLeft(player);
		}
		// if the player is on a gear rotate right
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.GEAR_ROT_RIGHT)) {
			return rotateRight(player);
		}

		// if the field is an express rotate to the left
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_LEFT_FAST)) {
			if (gb.fieldDirectionAt(x, y, false).equals(Direction.NORTH)) {
				return moveNorth(player, gb, playerlist, false, true);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.EAST)) {
				return moveEast(player, gb, playerlist, false, true);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.SOUTH)) {
				return moveSouth(player, gb, playerlist, false, true);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.WEST)) {
				return moveWest(player, gb, playerlist, false, true);
			}
		}

		// if the field is an express rotate to the right
		else if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST)) {
			if (gb.fieldDirectionAt(x, y, false).equals(Direction.NORTH)) {
				return moveNorth(player, gb, playerlist, true, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.EAST)) {
				return moveEast(player, gb, playerlist, true, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.SOUTH)) {
				return moveSouth(player, gb, playerlist, true, false);
			} else if (gb.fieldDirectionAt(x, y, false).equals(Direction.WEST)) {
				return moveWest(player, gb, playerlist, true, false);
			}
		}

		else {
			return remainPosition(player, gb, playerlist);
		}
		return null;
	}

	/**
	 * checks if every Player is on the correct spawn field.
	 * 
	 * @param playerlist
	 *             list containing all the players
	 * @param gb
	 *             gameboard
	 * @return  true if every player is on the correct spawn field
	 * @throws NullPointerException
	 *              playerlist contains a null object
	 * @throws IllegalSpawnException
	 *              if a player is on the wrong spawn field
	 */
	public boolean checkSpawn(ArrayList<Player> playerlist, GameBoard gb)
			throws NullPointerException, IllegalSpawnException {
		boolean correct = false;

		// chek for null
		for (Player player : playerlist) {
			if (player.equals(null)) {
				throw new NullPointerException("Given list of players contains a null object");
			}
		}

		// check spawn

		int x, y;

		for (Player player : playerlist) {
			x = player.getPos()[0];
			y = player.getPos()[1];

			if (gb.fieldTypeAt(x, y, false).equals(FieldType.SPAWN)) {
				if (player.getSpawnNumber() == gb.fieldIndexAt(x, y, false)) {
					correct = true;
				} else {
					throw new IllegalSpawnException(player.getName() + " is on the wrong spawn field");
				}
			} else {
				throw new IllegalSpawnException(player.getName() + " is not on a spawn field");
			}
		}

		return correct;

	}

	/**
	 * checks if the player touched a flag in the correct order.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *            the gameboard
	 * @return  true if the flag the player touched is in the correct order
	 * @throws GameOverException
	 *              if the player touched the third flag in the right order the
	 *             game is over
	 * @throws NullPointerException
	 *             player is null
	 */
	public boolean checkFlagTouch(Player player, GameBoard gb) throws GameOverException, NullPointerException {

		if (player.equals(null)) {
			throw new NullPointerException("given player is null!");
		}

		int x = player.getPos()[0];
		int y = player.getPos()[1];
		boolean correctFlag = false;

		if (gb.fieldTypeAt(x, y, false).equals(FieldType.FLAG)) {
			if (player.getLastTouchedFlag() == 0) {
				if (gb.fieldIndexAt(x, y, false) == 1) {
					return correctFlag = true;
				} else {
					return correctFlag = false;
				}
			} else if (player.getLastTouchedFlag() == 1) {
				if (gb.fieldIndexAt(x, y, false) == 2) {
					return correctFlag = true;
				} else {
					return correctFlag = false;
				}
			} else if (player.getLastTouchedFlag() == 2) {
				if (gb.fieldIndexAt(x, y, false) == 3) {
					throw new GameOverException(player.getName() + " has won the game");
				} else {
					return correctFlag = false;
				}
			}
		}

		return false;

	}

	/**
	 * checks if the player has landed on a gamefield with a laser.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             the gameboard
	 * @return the amount of damage taken through a laser hit
	 * @throws NullPointerException
	 *              player is null
	 * @throws OutsideBoardException 
	 */
	public int damageByLaser(Player player, GameBoard gb) throws NullPointerException, OutsideBoardException {
		if(player.getActiveMap().equals("dockingBay")) {
			return 0;
		}
		if (player.equals(null)) {
			throw new NullPointerException("given player is null!");
		}
		
		
		int damage = 0;
		int x = player.getPos()[0];
		int y = player.getPos()[1];

		if (outsideBoard(x,y)) {
			throw new OutsideBoardException("Player is outside Gameboard. Cant take damage by laser!");
		}
		
		for (int i = 0; i < 4; i++) {
			if (gb.fieldLasersAt(x, y, false)[i] == 1) {
				return damage = 1;
			} else if (gb.fieldLasersAt(x, y, false)[i] == 2) {
				return damage = 2;
			} else if (gb.fieldLasersAt(x, y, false)[i] == 3) {
				return damage = 3;
			} else if (gb.fieldLasersAt(x, y, false)[i] == 4) {
				return damage = 4;
			} 

		}

		return damage;

	}

	/**
	 * checks if the game is over. There can only be one winner.
	 * 
	 * @param playerlist
	 *             list containing all the players
	 * @param gb
	 *             the gameboard
	 * @return  true if one player has touched all three flags in the right order.
	 *         true if only one player is left in the game
	 * @throws NullPointerException
	 *              playerlist contains null object
	 */

	public boolean checkGameOver( GameBoard gb, ArrayList<Player>playerlist) throws NullPointerException {
	

		// check for all three flags touched
		for (Player player : playerlist){
			int x = player.getPos()[0];
			int y = player.getPos()[1];
			if (gb.fieldTypeAt(x, y, false).equals(FieldType.FLAG)) {
				if (gb.fieldIndexAt(x, y, false) == 3) {
					if (player.getLastTouchedFlag() == 2) {
						return true;
					}
				}
			}
		}
			

		// check if only one player is left in the game
		if (playerlist.size() == 1) {
			 return true;
		}

		return false;

	}

	/**
	 * A repair happens after every fifth register phase. A player must be on a
	 * repair Field and must have at least one damage Token. It
	 * 
	 * @param player
	 *            player who might be repaired
	 * @param registerPhase
	 *            must be 5!
	 * @param gb
	 *             the gameboard
	 * @return returns an int with the amount of damage tokens that get taken away
	 *         from the player. If a player doesnt have any damage tokens 0 is
	 *         returned. If a player only has 1 damage token but is on a "repair
	 *         two" field, only 1 token is taken from the player. If the player is
	 *         not on a repair field 0 is returned.
	 * @throws IllegalRegisterPhaseException
	 *             thrown if the register phase is not 5
	 * @throws NullPointerException
	 * @throws IllegalAmountOfDamageTokensException
	 *              player has a negative amount of damage tokens
	 */

	public int checkRepair(Player player, int registerPhase, GameBoard gb)
			throws IllegalRegisterPhaseException, NullPointerException, IllegalAmountOfDamageTokensException {
		// check exceptions

		if (player.equals(null)) {
			throw new NullPointerException("Player is null");
		}
		if (registerPhase != 4) {
			throw new IllegalRegisterPhaseException("For a repair register phase must be four");
		}

		// check for repair

		int x = player.getPos()[0];
		int y = player.getPos()[1];
		int repair = 0;
		// repair one
		if (gb.fieldTypeAt(x, y, false).equals(FieldType.REPAIR_ONE)) {
			if (player.getDamageTokens() == 0) {
				return repair = 0;
			} else if (player.getDamageTokens() > 0) {
				return repair = 1;
			} else {
				throw new IllegalAmountOfDamageTokensException("Players cant have negativ damage tokens!");
			}
			// repair two
		} else if (gb.fieldTypeAt(x, y, false).equals(FieldType.REPAIR_TWO)) {
			if (player.getDamageTokens() == 0) {
				return repair = 0;
			} else if (player.getDamageTokens() == 1) {
				return repair = 1;
			} else if (player.getDamageTokens() > 1) {
				return repair = 2;
			} else {
				throw new IllegalAmountOfDamageTokensException("Players cant have negativ damage tokens!");
			}
		} else {
			return repair = 0;
		}
	}

	/**
	 * Checks if a player is destroyed
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @return true If a robot has 10 DamageTokens it will be destroyed. If a robot
	 *         moved or was moved into a pit it gets destroyed. If a robot moved or
	 *         was pushed off the gameboard's edge it gets destroyed. Destruction
	 *         means the loss of a life token.
	 */
	public boolean checkDestruction(Player player, GameBoard gb) {
		int[] position = player.getPos();
		if (player.getDamageTokens() == 10) {
			return true;
		
			
		}
		
		if (position[0] > 11 || position[0] < 0) {
			return true;
		} else if (position[1] > 11 || position[1] < 0) {
			return true;
		}
		if (gb.fieldTypeAt(position[0], position[1], false).equals(FieldType.PIT)) {
			return true;
		}

		

		return false;
	}

	/**
	 * Gives information what happens when a player is moved by a program card.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param pc
	 *             the program card the belongs the player in this register phase
	 * @param playerlist
	 *             list containing all the players
	 * @param gb
	 *             the gameboard
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 * @throws IllegalProgramCardException
	 *              if the program card doesn't exist.
	 * @throws OutsideBoardException 
	 * 				if the player is outside the board but tries to move either way
	 */

	public List<ArrayList<Object>> checkMovement(Player player, ProgramCard pc, ArrayList<Player> playerlist,
			GameBoard gb) throws IllegalProgramCardException, OutsideBoardException {
		if(outsideBoard(player.getPos()[0],player.getPos()[1])) {
			throw new OutsideBoardException("Player is outside the board. He cant move!");
		}
		if (player.equals(null)) {
			throw new NullPointerException("Given player is null");
		}

		for (Player doedel : playerlist) {
			if (doedel.equals(null)) {
				throw new NullPointerException("Playerlist contains null Object");
			}
		}
		// Move one - moves player in the facing direction by one field - collision
		// possible//

		if (pc.getType().equals(CardType.MOVE_ONE)
			||	pc.getType().equals(CardType.MOVE_TWO)
			||  pc.getType().equals(CardType.MOVE_THREE)
				) {
			if (player.getFaceDirection().equals(Direction.NORTH)) {
				return moveNorth(player, gb, playerlist, false, false);

			} else if (player.getFaceDirection().equals(Direction.EAST)) {
				return moveEast(player, gb, playerlist, false, false);

			} else if (player.getFaceDirection().equals(Direction.SOUTH)) {
				return moveSouth(player, gb, playerlist, false, false);

			} else if (player.getFaceDirection().equals(Direction.WEST)) {
				return moveWest(player, gb, playerlist, false, false);

			}
		}

	

		// BackUp moves Player backwards - collision possible //
		else if (pc.getType().equals(CardType.BACK_UP)) {
			return backUp(player, playerlist, gb);
		}

		// Rotate left - no collision possible //
		else if (pc.getType().equals(CardType.ROTATE_LEFT)) {
			return rotateLeft(player);
		}

		// Rotate left - no collision possible //
		else if (pc.getType().equals(CardType.ROTATE_RIGHT)) {
			return rotateRight(player);

			// U Turn - no collision possible //
		} else if (pc.getType().equals(CardType.U_TURN)) {
			return uTurn(player);
		}

		else {
			throw new IllegalProgramCardException("The chosen card does not exist!");
		}
		return null;
	}

	/**
	 * Gives information about which player goes first depending on the priority
	 * number on their program card.
	 * 
	 * @param currentProgramCards
	 *             list of the cards that were turned over this register phase
	 * @return a freshly ordered list with the highest priority in the first index
	 *         and the lowest priority in the last index.
	 */
	public ArrayList<ProgramCard> checkPriority(ArrayList<ProgramCard> currentProgramCards) {

		currentProgramCards.sort(Comparator.comparingInt(ProgramCard::getPriorityNumber).reversed());

		return currentProgramCards;

	}

	/**
	 * This method is only for the movement of a player by board elements. gives
	 * information about how often the game master needs to move a player by one
	 * field.
	 * 
	 * @param player
	 *             who needs to be checked
	 * @param gb
	 *             gameboard
	 * @return  2 if it is a fast conveyer belt. 1 if it is a normal conveyer belt.
	 *         0 if it is a normal game field without movement.
	 */

	public int loopAmountBoardElement(Player player, GameBoard gb) {
		int x = player.getPos()[0];
		int y = player.getPos()[1];
		if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT)
				|| gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_LEFT)
				|| gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_RIGHT)
				|| gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_LEFT_FAST)
				|| gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST)
				|| gb.fieldTypeAt(x, y, false).equals(FieldType.GEAR_ROT_LEFT)
				|| gb.fieldTypeAt(x, y, false).equals(FieldType.GEAR_ROT_RIGHT)
				) {
			return 1;
		} else if (gb.fieldTypeAt(x, y, false).equals(FieldType.CONVEYOR_BELT_FAST)
				) {
			return 2;
		} else {
			return 0;

		}
	}

	/**
	 * This method is only for the movement of a player by program cards.
	 * 
	 * @param player
	 *            player who needs to be checked
	 * @param pc
	 *             the program card the player is moved by
	 * @return 3 if it is a MOVE_THREE. 2 if it is a MOVE_TWO. 1 for any other type
	 *         of cards.
	 */
	public int loopAmountProgramCard(Player player, ProgramCard pc) {
		if (pc.getType().equals(CardType.BACK_UP) || pc.getType().equals(CardType.ROTATE_LEFT)
				|| pc.getType().equals(CardType.ROTATE_RIGHT) || pc.getType().equals(CardType.MOVE_ONE)
				|| pc.getType().equals(CardType.U_TURN)) {
			return 1;
		} else if (pc.getType().equals(CardType.MOVE_TWO)) {
			return 2;
		} else {
			return 3;
		}

	}

	/**
	 * checks if a player is dead. Death means the player is out of the game.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @return true if the player has 0 life tokens
	 */
	public boolean checkDeath(Player player) {

		if (player.getLifeTokens() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * For the specified player decides how many cards this player can have in their program
	 * @param p
	 * @return The number of cards allowed in the program
	 */
	public int determineAllowedProgramSize(Player p) throws NullPointerException{
		//No penalty if 4 or less damage Tokens
		if(p == null) {
			new NullPointerException("Received player is null!");
		}
		if(p.getDamageTokens() < 5) {
			return 5;
		}
		//Apply penalty
		return 5 - (p.getDamageTokens() - 4);
		
		
	}

	// private help methods!
//	/**
//	 * checks for destruction by only position.
//	 * 
//	 * @param position
//	 *             int array [x,y]
//	 * @param gb
//	 * @return
//	 */
//
//	private boolean checkDestructionByPosition(int[] position, GameBoard gb) {
//		if (gb.fieldTypeAt(position[0], position[1], false).equals(FieldType.PIT)) {
//			return true;
//		}
//
//		if (position[0] > 11 || position[0] < 0) {
//			return true;
//		} else if (position[1] > 11 || position[1] < 0) {
//			return true;
//		}
//
//		return false;
//	}

	/**
	 * checks for collision by a given position and the playerlist.
	 * 
	 * @param playerlist
	 *             list of all the players
	 * @param position
	 *             int array contains the position a player will be moved to.
	 * @return (-1) if no player is in the way. return the players spawn number you
	 *         collided with.
	 */
	private int checkCollision(ArrayList<Player> playerlist, int [] position) {
	int collideWith;
		for (Player player : playerlist) {
			if (Arrays.equals(player.getPos(), position)) {
				return collideWith = player.getSpawnNumber();
			} else {
				continue;
			}
		}
		return collideWith = -1;
	}

	/**
	 * checks if walls are in the way of a players movement
	 * 
	 * @param PosX
	 *             players x position
	 * @param PosY
	 *             player y position
	 * @param dir
	 *             players face direction
	 * @param gb
	 *             gameboard
	 * @return  true if there is a wall on the current field or on the next field
	 *         in in the direction of movement.
	 */
	private boolean CheckForWalls(int PosX, int PosY, Direction dir, GameBoard gb) {
		boolean WallsInTheWay = false;
		int x = PosX;
		int y = PosY;
	
		
		if (dir.equals(Direction.NORTH)) {
			
			if (outsideBoard (x ,y - 1) ) {
				if (gb.fieldWallsAt(x, y, false)[0] == true) {
					return WallsInTheWay = true;
				} else {
					return WallsInTheWay = false;
				}
			} else {
				if (gb.fieldWallsAt(x, y, false)[0] == true || gb.fieldWallsAt(x, y - 1, false)[2] == true) {
					return WallsInTheWay = true;
				} else {
					return WallsInTheWay = false;
				}
			}
		} else if (dir.equals(Direction.EAST)) {
			if (outsideBoard(x + 1, y) || outsideBoard(x , y)) {
			if (gb.fieldWallsAt(x, y, false)[1] == true ) {
				return WallsInTheWay = true;
			} else {
				return WallsInTheWay = false;
			}
		} else {
			if (gb.fieldWallsAt(x, y, false)[1] == true || gb.fieldWallsAt(x + 1, y, false)[3] == true) {
				return WallsInTheWay = true;
			} else {
				return WallsInTheWay = false;
			}
		}
		} else if (dir.equals(Direction.SOUTH)) {
				if (outsideBoard(x, y + 1) || outsideBoard(x , y)) {
				if (gb.fieldWallsAt(x, y, false)[2] == true ) {
					return WallsInTheWay = true;
				} else {
					return WallsInTheWay = false;
				}
				} else {
					if (gb.fieldWallsAt(x, y, false)[2] == true || gb.fieldWallsAt(x, y + 1, false)[0] == true) {
						return WallsInTheWay = true;
					} else {
						return WallsInTheWay = false;
					}	
				}
		
		} else if (dir.equals(Direction.WEST)) {
			if (outsideBoard(x - 1, y) || outsideBoard(x , y)) {
				if (gb.fieldWallsAt(x, y, false)[3] == true ) {
					return WallsInTheWay = true;
				} else {
					return WallsInTheWay = false;
				}
				} else {
					if (gb.fieldWallsAt(x, y, false)[2] == true || gb.fieldWallsAt(x - 1, y, false)[0] == true) {
						return WallsInTheWay = true;
					} else {
						return WallsInTheWay = false;
					}	
				}
		}

		return true;
	}

	/**
	 * Moves player one field north.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             gameboard
	 * @param playerlist
	 *             all the players
	 * @param rotateRight
	 *             is the player on field that turns him to the right?
	 * @param rotateLeft
	 *             is the player on field that turns him to the left?
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> moveNorth(Player player, GameBoard gb, ArrayList<Player> playerlist,
			boolean rotateRight, boolean rotateLeft) {
		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();
		ArrayList<Object> CollidedWith = new ArrayList();
		Direction newDirection = null;
		int[] moveSecondPlayer = new int[2];
		int[] newPosition = new int[2];
		int spawnNumber = -1;
		int damageTaken = 0;
		int x;
		int y;
		boolean CollisionHappened = false;

		x = player.getPos()[0];
		y = player.getPos()[1];
		
		if (CheckForWalls(x, y, Direction.NORTH, gb)) { // check if the main player is blocked by a wall --> true
			newPosition[0] = x;
			newPosition[1] = y;
			if (rotateRight == true) {
				newDirection = (Direction) rotateRight(player).get(0).get(1);
			} else if (rotateLeft == true) {
				newDirection = (Direction) rotateLeft(player).get(0).get(1);
			} else {
				newDirection = player.getFaceDirection();
			}

			CollisionHappened = false;

		} else { // check if the main player is blocked by a wall --> false
			x = player.getPos()[0];
			y = player.getPos()[1] - 1;
			newPosition[0] = x;
			newPosition[1] = y;
			if (checkCollision(playerlist, newPosition) != -1) { // check for collision with another player --> true
				if (CheckForWalls(player.getPos()[0], player.getPos()[1] - 1, Direction.NORTH, gb)) { // check if
																										// collided
																										// player is
																										// blocked by a
																										// wall --> true
					x = player.getPos()[0];
					y = player.getPos()[1];
					newPosition[0] = x;
					newPosition[1] = y;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
				} else { // check if collided player is blocked by a wall --> false
					x = player.getPos()[0]; // set new Position for main player
					y = player.getPos()[1] - 1;
					newPosition[0] = x;
					newPosition[1] = y;

					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
				}

			} else { // check for collision with another player --> false
				x = player.getPos()[0];
				y = player.getPos()[1] - 1;
				newPosition[0] = x;
				newPosition[1] = y;
				if (rotateRight == true) {
					newDirection = (Direction) rotateRight(player).get(0).get(1);
				} else if (rotateLeft == true) {
					newDirection = (Direction) rotateLeft(player).get(0).get(1);
				} else {
					newDirection = player.getFaceDirection();
				}
				CollisionHappened = false;
			}
		}

		// correct order in which parameters are added to the main list
		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);

		afterMove.add(MovingPlayer);

		if (MovingPlayer.get(2).equals(true)
				&& !CheckForWalls(player.getPos()[0], player.getPos()[1] - 1, Direction.NORTH, gb)) {
			moveSecondPlayer[0] = player.getPos()[0];
			moveSecondPlayer[1] = player.getPos()[1] - 2;
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		} else if (MovingPlayer.get(2).equals(true)
				&& CheckForWalls(player.getPos()[0], player.getPos()[1] - 1, Direction.NORTH, gb)) {
			moveSecondPlayer[0] = player.getPos()[0];
			moveSecondPlayer[1] = player.getPos()[1] - 1;
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		}

		return afterMove;

	}

	/**
	 * Moves player one field east.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             gameboard
	 * @param playerlist
	 *             all the players
	 * @param rotateRight
	 *             is the player on field that turns him to the right?
	 * @param rotateLeft
	 *             is the player on field that turns him to the left?
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> moveEast(Player player, GameBoard gb, ArrayList<Player> playerlist,
			boolean rotateRight, boolean rotateLeft) {
		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();
		ArrayList<Object> CollidedWith = new ArrayList();
		Direction newDirection = null;
		int[] moveSecondPlayer = new int[2];
		int[] newPosition = new int[2];
		int spawnNumber = -1;
		int damageTaken = 0;
		int x;
		int y;
		boolean CollisionHappened = false;

		x = player.getPos()[0];
		y = player.getPos()[1];
		if (CheckForWalls(x, y, Direction.EAST, gb)) { // check if the main player is blocked by a wall --> true
			newPosition[0] = x;
			newPosition[1] = y;
			if (rotateRight == true) {
				newDirection = (Direction) rotateRight(player).get(0).get(1);
			} else if (rotateLeft == true) {
				newDirection = (Direction) rotateLeft(player).get(0).get(1);
			} else {
				newDirection = player.getFaceDirection();
			}
			CollisionHappened = false;

		} else { // check if the main player is blocked by a wall --> false
			x = player.getPos()[0] + 1;
			y = player.getPos()[1];
			newPosition[0] = x;
			newPosition[1] = y;
			if (checkCollision(playerlist, newPosition) != -1) { // check for collision with another player --> true
				if (CheckForWalls(player.getPos()[0] + 1, player.getPos()[1], Direction.EAST, gb)) { // check if
																										// collided
																										// player is
																										// blocked by a
																										// wall --> true
					x = player.getPos()[0];
					y = player.getPos()[1];
					newPosition[0] = x;
					newPosition[1] = y;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
				} else {
					x = player.getPos()[0] + 1; // set new Position for main player
					y = player.getPos()[1];
					newPosition[0] = x;
					newPosition[1] = y;
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
				}

			} else { // check for collision with another player --> false
				x = player.getPos()[0] + 1;
				y = player.getPos()[1];
				newPosition[0] = x;
				newPosition[1] = y;
				if (rotateRight == true) {
					newDirection = (Direction) rotateRight(player).get(0).get(1);
				} else if (rotateLeft == true) {
					newDirection = (Direction) rotateLeft(player).get(0).get(1);
				} else {
					newDirection = player.getFaceDirection();
				}
				CollisionHappened = false;
			}
		}

		// correct order in which parameters are added to the main list
		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);

		afterMove.add(MovingPlayer);

		if (MovingPlayer.get(2).equals(true)
				&& !CheckForWalls(player.getPos()[0] + 1, player.getPos()[1], Direction.EAST, gb)) {
			moveSecondPlayer[0] = player.getPos()[0] + 2;
			moveSecondPlayer[1] = player.getPos()[1];
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		} else if (MovingPlayer.get(2).equals(true)
				&& CheckForWalls(player.getPos()[0] + 1, player.getPos()[1], Direction.EAST, gb)) {
			moveSecondPlayer[0] = player.getPos()[0] + 1;
			moveSecondPlayer[1] = player.getPos()[1];
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		}

		return afterMove;

	}

	/**
	 * Moves player one field south.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             gameboard
	 * @param playerlist
	 *             all the players
	 * @param rotateRight
	 *             is the player on field that turns him to the right?
	 * @param rotateLeft
	 *            is the player on field that turns him to the left?
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> moveSouth(Player player, GameBoard gb, ArrayList<Player> playerlist,
			boolean rotateRight, boolean rotateLeft)  {
		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();
		ArrayList<Object> CollidedWith = new ArrayList();
		Direction newDirection = null;
		int[] moveSecondPlayer = new int[2];
		int[] newPosition = new int[2];
		int spawnNumber = -1;
		int damageTaken = 0;
		int x;
		int y;
		boolean CollisionHappened = false;

		x = player.getPos()[0];
		y = player.getPos()[1];
		if (CheckForWalls(x, y, Direction.SOUTH, gb)) { // check if the main player is blocked by a wall --> true
			newPosition[0] = x;
			newPosition[1] = y;
			if (rotateRight == true) {
				newDirection = (Direction) rotateRight(player).get(0).get(1);
			} else if (rotateLeft == true) {
				newDirection = (Direction) rotateLeft(player).get(0).get(1);
			} else {
				newDirection = player.getFaceDirection();
			}
			CollisionHappened = false;

		} else { // check if the main player is blocked by a wall --> false
			x = player.getPos()[0];
			y = player.getPos()[1] + 1;
			newPosition[0] = x;
			newPosition[1] = y;
			if (checkCollision(playerlist, newPosition) != -1) { // check for collision with another player --> true
				if (CheckForWalls(player.getPos()[0], player.getPos()[1] + 1, Direction.SOUTH, gb)) { // check if
																										// collided
																										// player is
																										// blocked by a
																										// wall --> true
					x = player.getPos()[0];
					y = player.getPos()[1];
					newPosition[0] = x;
					newPosition[1] = y;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
				} else { // check if collided player is blocked by a wall --> false
					x = player.getPos()[0]; // set new Position for main player
					y = player.getPos()[1] + 1;
					newPosition[0] = x;
					newPosition[1] = y;
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
				}

			} else { // check for collision with another player --> false
				x = player.getPos()[0];
				y = player.getPos()[1] + 1;
				newPosition[0] = x;
				newPosition[1] = y;
				if (rotateRight == true) {
					newDirection = (Direction) rotateRight(player).get(0).get(1);
				} else if (rotateLeft == true) {
					newDirection = (Direction) rotateLeft(player).get(0).get(1);
				} else {
					newDirection = player.getFaceDirection();
				}
				CollisionHappened = false;
			}
		}

		// correct order in which parameters are added to the main list
		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);

		afterMove.add(MovingPlayer);

		if (MovingPlayer.get(2).equals(true)
				&& !CheckForWalls(player.getPos()[0], player.getPos()[1] + 1, Direction.SOUTH, gb)) {
			moveSecondPlayer[0] = player.getPos()[0];
			moveSecondPlayer[1] = player.getPos()[1] + 2;
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		} else if (MovingPlayer.get(2).equals(true)
				&& CheckForWalls(player.getPos()[0], player.getPos()[1] + 1, Direction.SOUTH, gb)) {
			moveSecondPlayer[0] = player.getPos()[0];
			moveSecondPlayer[1] = player.getPos()[1] + 1;
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		}

		return afterMove;

	}

	/**
	 * Moves player one field west.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             gameboard
	 * @param playerlist
	 *             all the players
	 * @param rotateRight
	 *             is the player on field that turns him to the right?
	 * @param rotateLeft
	 *             is the player on field that turns him to the left?
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> moveWest(Player player, GameBoard gb, ArrayList<Player> playerlist,
			boolean rotateRight, boolean rotateLeft)  {
		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();
		ArrayList<Object> CollidedWith = new ArrayList();
		Direction newDirection = null;
		int[] moveSecondPlayer = new int[2];
		int[] newPosition = new int[2];
		int spawnNumber = -1;
		int damageTaken = 0;
		int x;
		int y;
		boolean CollisionHappened = false;

		x = player.getPos()[0];
		y = player.getPos()[1];
		if (CheckForWalls(x, y, Direction.WEST, gb)) { // check if the main player is blocked by a wall --> true
			newPosition[0] = x;
			newPosition[1] = y;
			if (rotateRight == true) {
				newDirection = (Direction) rotateRight(player).get(0).get(1);
			} else if (rotateLeft == true) {
				newDirection = (Direction) rotateLeft(player).get(0).get(1);
			} else {
				newDirection = player.getFaceDirection();
			}
			CollisionHappened = false;

		} else { // check if the main player is blocked by a wall --> false
			x = player.getPos()[0] - 1;
			y = player.getPos()[1];
			newPosition[0] = x;
			newPosition[1] = y;
			if (checkCollision(playerlist, newPosition) != -1) { // check for collision with another player --> true
				if (CheckForWalls(player.getPos()[0] - 1, player.getPos()[1], Direction.WEST, gb)) { // check if
																										// collided
																										// player is
																										// blocked by a
																										// wall --> true
					x = player.getPos()[0];
					y = player.getPos()[1];
					newPosition[0] = x;
					newPosition[1] = y;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
				} else {
					x = player.getPos()[0] - 1; // set new Position for main player
					y = player.getPos()[1];
					newPosition[0] = x;
					newPosition[1] = y;
					CollisionHappened = true; // set new parameters for collided with player
					spawnNumber = checkCollision(playerlist, newPosition);
					damageTaken = damageTaken + 1;
					if (rotateRight == true) {
						newDirection = (Direction) rotateRight(player).get(0).get(1);
					} else if (rotateLeft == true) {
						newDirection = (Direction) rotateLeft(player).get(0).get(1);
					} else {
						newDirection = player.getFaceDirection();
					}
				}

			} else { // check for collision with another player --> false
				x = player.getPos()[0] - 1;
				y = player.getPos()[1];
				newPosition[0] = x;
				newPosition[1] = y;
				if (rotateRight == true) {
					newDirection = (Direction) rotateRight(player).get(0).get(1);
				} else if (rotateLeft == true) {
					newDirection = (Direction) rotateLeft(player).get(0).get(1);
				} else {
					newDirection = player.getFaceDirection();
				}
				CollisionHappened = false;
			}
		}

		// correct order in which parameters are added to the main list
		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);

		afterMove.add(MovingPlayer);

		if (MovingPlayer.get(2).equals(true)
				&& !CheckForWalls(player.getPos()[0] - 1, player.getPos()[1], Direction.WEST, gb)) {
			moveSecondPlayer[0] = player.getPos()[0] - 2;
			moveSecondPlayer[1] = player.getPos()[1];
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		} else if (MovingPlayer.get(2).equals(true)
				&& CheckForWalls(player.getPos()[0] - 1, player.getPos()[1], Direction.WEST, gb)) {
			moveSecondPlayer[0] = player.getPos()[0] + 1;
			moveSecondPlayer[1] = player.getPos()[1];
			CollidedWith.add(spawnNumber);
			CollidedWith.add(moveSecondPlayer);
			CollidedWith.add(damageTaken);
			afterMove.add(CollidedWith);
		}

		return afterMove;

	}

	/**
	 * rotates the player to the left
	 * 
	 * @param player
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> rotateLeft(Player player) {

		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();
		Direction newDirection = null;
		int[] newPosition = new int[2];
		boolean CollisionHappened = false;
		newPosition[0] = player.getPos()[0];
		newPosition[1] = player.getPos()[1];

		if (player.getFaceDirection().equals(Direction.NORTH)) {
			newDirection = Direction.WEST;
		} else if (player.getFaceDirection().equals(Direction.EAST)) {
			newDirection = Direction.NORTH;
		} else if (player.getFaceDirection().equals(Direction.SOUTH)) {
			newDirection = Direction.EAST;
		} else if (player.getFaceDirection().equals(Direction.WEST)) {
			newDirection = Direction.SOUTH;
		}

		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);
		afterMove.add(MovingPlayer);
		return afterMove;
	}

	/**
	 * rotates the player to the right
	 * 
	 * @param player
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> rotateRight(Player player) {

		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();

		Direction newDirection = null;

		int[] newPosition = new int[2];
		boolean CollisionHappened = false;

		CollisionHappened = false;
		newPosition[0] = player.getPos()[0];
		newPosition[1] = player.getPos()[1];

		if (player.getFaceDirection().equals(Direction.NORTH)) {
			newDirection = Direction.EAST;
		} else if (player.getFaceDirection().equals(Direction.EAST)) {
			newDirection = Direction.SOUTH;
		} else if (player.getFaceDirection().equals(Direction.SOUTH)) {
			newDirection = Direction.WEST;
		} else if (player.getFaceDirection().equals(Direction.WEST)) {
			newDirection = Direction.NORTH;
		}

		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);
		afterMove.add(MovingPlayer);
		return afterMove;
	}

	/**
	 * Moves player one field backwards.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             gameboard
	 * @param playerlist
	 *             all the players
	 * @param rotateRight
	 *             is the player on field that turns him to the right?
	 * @param rotateLeft
	 *             is the player on field that turns him to the left?
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> backUp(Player player, ArrayList<Player> playerlist, GameBoard gb) {

		if (player.getFaceDirection().equals(Direction.NORTH)) {
			return moveSouth(player, gb, playerlist, false, false);
		} else if (player.getFaceDirection().equals(Direction.EAST)) {
			return moveWest(player, gb, playerlist, false, false);
		} else if (player.getFaceDirection().equals(Direction.SOUTH)) {
			return moveNorth(player, gb, playerlist, false, false);
		} else {
			return moveEast(player, gb, playerlist, false, false);
		}

	}

	/**
	 * rotates the player 180�
	 * 
	 * @param player
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> uTurn(Player player) {

		List<ArrayList<Object>> afterMove = new ArrayList();
		ArrayList<Object> MovingPlayer = new ArrayList();
		Direction newDirection = null;
		int[] newPosition = new int[2];
		boolean CollisionHappened = false;
		newPosition[0] = player.getPos()[0];
		newPosition[1] = player.getPos()[1];

		if (player.getFaceDirection().equals(Direction.NORTH)) {
			newDirection = Direction.SOUTH;
		} else if (player.getFaceDirection().equals(Direction.EAST)) {
			newDirection = Direction.WEST;
		} else if (player.getFaceDirection().equals(Direction.SOUTH)) {
			newDirection = Direction.NORTH;
		} else if (player.getFaceDirection().equals(Direction.WEST)) {
			newDirection = Direction.EAST;
		}

		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);
		afterMove.add(MovingPlayer);
		return afterMove;
	}

	/**
	 * if the player is a non-moving gameboard element. everything remains the same.
	 * 
	 * @param player
	 *             player who needs to be checked
	 * @param gb
	 *             gameboard
	 * @param playerlist
	 *             all the players
	 * @return  An array list containing information about the players new position
	 *         and if he collided with someone Here is an example : ArrayList
	 *         (ArrayList PlayerNewInformation(newPosition[newX, newY],
	 *         newFacingDirection, boolean collisionHappened),
	 *         PlayerWhoWasCollided(int spawnNumber,newPosition[newX,newY],
	 *         amountOfDamageTaken))
	 */
	private List<ArrayList<Object>> remainPosition(Player player, GameBoard gb, ArrayList<Player> playerlist) {
		List<ArrayList<Object>> afterMove = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> MovingPlayer = new ArrayList();
		Direction newDirection = null;
		int[] newPosition = new int[2];
		boolean CollisionHappened = false;

		newPosition[0] = player.getPos()[0];
		newPosition[1] = player.getPos()[1];
		newDirection = player.getFaceDirection();

		// correct order in which parameters are added to the main list
		MovingPlayer.add(newPosition);
		MovingPlayer.add(newDirection);
		MovingPlayer.add(CollisionHappened);

		afterMove.add(MovingPlayer);

		return afterMove;

	}
	
	private boolean outsideBoard (int x , int y ) {
		boolean outsideBoard;
		if (x > 11 || x < 0 || y < 0 || y > 11) {
			return outsideBoard = true;
		} else {
			return outsideBoard = false;
		}

	}
	


}
