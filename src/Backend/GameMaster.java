package Backend;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import Enums.CardType;
import Enums.Direction;
import Enums.FieldType;
import Exceptions.AmountOfPlayersException;
import Exceptions.AmountOfProgramCardsException;
import Exceptions.BotException;
import Exceptions.DestructionException;
import Exceptions.GameOverException;
import Exceptions.IllegalAmountOfDamageTokensException;
import Exceptions.IllegalParameterException;
import Exceptions.IllegalPositionException;
import Exceptions.IllegalProgramCardException;
import Exceptions.IllegalProgramException;
import Exceptions.IllegalProgramSizeException;
import Exceptions.IllegalRegisterPhaseException;
import Interface.iController;
import Interface.iDataAccess;
import Streams.DataAccessCSV;
import Streams.DataAccessJSON;
import Streams.DataAccessSER;

/**
 * Class GameMaster The Executer of all methods
 * 
 * @author Filip
 *
 */

public class GameMaster implements iController, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RuleBook rulebook = new RuleBook();
	private RuleBook rulebookdm = new RuleBookDM();
	private ArrayList<ProgramCard> deck = new ArrayList<ProgramCard>();
	private ArrayList<Player> playerList = new ArrayList<Player>();
	private GameBoard gameboard = new GameBoard();
	private int registerPhase = 0;
	private LinkedList<Integer> SpawnPoints = new LinkedList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
	private int whichPlayer = 0;
	private Player actualPlayer;
	private ArrayList<Player> turnOrder = new ArrayList<Player>();
	private ArrayList<Player> destroyedPlayer = new ArrayList<Player>();
	private ArrayList<String> allPlayerMovementLog = new ArrayList();
	/**
	 * This method creates the deck once, at the start of every game
	 */
	
	
	public GameMaster() {
		//turnOrder = playerList;
	}

	public void createDeck(ArrayList<ProgramCard> emptyDeck) {
		for (int i = 10; i <= 60; i += 10) {
			emptyDeck.add(new ProgramCard(i, CardType.U_TURN));
		}
		for (int i = 70; i <= 410; i += 20) {
			emptyDeck.add(new ProgramCard(i, CardType.ROTATE_LEFT));
		}
		for (int i = 80; i <= 420; i += 20) {
			emptyDeck.add(new ProgramCard(i, CardType.ROTATE_RIGHT));
		}
		for (int i = 430; i <= 480; i += 10) {
			emptyDeck.add(new ProgramCard(i, CardType.BACK_UP));
		}
		for (int i = 490; i <= 660; i += 10) {
			emptyDeck.add(new ProgramCard(i, CardType.MOVE_ONE));
		}
		for (int i = 670; i <= 780; i += 10) {
			emptyDeck.add(new ProgramCard(i, CardType.MOVE_TWO));
		}
		for (int i = 790; i <= 840; i += 10) {
			emptyDeck.add(new ProgramCard(i, CardType.MOVE_THREE));

		}
	}

	/**
	 * This method shuffles the Deck.
	 */

	public void shuffleDeck(ArrayList<ProgramCard> theDeck) {
		Collections.shuffle(theDeck);
	}

	/**
	 * Each player starts at a different Spawnpoint which will be decided by a
	 * random.
	 * 
	 * @return Spawnpointnumber as int
	 */

	public int[] randomPlayerNumber(LinkedList<Integer> SpawnPoints, RuleBook rulebook) {
		if(rulebook instanceof RuleBookDM) {
			Random rand = new Random();
			int[] random = {SpawnPoints.get(rand.nextInt(SpawnPoints.size())), 0, 0};
			random[1] = rand.nextInt((11)+1);
			random[2] = rand.nextInt((11)+1);
			SpawnPoints.remove((Integer) random[0]);
			return random;
		} else {
		Random rand = new Random();
		int[] random = {SpawnPoints.get(rand.nextInt(SpawnPoints.size())), 0, 0};
		random[1] = gameboard.getSpawnPos(random[0])[0];
		random[2] = gameboard.getSpawnPos(random[0])[1];
		SpawnPoints.remove((Integer) random[0]);
		return random;
		}
	}

	/**
	 * This method adds or deletes the chosen token of a chosen player. The method
	 * is called after every end of a round.
	 * 
	 * @param one
	 *            existing player
	 * @param lifeTokens
	 *            between 0 and 1 (can�t delete more than 1 Token at once)
	 * @param dmgTokens
	 *            between 0 and 5
	 * @param powerDownTokens
	 *            is either true or false, depends on the players decision
	 * @param secondPowerDown
	 *            is either true or false, depends on the players decision after the
	 *            first power down
	 */

	public void dealTokens(Player player, int lifeTokens, int dmgTokens, boolean powerDownTokens,
			boolean secondPowerDown) {
		player.setDamageTokens(player.getDamageTokens() + dmgTokens);
		player.setLifeTokens(player.getLifeTokens() - lifeTokens);
		player.setPowerDownToken(powerDownTokens);
		player.setSecondPowerdownToken(secondPowerDown);
	}

	/**
	 * This Method deals every player their amount of ProgramCards they are allowed
	 * to get. The amount will be checked in the rulebook, for every player in
	 * playerList, then a random card will be removed from the deck and added to the
	 * temp ArrayList. This ArrayList will be set as the actual hand. After the set,
	 * the ArrayList will be cleared.
	 */

	public void dealProgramCards(ArrayList<Player> playerList, ArrayList<ProgramCard> deck) {
		for (Player player : playerList) {
			ArrayList<ProgramCard> hand = new ArrayList<ProgramCard>();
			int a = 8;
			try {
				a = rulebook.amountProgramCards(player);
			} catch (IllegalAmountOfDamageTokensException | DestructionException | AmountOfProgramCardsException e1) {
				System.err.println(e1);
				e1.printStackTrace();
			}
			for (int i = 0; i < a; i++) {
				int b = (int) Math.random() * deck.size();
				hand.add(deck.get(b));
				deck.remove(b);
			}
			player.setProgramCards(hand);
			try {
				rulebook.checkAmountOfProgramCards(player);
			} catch (AmountOfProgramCardsException e) {
				System.err.println(e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method repairs a robot for the amount of 1 or 2 dmgTokens, dependent on
	 * the repairField. If someone tries to repair the robot on non-repairField if
	 * will repair nothing. It will always launch after the last register phase
	 * 
	 * @param a
	 *            playerList with all players in it will be given
	 */

	public void repairRobot(ArrayList<Player> playerList, int registerPhase, GameBoard gameboard) {
		for (Player player : playerList) {
			try {
				if (rulebook.checkRepair(player, registerPhase, gameboard) == 1) {
					System.out.println("calling deal tokens in repairRobot for player: " +player.getName() + " This is because he is getting repaired.");
					dealTokens(player, 0, -1, false, false);
				} else if (rulebook.checkRepair(player, registerPhase, gameboard) == 2) {
					System.out.println("calling deal tokens in repairRobot for player: " +player.getName() + " This is because he is getting repaired.");
					dealTokens(player, 0, -2, false, false);
				} else {
					System.out.println("calling deal tokens in repairRobot for player: " +player.getName() + " This is because he is getting repaired.");
					dealTokens(player, 0, 0, false, false);
				}
			} catch (NullPointerException | IllegalRegisterPhaseException | IllegalAmountOfDamageTokensException e) {
				System.err.println(e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method clears the register of every player and sends the cards back to
	 * the deck.
	 * 
	 * @param a
	 *            playerList with all players in it will be given
	 */

	public void wipeRegister(ArrayList<Player> playerList) {
		for (Player player : playerList) {
			deck.addAll(player.getProgram());
			try {
				player.clearRegisters();
			} catch (NullPointerException | IllegalProgramSizeException e) {
				System.err.println(e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method sets up the board for the next round (1 round = 5 register
	 * phases). If a player is poweredDown, dependent if first or second power down,
	 * he will either get repair for full health or repair for full health -2
	 * DamageTokens. If it is his first powerDown he can decide if he wants to
	 * powerDown for another turn. If a player was destroyed it will take one
	 * lifeToken and then check if he has any left. If not he will be kicked out of
	 * the game. If he has any left he can choose the facing direction when he
	 * respawns.
	 * 
	 * @param a
	 *            playerList with all players in it will be given
	 */

	public void setupForNextTurn(ArrayList<Player> playerList) {
		ArrayList<Player> toRemove = new ArrayList<Player>();
		for (Player player : playerList) {
			try {
				if (player.isPowerDownToken()) {
					String decide = "No"; // Spieler auswahl
					if (decide.equals("No")) {
						System.out.println("calling deal tokens in setupForNextTurn for player: " +player.getName() + " This is because he is powered down.");
						dealTokens(player, 0, -2, false, false);
					} else {
						System.out.println("calling deal tokens in setupForNextTurn for player: " +player.getName() + " This is because he is powered down.");
						dealTokens(player, 0, 0, false, true);
					}
				} else if (player.isSecondPowerdownToken()) {
					player.setDamageTokens(0);
					player.setPowerDownToken(false);
					player.setSecondPowerdownToken(false);
				} else if (rulebook.checkDestruction(player, gameboard)) {
					dealTokens(player, 1, 0, false, false);
					player.setDamageTokens(0);
					if (rulebook.checkDeath(player)) {
						toRemove.add(player);

					} else {
						System.out.println(player.getName() + ": calling setPos in setupForNextTurn with player.getArchiveTokenPos()[0]=" + player.getArchiveTokenPos()[0] + " player.getArchiveTokenPos()[1]=" + player.getArchiveTokenPos()[1]);
						player.setPos(player.getArchiveTokenPos());
					}
				}
			} catch (NullPointerException e) {
				System.err.println(e);
				e.printStackTrace();
			}
		}
		playerList.removeAll(toRemove);
	}
	
	
	
	public void isDestroyed(ArrayList<Player> playerList, ArrayList<Player> destroyedPlayer, GameBoard gameboard) {
		ArrayList<Player> toRemove = new ArrayList<Player>();
		for (Player player : playerList) {
			if (rulebook.checkDestruction(player, gameboard)) {
				System.out.println("calling deal tokens in isDestroyed for player: " +player.getName() + " This is because he is respawned.");
				dealTokens(player, 1, -(player.getDamageTokens()), false, false);
				player.setDamageTokens(0);
				if(rulebook.checkDeath(player)) {
					toRemove.add(player);
				} else {
					destroyedPlayer.add(player);
				}
				
			}
		}playerList.removeAll(toRemove);
		playerList.removeAll(destroyedPlayer);
	}

	/**
	 * This method is doing four things after the last register phase (register 5)
	 * finished. 1. It repairs every robot which stands on a wrench space discard
	 * (the amount is decided in the rulebook). 2. The register of every player is
	 * wiped out and the cards return back to the deck. 3. Every player who placed a
	 * PowerDownToken or got destroyed will be repaired or powered up if wanted. As
	 * last step the whichPlayer indicator will be set 0. After the three steps, the
	 * next turn will start.
	 */

	public void cleanUp() {
		repairRobot(playerList, registerPhase, gameboard);
		wipeRegister(playerList);
		setupForNextTurn(playerList);
		whichPlayer = 0;
	}

	/**
	 * This method executes the movement for each player. It will execute the chosen
	 * ProgramCard for the actuall registerPhase. It also gives DamageTokens if one
	 * Player collides with another.
	 * 
	 * @param inOrder
	 *            gives a ArrayList with the players in the order of their Cards
	 *            priorityNumber
	 * @param registerPhase
	 *            will give the actuall registerPhase
	 */

	public void movePlayer(ArrayList<Player> inOrder, int registerPhase, GameBoard gameboard) {
		List<ArrayList<Object>> inhalt;
		String dockingbayName = "dockingBay";
		String mapName = "dizzyDash";
		for (Player player : inOrder) {

			if (!player.isPowerDownToken() || !player.isSecondPowerdownToken()) {
				if (player.getProgram().get(registerPhase) == null) {
					return;
				}
				ProgramCard pc = player.getProgram().get(registerPhase);
				int a = rulebook.loopAmountProgramCard(player, pc);
				for (int i = 0; i < a; i++) {
					try {
						inhalt = rulebook.checkMovement(player, pc, inOrder, gameboard);
						int[] Pos = (int[]) inhalt.get(0).get(0);
						Direction wohinSchaust = (Direction) inhalt.get(0).get(1);
						boolean kollision = (boolean) inhalt.get(0).get(2);
						System.out.println(player.getName() + ": calling setPos in movePlayer with Pos[0]=" + Pos[0] + " Pos[1]=" + Pos[1]);
						//check if play is applicable for gameBoard transition
						if(Pos[1] < 0 && player.getActiveMap().equals(dockingbayName)) {
							player.setPos(Pos[0], 11);
							player.setActiveMap(mapName);
						}
						if(Pos[1] > 11 && player.getActiveMap().equals(mapName)){
							player.setPos(Pos[0], 0);
							player.setActiveMap(dockingbayName);
						}else {
							player.setPos(Pos[0], Pos[1]);
						}
						player.setFaceDirection(wohinSchaust);
						if (kollision == true) {
							int spawnPunktCollidPlayer = (int) inhalt.get(1).get(0);
							int posCollPlayer[] = (int[]) inhalt.get(1).get(1);
							int collDmgToken = (int) inhalt.get(1).get(2);
							for (Player collPlayer : playerList) {
								if (collPlayer.getSpawnNumber() == spawnPunktCollidPlayer) {
									System.out.println(player.getName() + ": calling setPos in movePlayer with posCollPlayer[0]=" + posCollPlayer[0] + " posCollPlayer[1]=" + posCollPlayer[1]);
									collPlayer.setPos(posCollPlayer[0], posCollPlayer[1]);
									System.out.println("calling deal tokens in move player for player: " +player.getName() + " This is for a collision");
									dealTokens(collPlayer, 0, collDmgToken, collPlayer.isPowerDownToken(),
											collPlayer.isSecondPowerdownToken());
								}
							}
						}
					} catch (Exception e) {
						System.err.println(e);
						e.printStackTrace();
					}
					allPlayerMovementLog.add(getAllPlayerMapInfo());
				}
			}
			try {
				int dmgToken = rulebook.damageByLaser(player, gameboard);
				System.out.println("calling deal tokens in move player for player: " +player.getName() + " This is for a laser");
				dealTokens(player, 0, dmgToken, player.isPowerDownToken(), player.isSecondPowerdownToken());
			} catch(Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		}
	}

	/**
	 * This method is nearly the same as movePlayer, but this time it will move not
	 * based on the ProgramCard but on the fieldType on which each player stands. It
	 * also gives DamageTokens if one Player collides with another.
	 * 
	 * @param inOrder
	 *            gives a ArrayList with the players in the order of their Cards
	 *            priorityNumber
	 */

	public void movedByBoard(ArrayList<Player> inOrder, GameBoard gb) {
		for (Player momPlayer : inOrder) {
			int a = rulebook.loopAmountBoardElement(momPlayer, gb);
			for (int i = 0; i < a; i++) {
				try {
					List<ArrayList<Object>> inhalt = rulebook.movedByBoardElement(momPlayer, playerList, gameboard);
					ArrayList<Object> playerInhalt = inhalt.get(0);
					int[] momPlayerPos = (int[]) playerInhalt.get(0);
					Enums.Direction momPlayerDir = (Direction) playerInhalt.get(1);
					System.out.println(momPlayer.getName() + ": calling setPos in movedByBoard with momPlayerPos[0]=" + momPlayerPos[0] + " momPlayerPos[1]=" + momPlayerPos[1]);
					momPlayer.setPos(momPlayerPos[0], momPlayerPos[1]);
					momPlayer.setFaceDirection(momPlayerDir);
					boolean kollision = (boolean) inhalt.get(0).get(2);
					if (kollision == true) {
						int spawnPunktCollidPlayer = (int) inhalt.get(1).get(0);
						int posCollPlayer[] = (int[]) inhalt.get(1).get(1);
						int collDmgToken = (int) inhalt.get(1).get(2);
						for (Player collPlayer : playerList) {
							if (collPlayer.getSpawnNumber() == spawnPunktCollidPlayer) {
								System.out.println("calling setPos in movedByBoard with posCollPlayer[0]=" + posCollPlayer[0] + " posCollPlayer[1]=" + posCollPlayer[1]);
								collPlayer.setPos(posCollPlayer[0], posCollPlayer[1]);
								System.out.println("calling deal tokens in movedByBoard for player: " +momPlayer.getName() + " This is for a collision");
								dealTokens(collPlayer, 0, collDmgToken, collPlayer.isPowerDownToken(),
										collPlayer.isSecondPowerdownToken());
							}
						}
					}
				} catch (Exception e) {
					System.err.println(e);
					e.printStackTrace();
				}
				try {
					int dmgToken = rulebook.damageByLaser(momPlayer, gameboard);
					System.out.println("calling deal tokens in move player for player: " +momPlayer.getName() + " This is for a laser");
					dealTokens(momPlayer, 0, dmgToken, momPlayer.isPowerDownToken(), momPlayer.isSecondPowerdownToken());
				} catch(Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * This method fires the laser from each roboter at the end of every phase. If
	 * it hits another player he will take one DamageToken and inform both players.
	 * 
	 * @param inOrder
	 *            gives a ArrayList with the players in the order of their Cards
	 *            priorityNumber
	 */

	public void fireLaser(ArrayList<Player> inOrder, ArrayList<Player> playerList, GameBoard gameboard) {
		for (Player player : inOrder) {
			Enums.Direction laserDir = player.getFaceDirection();
			int getNextFieldx;
			int getNextFieldy;
			boolean isWall = false;
			int[] getNextField;
			switch (laserDir) {
			case NORTH:

				getNextFieldx = player.getPos()[0];
				getNextFieldy = player.getPos()[1] - 1;
				while (!isWall && getNextFieldy >= 0) {
					getNextField = new int[] { getNextFieldx, getNextFieldy };
						for (Player nextPlayer : playerList) {
							if (Arrays.equals(nextPlayer.getPos(), getNextField)) {
								System.out.println("calling deal tokens in fireLaser for player: " +player.getName() + " This is because he got shot by another player");
								dealTokens(nextPlayer, 0, 1, nextPlayer.isPowerDownToken(),
										nextPlayer.isSecondPowerdownToken());
								isWall = true;
								hitByLaser(player, nextPlayer, isWall);
							} else if (gameboard.fieldWallsAt(getNextFieldx, getNextFieldy, false)[0] == true) {
								isWall = true;
							} else {
								isWall = false;
							}
						}
						getNextFieldy--;
				}
				break;
			case EAST:

				getNextFieldx = player.getPos()[0] + 1;
				getNextFieldy = player.getPos()[1];
				while (!isWall && getNextFieldx < 12) {
					getNextField = new int[] { getNextFieldx, getNextFieldy };
						for (Player nextPlayer : playerList) {
							if (Arrays.equals(nextPlayer.getPos(), getNextField)) {
								System.out.println("calling deal tokens in fireLaser for player: " +player.getName() + " This is because he got shot by another player");
								dealTokens(nextPlayer, 0, 1, nextPlayer.isPowerDownToken(),
										nextPlayer.isSecondPowerdownToken());
								isWall = true;
								hitByLaser(player, nextPlayer, isWall);
							} else if (gameboard.fieldWallsAt(getNextFieldx, getNextFieldy, false)[1] == true) {
								isWall = true;
							} else {
								isWall = false;
							}
						}
						getNextFieldx++;
				}
				break;
			case SOUTH:

				getNextFieldx = player.getPos()[0];
				getNextFieldy = player.getPos()[1] + 1;
				while (!isWall && getNextFieldy < 12) {
					getNextField = new int[] { getNextFieldx, getNextFieldy };
						for (Player nextPlayer : playerList) {
							if (Arrays.equals(nextPlayer.getPos(), getNextField)) {
								dealTokens(nextPlayer, 0, 1, nextPlayer.isPowerDownToken(),
										nextPlayer.isSecondPowerdownToken());
								isWall = true;
								hitByLaser(player, nextPlayer, isWall);
							} else if (gameboard.fieldWallsAt(getNextFieldx, getNextFieldy, false)[2] == true) {
								isWall = true;
							} else {
								isWall = false;
							}
						}
						getNextFieldy++;
				}
				break;
			case WEST:

				getNextFieldx = player.getPos()[0] - 1;
				getNextFieldy = player.getPos()[1];
				while (!isWall && getNextFieldx >= 0) {
					getNextField = new int[] { getNextFieldx, getNextFieldy };
						for (Player nextPlayer : playerList) {
							if (Arrays.equals(nextPlayer.getPos(), getNextField)) {
								System.out.println("calling deal tokens in fireLaser for player: " +player.getName() + " This is because he got shot by another player");
								dealTokens(nextPlayer, 0, 1, nextPlayer.isPowerDownToken(),
										nextPlayer.isSecondPowerdownToken());
								isWall = true;
								hitByLaser(player, nextPlayer, isWall);
							} else if (gameboard.fieldWallsAt(getNextFieldx, getNextFieldy, false)[3] == true) {
								isWall = true;
							} else {
								isWall = false;
							}
						}
						getNextFieldx--;
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * This method checks if a player stands at the end of a phase at a repair or
	 * flag field. If this is the case he will set his archivToken at this position,
	 * where he can respawn.
	 * 
	 * @param inOrder
	 *            gives a ArrayList with the players in the order of their Cards
	 *            priorityNumber
	 */

	public void touchCheckpoints(ArrayList<Player> inOrder, GameBoard gameboard) {
		for (Player player : inOrder) {
			int[] pos = player.getPos();
			if (gameboard.fieldTypeAt(pos[0], pos[1], false).equals(Enums.FieldType.REPAIR_ONE)) {
				player.setArchiveTokenPos(pos);
			} else if (gameboard.fieldTypeAt(pos[0], pos[1], false).equals(Enums.FieldType.REPAIR_TWO)) {
				player.setArchiveTokenPos(pos);
			} else
				try {
					if (rulebook.checkFlagTouch(player, gameboard)) {
						player.setArchiveTokenPos(pos);
						player.setLastTouchedFlag(gameboard.fieldIndexAt(pos[0], pos[1], false));

					}
				} catch (NullPointerException | IndexOutOfBoundsException | GameOverException e) {
					System.err.println(e);
					e.printStackTrace();
				}
		}
	}

	/**
	 * This method sets each player on his position dependent on his chosen card. It
	 * fills the ArrayList turnOrder in the right order.
	 * 
	 * @param playerList
	 */

	public void setFirstCards(ArrayList<Player> playerList, ArrayList<Player> turnOrder, int registerPhase) {
		ArrayList<ProgramCard> firstCards = new ArrayList<ProgramCard>();
		for (Player player : turnOrder) {
			firstCards.add(player.getProgram().get(registerPhase));
		}
		firstCards = rulebook.checkPriority(firstCards);
		for (int i = 0; i < turnOrder.size(); i++) {
			for (Player player : turnOrder) {
				if (firstCards.get(i).equals(player.getProgram().get(registerPhase))) {
					turnOrder.add(player);
				}
			}
		}
		firstCards.clear();
	}

	/**
	 * This method starts one Phase and checks in the end of each if it will start a
	 * new one or if the whole turn ended.
	 */

	public void startPhase() {
		for (int i = 0; i< 5; i++) {
//			setFirstCards(playerList, turnOrder, registerPhase);
			movePlayer(playerList, registerPhase, gameboard);
			movedByBoard(playerList, gameboard);
			fireLaser(playerList, playerList, gameboard);
			isDestroyed(playerList, destroyedPlayer, gameboard);
			touchCheckpoints(playerList, gameboard);
			gameEnd();
			if (registerPhase < 4) {
				registerPhase++;
//				startPhase();
			} else {
				cleanUp();
				playerList.addAll(destroyedPlayer);
				registerPhase = 0;
				dealProgramCards(playerList, deck);
//				turnOrder.clear();
			}
		}
		
	}

	/**
	 * This method checks for each player if he has won the game or if another phase
	 * should start
	 * 
	 * @returns a String with the message, dependent if someone won or not
	 */

	public String gameEnd() {
		for (Player player : playerList) {
			if (rulebook.checkGameOver(gameboard, playerList)) {
				endGame();
				return "The game is over and the winner is" + player.getName();
			}
		}
		return "Next Phase starting";

	}

	/**
	 * This method checks and executes methods which will be executed only once in
	 * the whole game
	 * 
	 * @returns either true or false, dependent if the game has the right parameter
	 *          to start.
	 */

	public boolean oneTime() {
		if(rulebook.checkSettings(playerList)) {			
			createDeck(deck);
			return true;
		} else {
			return false;
		}
		
	}

	/**
	 * This method simply "ends" the game by removing every player from the
	 * playerList.
	 */

	public void endGame() {
		playerList.clear();
	}

	public String hitByLaser(Player fire, Player taken, boolean hit) {
		if (hit) {
			return fire.getName() + " hit " + taken.getName() + " with a laser.";
		}
		return fire.getName() + " hit noone.";
	}
	
	
	private void resetDeck() {
		for (int i =0; i< playerList.size();i++) {
			for (int e =0; e < playerList.get(i).getProgram().size();e++) {
				ProgramCard temp = playerList.get(i).getProgram().get(e);
				deck.add(temp);
			}
		}
		shuffleDeck(deck);
	}

	@Override
	public boolean startGame() {		
		if(rulebook instanceof RuleBookDM) {
			Random r = new Random();
			
			for(Player p : playerList) {
				p.setPos(0, 0);
			}
			
			for(Player p : playerList) {
				boolean canSet = true;
				int[] pos = {r.nextInt(12), r.nextInt(4)};
				
				for(Player pPosCheck : playerList) {
					if(pPosCheck.getPos()[0] == pos[0] && pPosCheck.getPos()[1] == pos[1]) {
						canSet = false;
					}
						
				}
				if(canSet) {
					p.setPos(pos);
				} else {
					p.setPos(pos[0], 2); 
				}
			}
		}
		
		if (oneTime()) {
			shuffleDeck(deck);
			dealProgramCards(playerList, deck);
			// Auswahl der Karten
			return true;
		} else {
			return false;
		}
		
	}

	@Override
	public boolean addPlayer(String name) {
		int[] playerNumAndPos = randomPlayerNumber(SpawnPoints, rulebook);
//		int a = randomPlayerNumber(SpawnPoints, rulebook)[0];
//		int x = randomPlayerNumber(SpawnPoints, rulebook)[1];
//		int y = randomPlayerNumber(SpawnPoints, rulebook)[2];
		playerList.add(new Player(playerNumAndPos[0], 3, playerNumAndPos[1], playerNumAndPos[2], name, Direction.NORTH, "dockingBay"));
		
		actualPlayer = playerList.get(whichPlayer);
		return true;

	}

	@Override
	public boolean chooseFacingDirection(String direction) {
		switch (direction) {
		case "NORTH":
			actualPlayer.setFaceDirection(Direction.NORTH);
		case "SOUTH":
			actualPlayer.setFaceDirection(Direction.SOUTH);
		case "EAST":
			actualPlayer.setFaceDirection(Direction.EAST);
		case "WEST":
			actualPlayer.setFaceDirection(Direction.WEST);
		default:
			actualPlayer.setFaceDirection(Direction.NONE);
		}
		return true;
	}

	@Override
	public boolean announceEndOfProgramming() {
		try {
			rulebook.checkPlayersProgram(actualPlayer);
			deck.addAll(actualPlayer.getProgramCards());
			actualPlayer.getProgramCards().clear();
			
			whichPlayer++;
			for(int i=whichPlayer; i<playerList.size(); i++) {
				if(!(playerList.get(i) instanceof Bot)) {
					actualPlayer = playerList.get(i);
					i = 100;
				}
				if(i == playerList.size()-1) {
					i = playerList.size();
					whichPlayer = i;
				}
			}
			if (whichPlayer < playerList.size()) {
				actualPlayer = playerList.get(whichPlayer);
				return true;
			} else {
				
				//save current state of current player poiters
				int whichPlayerTemp = whichPlayer;
				Player actualPlayerTemp = actualPlayer;
				
				for(Player pl: playerList) {
					if(pl instanceof Bot) {
						/*
						 * Adjust both vars to always point to the player (Bot) that is currently running runRoutine so that the  chooseProgramCard
						 * call within it is also called on the right player object.
						 */
						
						actualPlayer = pl;
						whichPlayer = playerList.indexOf(pl);
						try {
							((Bot) pl).runRoutine(rulebook.determineAllowedProgramSize(pl));
							System.out.println("runRoutine for " + pl.getName());
						} catch (NullPointerException | BotException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				whichPlayer = whichPlayerTemp;
				actualPlayer = actualPlayerTemp;
				
				startPhase();
				whichPlayer = 0;
				actualPlayer = playerList.get(whichPlayer);
//				resetDeck();
				return true;
			}
			
		} catch (IllegalProgramException e) {
			System.err.println(e);
			e.printStackTrace();

		}
		return false;
	}

	@Override
	public String chooseCourse(String name) {
		switch (name) {
		case "Dizzy Dash":

			gameboard = new GameBoard();
		}
		return "Sie haben das Spielfeld " + name + " ausgew�hlt";
	}

	@Override
	public boolean chooseProgramCard(int handIndex, int registerPhase) {
		System.out.println(actualPlayer.getName() + " plays card index " + handIndex + " in to register " + registerPhase);
		ArrayList<ProgramCard> Hand = actualPlayer.getProgramCards();
		int openRegister = 5;
		
		try {
//			openRegister = rulebook.amountProgramCards(actualPlayer) -5;
			openRegister = rulebook.amountProgramCards(actualPlayer) ;
		} catch (IllegalAmountOfDamageTokensException | DestructionException | AmountOfProgramCardsException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		if (registerPhase - 1 <= openRegister) {
			for (ProgramCard Karte : Hand) {
				Karte.getInfo();
			}
			
			//Check if received index exists
			if(handIndex < Hand.size()) {
				try {
					actualPlayer.playCard(Hand.get(handIndex), registerPhase);
					return true;
				} catch(IndexOutOfBoundsException | NullPointerException | IllegalArgumentException e) {
					System.err.println(e.getMessage());
					e.printStackTrace();
				}
			}
//			for (ProgramCard Karte : Hand) {
//				if (priorityNumber == Karte.getPriorityNumber()) {
//					try {
//						actualPlayer.playCard(Karte, registerPhase);
//						return true;
//					} catch (IndexOutOfBoundsException e) {
//						System.err.println(e);
//						e.printStackTrace();
//					}
//				}
//			}
		}
		return false;
	}

	@Override
	public String getCurrentPlayerInfo() {
		return actualPlayer.getInfo();
	}
	
	@Override
	public String getAllPlayerMapInfo() {
		String retString = "";
		
		for(Player player : playerList){
			retString +=  Integer.toString(player.getPos()[0]) + "_" + Integer.toString(player.getPos()[1]) + ":";
			retString +=  Integer.toString(player.getArchiveTokenPos()[0]) + "_" + Integer.toString(player.getArchiveTokenPos()[1]) + ":";
			retString += Integer.toString(player.getSpawnNumber()) + ":";
			retString += player.getFaceDirection().toString() + ":";
			retString += player.getActiveMap() + "|";
		}
		return retString;
	}
	
	@Override
	public boolean askForPowerDown(boolean decisionPowerDown) {
		if (decisionPowerDown == true) {
			if (actualPlayer.isPowerDownToken() == false) {
				actualPlayer.setPowerDownToken(true);
				return true;
			} else if (actualPlayer.isSecondPowerdownToken() == false) {
				actualPlayer.setSecondPowerdownToken(true);
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * load game
	 */
	@Override
	public boolean load(String path) {
		if(path.endsWith(".ser")) {
			iDataAccess ser = new DataAccessSER();
			try {
				GameMaster gamemaster = (GameMaster) ser.load(path);
				this.setRulebook(gamemaster.getRulebook());
				this.setDeck(gamemaster.getDeck());
				this.setPlayerList(gamemaster.getPlayerList());
				this.setGameboard(gamemaster.getGameboard());
				this.setRegisterPhase(gamemaster.getRegisterPhase());
				this.setSpawnPoints(gamemaster.getSpawnPoints());
				this.setWhichPlayer(gamemaster.getWhichPlayer());
				this.setActualPlayer(gamemaster.getActualPlayer());
				this.setTurnOrder(gamemaster.getTurnOrder());
				this.setDestroyedPlayer(gamemaster.getDestroyedPlayer());
				
			}catch(IOException e) {
				e.printStackTrace();
			}
			return true;
		}
        if(path.endsWith(".csv")) {
			iDataAccess csv = new DataAccessCSV();
			try {
				//load
				String line = csv.load(path).toString();
				String[] fields = line.split("\n");
				ArrayList<String[]> column = new ArrayList<String[]>();
				for(int i = 0; i < fields.length; i++) {
					column.add(fields[i].split(";"));	
				}
				
				int tempPlayer = Integer.valueOf(column.get(0)[0]);
				ArrayList<Player> tempPlayerlist = new ArrayList<Player>();
				for(int i = 0; i < tempPlayer; i++) {
					
					Player p = null;
					
					int tempPlayerInfo = Integer.valueOf(column.get(i + 1)[23]);
					
					if(column.get(i + 1)[(tempPlayerInfo * 2 + 1) + 23].equals("Bot")) {
						 p = new Bot(Integer.valueOf(column.get(i + 1)[1]), Integer.valueOf(column.get(i + 1)[5]), Integer.valueOf(column.get(i + 1)[2]), Integer.valueOf(column.get(i + 1)[3]), column.get(i + 1)[0], Direction.valueOf(column.get(i + 1)[4]), column.get(i + 1)[22]);
						
					} else {
						 p = new Player(Integer.valueOf(column.get(i + 1)[1]), Integer.valueOf(column.get(i + 1)[5]), Integer.valueOf(column.get(i + 1)[2]), Integer.valueOf(column.get(i + 1)[3]), column.get(i + 1)[0], Direction.valueOf(column.get(i + 1)[4]), column.get(i + 1)[22]);
					}
					
					p.setArchiveTokenPos(Integer.valueOf(column.get(i+1)[19]), Integer.valueOf(column.get(i+1)[20]));
					p.setLastTouchedFlag(Integer.valueOf(column.get(i + 1)[21]));
					p.setDamageTokens(Integer.valueOf(column.get(i + 1)[6]));
					p.setPowerDownToken(Boolean.valueOf(column.get(i + 1)[7]));
					p.setSecondPowerdownToken(Boolean.valueOf(column.get(i + 1)[8]));
					
					int tempProgramCardSize = Integer.valueOf(column.get(i + 1)[23]);
					ArrayList<ProgramCard> arrayProgramCards = new ArrayList<ProgramCard>();
					for(int x = 0; x < tempProgramCardSize; x++) {
						int tempPrioNumber = Integer.valueOf(column.get(i + 1)[24 + (2*x)]);
						CardType tempCardType = CardType.valueOf(column.get(i + 1)[24 + (2 * x)+1]);
						ProgramCard tempProC = new ProgramCard(tempPrioNumber, tempCardType);
						arrayProgramCards.add(tempProC);
					}
					p.setProgramCards(arrayProgramCards);
					
					ArrayList<ProgramCard> program = new ArrayList<ProgramCard>();
					for(int y = 0; y < 5; y++) {
						int tempPrioNumber = 0;
						if(!column.get(i + 1)[9 + (2 * y)].equals("noPrioNumber")) {
							tempPrioNumber = Integer.valueOf(column.get(i + 1)[9 + (2*y)]);
							
							CardType tempCardType = CardType.valueOf(column.get(i + 1)[9 + (2 * y)+1]);
							ProgramCard tempProC = new ProgramCard(tempPrioNumber, tempCardType);
							program.add(tempProC);
						} else {
							program.add(null);
						}
						
					}
					try {
						p.setProgram(program);
					} catch (NullPointerException | IllegalProgramSizeException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.err.println(e.getMessage());
					}
 
					tempPlayerlist.add(p);
					
				}
				this.setPlayerList(tempPlayerlist);
				whichPlayer = Integer.valueOf(column.get(0)[2]);
				this.actualPlayer = this.getPlayerList().get(Integer.valueOf(column.get(0)[2]));
				
				
				int tempProgramCardDeck = Integer.valueOf(column.get(0)[1]);
				ArrayList<ProgramCard> tempProCard = new ArrayList<ProgramCard>();
				for(int i = 0; i < tempProgramCardDeck; i++) {
					
					ProgramCard proCri = new ProgramCard(Integer.valueOf(column.get(i + 1 + tempPlayer)[0]), CardType.valueOf(column.get(i + 1 + tempPlayer)[1]));
					tempProCard.add(proCri);
				}
				this.setDeck(tempProCard);
				
				gameboard.setCourseName(column.get(1 + tempPlayer + tempProgramCardDeck)[0]);
				
				String.valueOf(column.get(1 + tempPlayer + tempProgramCardDeck + 1)[0]);
				
				loadOverJson(gameboard.getCourseName() + ".json");

				

				
			} catch(IOException e) {
				
			}
			return true;
		}
        if(path.endsWith(".json")) {

        	loadOverJson(path);
        	
        }
		return false;
	}
	
	private void loadOverJson(String path) {
		iDataAccess jSON = new DataAccessJSON();
    	try {
			JsonArray jsonArray = (JsonArray) jSON.load(path);
			for(int i = 0; i < jsonArray.size(); i++) {
				JsonArray jsonArray2 = jsonArray.getJsonArray(i);
				for(int j = 0; j < jsonArray2.size(); j++) {
				System.out.println("We are at " + i + "/" + j);
				JsonObject jsonObject = jsonArray2.getJsonObject(j);
				System.out.println(jsonObject.getJsonArray("rotation"));
						
				FieldType felder = null;
				String fieldType = jsonObject.getString("fieldType");
				int flag = jsonObject.getInt("flag");
				Direction richtung;
				JsonArray walls = jsonObject.getJsonArray("walls");
				JsonArray lasers = jsonObject.getJsonArray("lasers");
				JsonArray rotation = jsonObject.getJsonArray("rotation");
				JsonArray shift = jsonObject.getJsonArray("shift");
				boolean[] wallsArray = {walls.getBoolean(0), walls.getBoolean(1), walls.getBoolean(2), walls.getBoolean(3)};
				int[] lasersArray = {lasers.getInt(0), lasers.getInt(1), lasers.getInt(2), lasers.getInt(3)};
				int[] rotationArray = {rotation.getInt(0), rotation.getInt(1)};
				int[] shiftArray = {shift.getInt(0), shift.getInt(1), shift.getInt(2), shift.getInt(3)};
				
				switch(fieldType) {
				case "Floor": 
					if(flag == 0) {
						felder = FieldType.EMPTY;
						break;
					} else {
						felder = FieldType.FLAG;
					}
					break;
				case "Repair":
					if(fieldType.contains("REPAIR_ONE")) {
						felder = FieldType.REPAIR_ONE;
						break;
					} else {
						felder = FieldType.REPAIR_TWO;
					}
					break;
					
				case "Gear":
					if((rotationArray[0] == 1) && (rotationArray[1] == 0)) {
						felder = FieldType.GEAR_ROT_RIGHT;
						break;
					} else if((rotationArray[0] == 0) && (rotationArray[1] == 1)){
						felder = FieldType.GEAR_ROT_LEFT;
					}
					break;
					
				case "ConveyorBelt":
					int fast = 0;
					for(int zahl = 0; zahl < shiftArray.length; zahl++) {
						fast += shiftArray[zahl];
					}
					if(fast == 2 && (rotationArray[0] == 0) && (rotationArray[1] == 0)) {
						felder = FieldType.CONVEYOR_BELT_FAST;
						break;
					} else if(fast == 1 && (rotationArray[0] == 0) && (rotationArray[1] == 0)) {
						felder = FieldType.CONVEYOR_BELT;
						break;
					} else if(fast == 1 && (rotationArray[0] == 0) && (rotationArray[1] == 1)) {
						felder = FieldType.CONVEYOR_BELT_ROT_LEFT;
						break;
					} else if(fast == 2 && (rotationArray[0] == 0) && (rotationArray[1] == 1)) {
						felder = FieldType.CONVEYOR_BELT_ROT_LEFT_FAST;
						break;
					} else if(fast == 1 && (rotationArray[0] == 1) && (rotationArray[1] == 0)) {
						felder = FieldType.CONVEYOR_BELT_ROT_RIGHT;
						break;
					} else if(fast == 2 && (rotationArray[0] == 1) && (rotationArray[1] == 0)) {
						felder = FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST;
					}
					break;
				case "Pit":
					felder = FieldType.PIT;
				}
				
				
					
				if(shiftArray[0] != 0) {
					richtung = Direction.NORTH;
				} else if(shiftArray[1] != 0) {
					richtung = Direction.EAST;
				} else if(shiftArray[2] != 0) {
					richtung = Direction.SOUTH;
				} else if(shiftArray[3] != 0) {
					richtung = Direction.WEST;
				} else {
					richtung = Direction.NONE;
				}
				
				
				gameboard.setFieldDirectionAt(i, j, richtung);
				gameboard.setFieldLasersAt(i, j, flag);
				gameboard.setFieldLasersAt(i, j, lasersArray);
				gameboard.setFieldTypeAt(i, j, felder);
				gameboard.setFieldWallsAt(i, j, wallsArray);
				}
				

		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * save game
	 */
	@Override
	public boolean save(String path) {
		if(path.endsWith(".ser")) {
			iDataAccess ser = new DataAccessSER();
			try {
				ser.save(this, path);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return true;
		}
		createCSVString();
		if(path.endsWith(".csv")) {
			iDataAccess csv = new DataAccessCSV();
			try {
				csv.save(this, path);
			} catch(FileNotFoundException e) {
				e.printStackTrace();
			} catch(IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		if(path.endsWith(".json")) {
			iDataAccess json = new DataAccessJSON();
			try {
				json.save(createJsonArray(), path);
			} catch(FileNotFoundException e) {
				e.printStackTrace();
			} catch(IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	public String createCSVString() {
		GameMaster gm = this;
		String stringForCSV=playerList.size()+";"+ deck.size() + ";" + whichPlayer +"\n";
		
		for(Player pl: gm.playerList) {
			stringForCSV += pl.toCsvString();
			if(pl instanceof Bot) {
				String plInfo = "Bot";
				stringForCSV += ";" + plInfo +"\n";
			} else {
				String plInfo = "Human";
				stringForCSV += ";" + plInfo + "\n";
			}
		}
		
		for(ProgramCard pr: gm.deck) {
			stringForCSV += pr.toCsvString()+"\n";
		}
		
		stringForCSV += gameboard.getCourseName() + "\n";
		
		if(rulebook instanceof RuleBookDM) {
			stringForCSV += "DeathMatch";
		} else if(rulebook instanceof RuleBook) {
			stringForCSV += "RoboRally";
		}
		
		
		return stringForCSV;
		
	}
	
	public JsonArrayBuilder createJsonArray() {
		
		JsonArrayBuilder jsonArrayBuilderAu�en = Json.createArrayBuilder();
		JsonArrayBuilder jsonArrayBuilderInnen = Json.createArrayBuilder();
		JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
		
		for(int i = 0; i < 12; i++) {
			for(int j = 0; j < 12; j++) {
				
				String fieldType = gameboard.fieldTypeAt(i, j, false).toString();

				switch(fieldType) {
				case "EMPTY":
					jsonObjectBuilder.add("fieldType", "Floor");
					break;
                case "REPAIR_ONE":
                	jsonObjectBuilder.add("fieldType", "Repair");
					break;
				case "REPAIR_TWO":
					jsonObjectBuilder.add("fieldType", "Repair");
					break;
				case "GEAR_ROT_LEFT":
					jsonObjectBuilder.add("fieldType", "Gear");
					break;
				case "GEAR_ROT_RIGHT":
					jsonObjectBuilder.add("fieldType", "Gear");
					break;
				case "CONVEYOR_BELT_FAST":
					jsonObjectBuilder.add("fieldType", "ConveyorBelt");
					break;
				case "CONVEYOR_BELT_ROT_RIGHT_FAST":
					jsonObjectBuilder.add("fieldType", "ConveyorBelt");
					break;
				case "FLAG":
					jsonObjectBuilder.add("fieldType", "Floor");
					break;
				case "PIT":
					jsonObjectBuilder.add("fieldType", "Pit");
					break;
				}
				
				
				jsonObjectBuilder.add("flag", gameboard.fieldIndexAt(i, j, false));
				
	
				JsonArrayBuilder jsonWallsA = Json.createArrayBuilder();
				jsonWallsA.add(gameboard.fieldWallsAt(i, j, false)[0]);
				jsonWallsA.add(gameboard.fieldWallsAt(i, j, false)[1]);
				jsonWallsA.add(gameboard.fieldWallsAt(i, j, false)[2]);
				jsonWallsA.add(gameboard.fieldWallsAt(i, j, false)[3]);
				jsonObjectBuilder.add("walls", jsonWallsA);
				

				JsonArrayBuilder jsonLaserA = Json.createArrayBuilder();
				jsonLaserA.add(gameboard.fieldLasersAt(i, j, false)[0]);
				jsonLaserA.add(gameboard.fieldLasersAt(i, j, false)[1]);
				jsonLaserA.add(gameboard.fieldLasersAt(i, j, false)[2]);
				jsonLaserA.add(gameboard.fieldLasersAt(i, j, false)[3]);
				jsonObjectBuilder.add("lasers", jsonLaserA);
				
				int[] tempR = new int[2];
				tempR[0] = 0;
				tempR[1] = 1;
				JsonArrayBuilder jsonRotaA = Json.createArrayBuilder();
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.EMPTY))) {
					jsonRotaA.add(tempR[0]);
					jsonRotaA.add(tempR[0]);
				}
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.FLAG))) {
					jsonRotaA.add(tempR[0]);
					jsonRotaA.add(tempR[0]);
				}
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.REPAIR_ONE)) || (gameboard.fieldTypeAt(i, j, false).equals(FieldType.REPAIR_TWO))) {
					jsonRotaA.add(tempR[0]);
					jsonRotaA.add(tempR[0]);
				}
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.GEAR_ROT_LEFT))) {
					jsonRotaA.add(tempR[0]);
					jsonRotaA.add(tempR[1]);
				}
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.GEAR_ROT_RIGHT))) {
					jsonRotaA.add(tempR[1]);
					jsonRotaA.add(tempR[0]);
				}
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.CONVEYOR_BELT_FAST))) {
					jsonRotaA.add(tempR[0]);
					jsonRotaA.add(tempR[0]);
				}
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST))) {
					
					if(gameboard.fieldDirectionAt(i, j, false).equals(Direction.NORTH)){
						jsonRotaA.add(tempR[1]);
						jsonRotaA.add(tempR[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.EAST))) {
						jsonRotaA.add(tempR[1]);
						jsonRotaA.add(tempR[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.SOUTH))) {
						jsonRotaA.add(tempR[1]);
						jsonRotaA.add(tempR[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.WEST)) ) {
						jsonRotaA.add(tempR[1]);
						jsonRotaA.add(tempR[0]);
					}
				}
				if(gameboard.fieldTypeAt(i, j, false).equals(FieldType.PIT)) {
					jsonRotaA.add(tempR[0]);
					jsonRotaA.add(tempR[0]);
				}
				
				jsonObjectBuilder.add("rotation", jsonRotaA);
				
				
				int[] tempS = new int[3];
				tempS[0] = 0;
				tempS[1] = 1;
				tempS[2] = 2;
				JsonArrayBuilder jsonShiftA = Json.createArrayBuilder();
				if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.CONVEYOR_BELT_FAST))) {
					
					if(gameboard.fieldDirectionAt(i, j, false).equals(Direction.NORTH)){
						jsonShiftA.add(tempS[2]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.EAST))) {
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[2]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.SOUTH))) {
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[2]);
						jsonShiftA.add(tempS[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.WEST)) ) {
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[2]);
					}
					
				} 
				else if((gameboard.fieldTypeAt(i, j, false).equals(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST))) {
					
					if(gameboard.fieldDirectionAt(i, j, false).equals(Direction.NORTH)){
						jsonShiftA.add(tempS[2]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.EAST))) {
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[2]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.SOUTH))) {
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[2]);
						jsonShiftA.add(tempS[0]);
					}
					
					if((gameboard.fieldDirectionAt(i, j, false).equals(Direction.WEST)) ) {
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[0]);
						jsonShiftA.add(tempS[2]);
					}
				}else {
					jsonShiftA.add(tempS[0]);
					jsonShiftA.add(tempS[0]);
					jsonShiftA.add(tempS[0]);
					jsonShiftA.add(tempS[0]);
				}
				jsonObjectBuilder.add("shift", jsonShiftA);

				
				
				jsonArrayBuilderInnen.add(jsonObjectBuilder);
				
			}
			jsonArrayBuilderAu�en.add(jsonArrayBuilderInnen);
		}
		
		
		return jsonArrayBuilderAu�en;
		
	}
	
	
	//Getter and Setter
	public RuleBook getRulebook() {
		return rulebook;
	}

	public void setRulebook(RuleBook rulebook) {
		this.rulebook = rulebook;
	}

	public ArrayList<ProgramCard> getDeck() {
		return deck;
	}

	public void setDeck(ArrayList<ProgramCard> deck) {
		this.deck = deck;
	}

	public ArrayList<Player> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(ArrayList<Player> playerList) {
		this.playerList = playerList;
	}

	public GameBoard getGameboard() {
		return gameboard;
	}

	public void setGameboard(GameBoard gameboard) {
		this.gameboard = gameboard;
	}

	public int getRegisterPhase() {
		return registerPhase;
	}

	public void setRegisterPhase(int registerPhase) {
		this.registerPhase = registerPhase;
	}

	public LinkedList<Integer> getSpawnPoints() {
		return SpawnPoints;
	}

	public void setSpawnPoints(LinkedList<Integer> spawnPoints) {
		SpawnPoints = spawnPoints;
	}

	public int getWhichPlayer() {
		return whichPlayer;
	}

	public void setWhichPlayer(int whichPlayer) {
		this.whichPlayer = whichPlayer;
	}

	public Player getActualPlayer() {
		return actualPlayer;
	}

	public void setActualPlayer(Player actualPlayer) {
		this.actualPlayer = actualPlayer;
	}

	public ArrayList<Player> getTurnOrder() {
		return turnOrder;
	}

	public void setTurnOrder(ArrayList<Player> turnOrder) {
		this.turnOrder = turnOrder;
	}

	public ArrayList<Player> getDestroyedPlayer() {
		return destroyedPlayer;
	}

	public void setDestroyedPlayer(ArrayList<Player> destroyedPlayer) {
		this.destroyedPlayer = destroyedPlayer;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof GameMaster) {
			GameMaster gm = (GameMaster) obj;
			
			if((gm.getRegisterPhase() == this.getRegisterPhase()) || (gm.getRulebook() == this.getRulebook()) || (gm.getDeck() == this.getDeck()) || (gm.getPlayerList() == this.getPlayerList()) || (gm.getGameboard() == this.getGameboard()) || (gm.getSpawnPoints() == this.getSpawnPoints()) || (gm.getWhichPlayer() == this.getWhichPlayer()) || (gm.getActualPlayer() == this.getActualPlayer()) || (gm.getTurnOrder() == this.getTurnOrder()) || (gm.getDestroyedPlayer() == this.getDestroyedPlayer()) ) {
				return true;
			}
			
		}
		return false;
	}

	@Override
	public ArrayList<String> getAllPlayerMovementLog() {
		return allPlayerMovementLog;
	}
	
	@Override
	public String getBoardString(String board) {
		return gameboard.getAllBoardInfo();
	}
	
	@Override
	public boolean selectGameMode(String mode) {
		
		if(mode.equals("RoboRally") ) {
			rulebook = new RuleBook();
		} 
		if(mode.equals("DeathMatch")) {
			rulebook = new RuleBookDM();
		}
		
		return false;
	}
	
	@Override
	public int setAIPlayers(int aiCount) {
		
//		int a = randomPlayerNumber(SpawnPoints, rulebook)[0];
//		int x = randomPlayerNumber(SpawnPoints, rulebook)[1];
//		int y = randomPlayerNumber(SpawnPoints, rulebook)[2];
		
		for(int i = 0; i < aiCount; i++) {
			int[] playerNumAndPos = randomPlayerNumber(SpawnPoints, rulebook);
			playerList.add(new Bot(playerNumAndPos[0], 3, playerNumAndPos[1], playerNumAndPos[2], "Bot "+i, Direction.NORTH, "dockingBay", this));
		}
		
		actualPlayer = playerList.get(whichPlayer);
		
		return aiCount;
	}



	@Override
	public Player winningPlayer() {
		if(rulebook.checkGameOver(gameboard, playerList)) {
			return playerList.get(0);
		}
		return null;
	}
	
}
