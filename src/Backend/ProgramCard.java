package Backend;

import java.io.Serializable;

import Enums.CardType;
import Exceptions.IllegalParameterException;

/**
 * ProgramCard Class
 * The cards players use to program their robots.
 * @author Arthur Huber
 */
public class ProgramCard implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int priorityNumber;
	private CardType type;
	
	
	
	/**
	 * Create a new object with parameters initialized according to parameters
	 * @param priorityNumber
	 * @param type
	 * @throws NullPointerException Exceptions thrown by setters are passed to caller
	 */
	public ProgramCard(int priorityNumber, CardType type) throws NullPointerException{
		setPriorityNumber(priorityNumber);
		setType(type);
	}
	
	public int getPriorityNumber() {
		return priorityNumber;
	}
	
	public CardType getType() {
		return type;
	}
	
	/**
	 * Sets the priority number of the program card
	 * @param priorityNumber
	 */
	public void setPriorityNumber(int priorityNumber) {
		this.priorityNumber = priorityNumber;
	}
	
	/**
	 * Sets the type of the program card
	 * @param type
	 * @throws NullPointerException if the passed type is null
	 */
	public void setType(CardType type) throws NullPointerException{
		if(type== null) {
			throw new NullPointerException("The passed cardType was null!");
		}
		this.type = type;
	}
	
	public String getInfo() {
		return (type.toString() + "." + Integer.toString(priorityNumber));
	}
	
   public String toCsvString() {
		
		String csv = priorityNumber + ";" + type + ";";
		//csv += ";";
		
		return csv;
		
	}
}

 
