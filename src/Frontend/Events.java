package Frontend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;



import Backend.GameMaster;
import Interface.iController;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Events implements EventHandler<ActionEvent> {
	private static iController gm;
	private StartWindow controller;
	private MapWindow MWController;

	/**
	 * This Constructor starts the START-Window (AddPlayer, Choose Map, etc ...)
	 * 
	 * @param controller
	 * 			Is the WelcomeWindow controller
	 */

	public Events(StartWindow controller) {
		if (gm == null) {
			gm = new GameMaster();
		}
		this.controller = controller;
		MWController = new MapWindow();
	}

	/**
	 * This Constructor starts the PLAY-Window, where you see the current Board with
	 * Players and the actual Programsheet
	 * 
	 * @param controller
	 * 			Is the WelcomeWindow controller
	 */
	
	public Events(MapWindow controller) {
		if (gm == null) {
			gm = new GameMaster();
		}
		this.MWController = controller;
	}

	@Override
	public void handle(ActionEvent event) {

		if (event.getSource() instanceof Button) {
			Button b = (Button) event.getSource();
			switch (b.getId()) {

			case "load":
				FileChooser fc = new FileChooser();
				fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Serialize", "*.ser"));
				fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
//				fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.json")); //Auskommentiert, weil JSON nicht alleine geladen werden soll

				gm.load(fc.showOpenDialog((Stage) b.getScene().getWindow()).toString());
				gm.startGame();
				switchToMapScene(b);
				System.out.println("Load a game!");
				break;

			case "addPlayer":
				TextInputDialog tid = new TextInputDialog();
				tid.setTitle("Add a player");
				tid.setHeaderText("");
				tid.setContentText("");
				Optional<String> name = tid.showAndWait();
				// Check if name != null and if the String is > 0
				if (name.isPresent() && name.get().length() > 0) {
					if (gm.addPlayer(name.get())) {
						controller.getPlayerList().getItems().add(name.get());
						
						//Update Ai selection list
						int maxAi = 8 - controller.getPlayerList().getItems().size();
						int aiCountPlayerChoice = controller.getAiCountSelect().getValue();
						
						controller.getAiCountSelect().getItems().clear();
						
						for(int i=0; i<=maxAi; i++) {
							controller.getAiCountSelect().getItems().add(i);
						}
						
						if(aiCountPlayerChoice > maxAi) {
							controller.getAiCountSelect().setValue(maxAi);
						} else {
							controller.getAiCountSelect().setValue(aiCountPlayerChoice);
						}
						
						System.out.println("Player " + name.get() + " has been added.");
						
					}
				} else {
					System.out.println("Player has NOT been added.");
				}
				break;

			case "start":
				
				gm.selectGameMode(controller.getGameModeSelect().getValue());
				gm.setAIPlayers(controller.getAiCountSelect().getValue());
				if (gm.startGame()) {
					switchToMapScene(b);
				} else {
					Alert wrongAmountOFPlayers = new Alert(AlertType.ERROR, "You need 2 - 8 Players for this Map!");
					wrongAmountOFPlayers.showAndWait();
				}
				break;

			case "deal":
				MWController.refreshScreen(gm.getCurrentPlayerInfo(), gm.getAllPlayerMapInfo());
				break;

			case "save":
				FileChooser fcSave = new FileChooser();
				fcSave.getExtensionFilters().add(new FileChooser.ExtensionFilter("Serialisierung", "*.ser"));
				fcSave.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
				fcSave.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.json"));

				gm.save(fcSave.showSaveDialog((Stage) b.getScene().getWindow()).toString());
				break;

			case "finishProg":
				// String[] playerMapInfo = gm.getAllPlayerMapInfo().split("\\|");
				if (!gm.announceEndOfProgramming()) {
					new Alert(AlertType.INFORMATION, "You have to fill all registers with cards!").showAndWait();

				} else {
					MWController.refreshScreen(gm.getCurrentPlayerInfo(), gm.getAllPlayerMapInfo());
				}
				
				if(!(gm.winningPlayer() == null)) {
					Alert win = new Alert(AlertType.INFORMATION);
					win.setContentText(gm.winningPlayer().getName() + " has won the Game!");
				}
				break;

			case "getPlayerInfo":
				
				animatePlayers();

				break;
				
//			case "setModeButton":
//				gm.selectGameMode(controller.getGameModeSelect().getValue());
//				break;
				
//			case "setAiButton":
//				gm.setAIPlayers(controller.getAiCountSelect().getValue());
//				break;
				
			default:
				break;
			}

		}
		if (event.getSource() instanceof ComboBox) {
			ComboBox cb = (ComboBox) event.getSource();
			switch (cb.getId()) {
				case "gameModeSelection":
					System.out.println("gameModeSelection was triggered");
					break;
					
				case "aiCountSelection":
					System.out.println("aiCountSelection was triggered");

					break;
			}
		}
	}

	/**
	 * This method switches from the Startwindow to the Mapwindow
	 * @param stageSource
	 * 			Button thats clicked to switch from start to game window
	 */
	
	private void switchToMapScene(Button stageSource) {

		BorderPane mapRoot;
		Stage primaryStage = (Stage) stageSource.getScene().getWindow();

		Scene scene;
		try {
			mapRoot = FXMLLoader.load(getClass().getResource("../GUI/MapWindow.fxml"));
			scene = new Scene(mapRoot);

			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setMaximized(true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 *This Method  
	 */
	
	private void updateLeftPane() {
		String[] playerMapInfo = gm.getAllPlayerMapInfo().split("\\|");
		// TODO Proper bot rotation!
		for (int i = 0; i < playerMapInfo.length; i++) {
			String[] playerInfo = playerMapInfo[i].split(":");
			String[] tempPos = playerInfo[0].split("_");
			System.out.println("Drawing bot at: " + tempPos[0] + "|" + tempPos[1]);
			MWController.getDockingBay().add(MWController.getBotImage(Integer.valueOf(playerInfo[2])),
					Integer.valueOf(tempPos[0]), Integer.valueOf(tempPos[1]));
		}
	}

	private void updateCenterPane() {
		String[] playerInfo = gm.getCurrentPlayerInfo().split(",");
		MWController.setupPlayerName(playerInfo[0], Integer.valueOf(playerInfo[1]));

		GridPane programCards = MWController.getProgramCards();
		// Draw hand
		drawCardsToPane(MWController.getProgramCards(), playerInfo[2].split(";"));

		// DrawProgram
		drawCardsToPane(MWController.getPlayerProgram(), playerInfo[7].split(";"));
	}

	private void drawCardsToPane(GridPane gp, String[] cards) {
		// Clear Pane
		gp.getChildren().clear();

		// Player Hand
		for (int i = 0; i < cards.length; i++) {
			// "." is a regex metacharacter which has to be escaped
			String[] card = cards[i].split("\\.");
			if (card[0].equals("null")) {
				ImageView base = MWController.getCardTexture("card_base");
				gp.add(base, i, 0);
			} else {
				char[] digits = card[1].toCharArray();
				ImageView base = MWController.getCardTexture("card_base");

				gp.add(base, i, 0);
				ImageView typeImage = MWController.getCardTexture(card[0]);
				gp.add(typeImage, i, 0);

				// draw digits
				for (int j = digits.length - 1; j > -1; j--) {
					// All these hardcoded numbers were obtained by trial and error. There probably
					// is a better way.
					ImageView tempDigit = new ImageView(new Image(
							"file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_" + digits[j] + ".png", 20,
							40, false, false));
					// 20 -> Standart offset for centering.
					// 25 -> Distance between digits
					// All positins are relative to the position of the place of the GridPane
					tempDigit.setTranslateX(20 + 25 * j);
					tempDigit.setTranslateY(-63);

					gp.add(tempDigit, i, 0);
					// Fancy 0 in front of double digit priorities
					// if(digits.length == 2) {
					// tempDigit = new ImageView(new
					// Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_0.png",
					// 20, 40, false, false));
					// //20 -> Standart offset for centering.
					// //25 -> Distance between digits
					// //All positins are relative to the position of the place of the GridPane
					// tempDigit.setTranslateX(20+25*0);
					// tempDigit.setTranslateY(-63);
					// myPane.add(tempDigit, i, 0);
					// }
				}
			}
		}
	}

	public static iController getGm() {
		return gm;
	}

	public static void setGm(iController gm) {
		Events.gm = gm;
	}

	
	/**
	 * This method is called after all the movement has been calculated in the backend.
	 * It then animates every single move of every player. It takes the information from gameMaster.getAllPlayerMovementLog()
	 */
	public void animatePlayers() {
		System.out.println("called for animation");
		ArrayList<String> completeMovementLog = gm.getAllPlayerMovementLog();
		String[] oneRegisterPhaseOnly = { "" };
		String[] playerInfo = { "" };
		String rotation;
		System.out.println(gm.getAllPlayerMovementLog().size());
		for (int x = 0; x < gm.getAllPlayerMovementLog().size(); x++) {
			System.out.println(gm.getAllPlayerMovementLog().get(x));
		}

		TranslateTransition movePlayerAnimationX = new TranslateTransition();
		movePlayerAnimationX.setDuration(Duration.millis(1500));

		TranslateTransition movePlayerAnimationY = new TranslateTransition();
		movePlayerAnimationY.setDuration(Duration.millis(1500));

		RotateTransition rotatePlayerAnimation = new RotateTransition();
		rotatePlayerAnimation.setDuration(Duration.millis(1500));

		SequentialTransition completeMove = new SequentialTransition(rotatePlayerAnimation, movePlayerAnimationX,
				movePlayerAnimationY);

		for (int i = 0; i < completeMovementLog.size(); i++) {
			oneRegisterPhaseOnly = completeMovementLog.get(i).split("\\|");

			for (int e = 0; e < oneRegisterPhaseOnly.length; e++) {
				playerInfo = oneRegisterPhaseOnly[e].split(":");
				String[] singlePosition = playerInfo[0].split("_");

				rotation = playerInfo[3];
				int currentxPosition = Integer.parseInt(singlePosition[0]);
				int currentyPosition = Integer.parseInt(singlePosition[1]);
				int xShift = Integer.parseInt(singlePosition[0]);
				int yShift = Integer.parseInt(singlePosition[1]);

				if (i > 0) {
					oneRegisterPhaseOnly = completeMovementLog.get(i - 1).split("\\|");
					playerInfo = oneRegisterPhaseOnly[e].split(":");
					singlePosition = playerInfo[1].split("_");
					int previousXPosition = Integer.parseInt(singlePosition[0]);
					int previousYPosition = Integer.parseInt(singlePosition[1]);

					xShift = currentxPosition - previousXPosition;
					yShift = currentyPosition - previousYPosition;
				}
				boolean move = true;
				while (move) {
					if (xShift == 1) {
						movePlayerAnimationX.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						movePlayerAnimationX.setByX(50);
						move = false;

					} else if (xShift == -1) {
						movePlayerAnimationX.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						movePlayerAnimationX.setByX(-50);
						move = false;
					} else if (yShift == 1) {
						movePlayerAnimationY.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						movePlayerAnimationY.setByY(50);
						move = false;
					} else if (yShift == -1) {
						movePlayerAnimationY.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						movePlayerAnimationY.setByY(-50);
						move = false;
					}
					if (rotation.equals("NORTH")) {
						rotatePlayerAnimation.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						rotatePlayerAnimation.setByAngle(0);
						move = false;
					} else if (rotation.equals("EAST")) {
						rotatePlayerAnimation.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						rotatePlayerAnimation.setByAngle(90);
						move = false;
					} else if (rotation.equals("SOUTH")) {
						rotatePlayerAnimation.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						rotatePlayerAnimation.setByAngle(180);
						move = false;
					} else if (rotation.equals("WEST")) {
						rotatePlayerAnimation.setNode(MWController.getDockingBay().getChildren().get(80 + i));
						rotatePlayerAnimation.setByAngle(270);
						move = false;
					}
				}

			}
			completeMove.play();

		}

	}

}
