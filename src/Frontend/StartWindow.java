package Frontend;

import java.net.URL;
import java.util.ResourceBundle;



import Frontend.Events;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;

public class StartWindow  implements Initializable{
	
	@FXML
	private Button load;
	
	@FXML
	private Button addPlayer;
	
	@FXML
	private Button start;
	
	@FXML
	private ListView<String> playerList;
	
	@FXML
	private ListView<String> courseList;
	
	@FXML
	private ComboBox<String> gameModeSelect;
	
	@FXML
	private ComboBox<Integer> aiCountSelect;

//	@FXML
//	private Button setModeButton;
//	
//	@FXML
//	private Button setAiButton;
	
	//These are dummies just for demostration
	private int maxAI = 7;
	private String[] gameModes = {"RoboRally", "DeathMatch"};  
	
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//DON'T INITIALIZE LISTVIEWS HERE BECAUSE IT DUCKS EVERYTHING UP!!!
		//IN FACT DON'T INITIALIZE ANY OF THE GUI ELEMNTS HERE
		
		Events e = new Events(this);
		load.setOnAction(e);
		addPlayer.setOnAction(e);
		start.setOnAction(e); 
//		setModeButton.setOnAction(e);
//		setAiButton.setOnAction(e);
		
		//Add selectable items
		gameModeSelect.getItems().addAll(gameModes);
		for(int i=0; i<=maxAI; i++) {
			aiCountSelect.getItems().add(i);
		}
		
		gameModeSelect.setValue("RoboRally");
		aiCountSelect.setValue(0);
		//gameModeSelect.setOnAction(e);
		aiCountSelect.setOnAction(e);
	}

	public ListView<String> getPlayerList(){
		return playerList;
	}
	
	public ComboBox<String> getGameModeSelect(){
		return gameModeSelect;
	}
	
	public ComboBox<Integer> getAiCountSelect(){
		return aiCountSelect;
	}
	
}
