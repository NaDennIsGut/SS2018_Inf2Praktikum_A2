package Frontend;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import Backend.GameMaster;
import Interface.iController;
import javafx.event.EventHandler;

public class MouseEvents implements EventHandler<MouseEvent>{

	private static iController gm;
	private StartWindow controller;
	private MapWindow MWController;
	private int choosenRegister;
	private int choosenCard;
	
	public MouseEvents(MapWindow controller) {
		this.MWController = controller;
		choosenRegister = -1;
		choosenCard = -1;
	}
	
	@Override
	public void handle(MouseEvent event) {
		if(event.getSource() instanceof GridPane) {
			GridPane gp = (GridPane) event.getSource();
			double cardWidth = 112;
			double x = event.getX();
			int cardNum = (int) (x/cardWidth);
			switch (gp.getId()) {
				case "programCards":

					System.out.println("Card " + cardNum + " has been clicked");
					
					
					if(cardNum < 10) {
						choosenCard = cardNum;
						if(choosenRegister != -1 && choosenCard != -1) {
							gm.chooseProgramCard(choosenCard, choosenRegister);
							MWController.refreshScreen(gm.getCurrentPlayerInfo(), gm.getAllPlayerMapInfo());
							choosenCard = -1;
							choosenRegister = -1;
						}
					}
					System.out.println("ChoosenCard is now " + choosenCard);
					System.out.println("ChooseRegister is now " + choosenRegister);
					break;
					
				case "playerProgram":
					System.out.println("Register " + cardNum + " has been clicked");
					if(cardNum < 6) {
						choosenRegister = cardNum;
						if(choosenRegister != -1 && choosenCard != -1) {
							gm.chooseProgramCard(choosenCard, choosenRegister);
							MWController.refreshScreen(gm.getCurrentPlayerInfo(), gm.getAllPlayerMapInfo());
							choosenCard = -1;
							choosenRegister = -1;
						}
					}
					System.out.println("ChosenCard is now " + cardNum);
					System.out.println("ChosenRegister is now " + choosenRegister);
					break;
				
			default:
				
			}
		}
	}

	public static iController getGm() {
		return gm;
	}

	public static void setGm(iController gm) {
		MouseEvents.gm = gm;
	}

	
}
