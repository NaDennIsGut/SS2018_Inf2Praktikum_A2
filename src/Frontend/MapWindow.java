package Frontend;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Backend.GameMaster;
import Interface.iController;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class MapWindow implements Initializable {

	@FXML
	private GridPane gameboard;

	@FXML
	private GridPane dockingBay;
	
	@FXML
	private Label playerNameLabel;
	
	@FXML
	private HBox programSheet;

	@FXML
	private GridPane programCards;
	
	@FXML 
	private Button deal;
	
	@FXML
	private Button save;

	@FXML
	private Button pwrDwn;
	
	@FXML
	private Button finishProg;
	
	@FXML
	private GridPane playerProgram;
	
	@FXML
	private Button getPlayerInfo;
	
	@FXML
	private GridPane powerDownTokens;
	
	@FXML
	private GridPane lifeTokens;
	
	@FXML
	private GridPane damageTokens;
	
	private static final int mapTileSize = 50;

	private final int CARD_SIZE_X = 112;
	private final int CARD_SIZE_Y = 180;
	private final int PRIO_NUMBER_SIZE_X = 20;
	private final int PRIO_NUMBER_SIZE_Y = 40;
	
	private final int lifeTokenSize = 42;
	private final int damageTokenSize = 40;
	private final int powerDownTokenSize = 84;
	
	private Image baseField = new Image("file:../../GUI_Assets/MapWindow/base_field.png", mapTileSize, mapTileSize, false,
			false);

	//LOAD BELTS
	//slow
	private Image belt_straight = new Image("file:../../GUI_Assets/MapWindow/belts/belt_straight.png", mapTileSize,
			mapTileSize, false, false);
	private Image belt_rot_left = new Image("file:../../GUI_Assets/MapWindow/belts/belt_rot_left.png", mapTileSize,
			mapTileSize, false, false);
	private Image belt_rot_right = new Image("file:../../GUI_Assets/MapWindow/belts/belt_rot_right.png", mapTileSize,
			mapTileSize, false, false);
	
	//fast
	private Image belt_straight_fast = new Image("file:../../GUI_Assets/MapWindow/belts/belt_straight_fast.png",
			mapTileSize, mapTileSize, false, false);
	private Image belt_rot_left_fast = new Image("file:../../GUI_Assets/MapWindow/belts/belt_rot_left_fast.png",
			mapTileSize, mapTileSize, false, false);
	private Image belt_rot_right_fast = new Image("file:../../GUI_Assets/MapWindow/belts/belt_rot_right_fast.png",
			mapTileSize, mapTileSize, false, false);

	//LOAD FLAGS
	private Image flag_1 = new Image("file:../../GUI_Assets/MapWindow/flags/flag_1.png", mapTileSize, mapTileSize, false,
			false);
	private Image flag_2 = new Image("file:../../GUI_Assets/MapWindow/flags/flag_2.png", mapTileSize, mapTileSize, false,
			false);
	private Image flag_3 = new Image("file:../../GUI_Assets/MapWindow/flags/flag_3.png", mapTileSize, mapTileSize, false,
			false);
	private Image flag_4 = new Image("file:../../GUI_Assets/MapWindow/flags/flag_4.png", mapTileSize, mapTileSize, false,
			false);

	//LOAD GEARS
	private Image gear_rot_left = new Image("file:../../GUI_Assets/MapWindow/gears/gear_rot_left.png", mapTileSize,
			mapTileSize, false, false);
	private Image gear_rot_right = new Image("file:../../GUI_Assets/MapWindow/gears/gear_rot_right.png", mapTileSize,
			mapTileSize, false, false);

	//LOAD REPAIR
	private Image repair_1 = new Image("file:../../GUI_Assets/MapWindow/repairs/repair_1.png", mapTileSize, mapTileSize,
			false, false);
	private Image repair_2 = new Image("file:../../GUI_Assets/MapWindow/repairs/repair_2.png", mapTileSize, mapTileSize,
			false, false);

	//LOAD SPAWNS
	private Image spawn_1 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_1.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_2 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_2.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_3 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_3.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_4 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_4.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_5 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_5.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_6 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_6.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_7 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_7.png", mapTileSize, mapTileSize, false,
			false);
	private Image spawn_8 = new Image("file:../../GUI_Assets/MapWindow/spawns/spawn_8.png", mapTileSize, mapTileSize, false,
			false);

	//LOAD WALL
	private Image wall = new Image("file:../../GUI_Assets/MapWindow/wall.png", mapTileSize, mapTileSize, false,
			false);

	//LOAD MISSINGS
	private Image missing_field = new Image("file:../../GUI_Assets/MapWindow/missing_field.png", mapTileSize, mapTileSize, false, false);
	private Image MISSINGNO = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/MISSINGNO.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	
	//LOAD PROGRAMSHEET
	private Image program_sheet = new Image ("file:../../GUI_Assets/MapWindow/program_sheet/program_sheet.png",1700*0.33 ,850*0.33 ,false ,false );
	
	private GridPane playerHand = new GridPane();
	
	
	
	//Load card bases
	private Image cardBase = new Image ("file:../../GUI_Assets/MapWindow/cards/card_base.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image noCardBase = new Image ("file:../../GUI_Assets/MapWindow/cards/no_card_base.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	
	//Load card types
	private Image unknown = new Image ("file:../../GUI_Assets/MapWindow/cards/UNKNOWN.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image back_up = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/BACK_UP.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image move_1 = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/MOVE_ONE.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image move_2 = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/MOVE_TWO.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image move_3 = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/MOVE_THREE.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image rot_left = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/ROTATE_LEFT.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image rot_right = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/ROTATE_RIGHT.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	private Image u_turn = new Image ("file:../../GUI_Assets/MapWindow/cards/icons/U_TURN.png", CARD_SIZE_X, CARD_SIZE_Y, false, false);
	
	//Load priority digits
	private Image prio_0 = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_0.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_1 = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_1.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_2 = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_2.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_3 = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_3.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_4 = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_4.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_5 = new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_5.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_6 = new Image ("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_6.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_7 = new Image ("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_7.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_8 = new Image ("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_8.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	private Image prio_9 = new Image ("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_9.png", PRIO_NUMBER_SIZE_X, PRIO_NUMBER_SIZE_Y, false, false);
	
	//Load Tokens

	
	private Image lifeToken = new Image("file:../../GUI_Assets/MapWindow/program_sheet/tokens/life.png", lifeTokenSize, lifeTokenSize, false, false);
	private Image damageToken = new Image("file:../../GUI_Assets/MapWindow/program_sheet/tokens/damage.png", damageTokenSize, damageTokenSize, false, false);
	private Image notDamageToken = new Image("file:../../GUI_Assets/MapWindow/program_sheet/tokens/not_damage.png", damageTokenSize, damageTokenSize, false, false);
	private Image powerDownToken = new Image("file:../../GUI_Assets/MapWindow/program_sheet/tokens/power_down.png", powerDownTokenSize, powerDownTokenSize, false, false);
	
	//LOAD PIT
	private Image pit = new Image("file:../../GUI_Assets/MapWindow/pit.png", damageTokenSize, damageTokenSize, false, false);
	
	
	//LOAD BOTS
	private Image bot_1 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_1.png", mapTileSize, mapTileSize, false, false);
	private Image bot_2 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_2.png", mapTileSize, mapTileSize, false, false);
	private Image bot_3 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_3.png", mapTileSize, mapTileSize, false, false);
	private Image bot_4 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_4.png", mapTileSize, mapTileSize, false, false);
	private Image bot_5 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_5.png", mapTileSize, mapTileSize, false, false);
	private Image bot_6 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_6.png", mapTileSize, mapTileSize, false, false);
	private Image bot_7 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_7.png", mapTileSize, mapTileSize, false, false);
	private Image bot_8 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_8.png", mapTileSize, mapTileSize, false, false);
	private Image bot_9 = new Image("file:../../GUI_Assets/MapWindow/bots/bot_9.png", mapTileSize, mapTileSize, false, false);

	//LOAD LASERS
	private Image laser_emitter = new Image("file:../../GUI_Assets/MapWindow/lasers/laser_emitter.png", mapTileSize, mapTileSize, false, false);
	private Image laser_beam = new Image("file:../../GUI_Assets/MapWindow/lasers/laser_beam.png", mapTileSize, mapTileSize, false, false);
	
	private ArrayList<ImageView> belt_rot_right_fast_90_degree = new ArrayList();
	private ArrayList<ImageView> belt_rot_right_fast_180_degree = new ArrayList();
	private ArrayList<ImageView> belt_rot_right_fast_270_degree = new ArrayList();

	private ArrayList<ImageView> belt_straight_fast_90_degree = new ArrayList();
	private ArrayList<ImageView> belt_straight_fast_180_degree = new ArrayList();
	private ArrayList<ImageView> belt_straight_fast_270_degree = new ArrayList();

	private ArrayList<ImageView> walls_90_degree = new ArrayList();
	private ArrayList<ImageView> walls_180_degree = new ArrayList();
	private ArrayList<ImageView> walls_270_degree = new ArrayList();

	private ArrayList<ImageView> dockingBay_walls_90_degree = new ArrayList();
	private ArrayList<ImageView> dockingBay_walls_270_degree = new ArrayList();
	
	private ArrayList<ImageView> laserEmitter_180_degree = new ArrayList();
	private ArrayList<ImageView> laser_90_degree = new ArrayList();
	
	private ArrayList<ImageView> belt_rot_left_180_degree = new ArrayList();
	private ArrayList<ImageView> belt_rot_left_270_degree = new ArrayList();

	private ArrayList<ImageView> belt_rot_right_180_degree = new ArrayList();
	private ArrayList<ImageView> belt_rot_right_90_degree = new ArrayList();
	
	private ArrayList<ImageView> dockingBay_belt_rot_left_180_degree = new ArrayList();
	private ArrayList<ImageView> dockingBay_belt_rot_left_270_degree = new ArrayList();

	private ArrayList<ImageView> dockingBay_belt_rot_right_180_degree = new ArrayList();
	private ArrayList<ImageView> dockingBay_belt_rot_right_90_degree = new ArrayList();
	
	private ArrayList<ImageView> dockingBay_belt_straight_90_degree = new ArrayList();
	private ArrayList<ImageView> dockingBay_belt_straight_270_degree = new ArrayList();

	private ImageView[] handCards = new ImageView[9];
	
	private String completeMapString; 

	private iController gm = new GameMaster();
	

	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Events e = new Events(this);
		MouseEvents me = new MouseEvents(this);
		me.setGm(e.getGm());
//		setUpGameboard();
//		setupDockingBay();
		setupProgramSheet();
		
		deal.setOnAction(e);
		save.setOnAction(e);
		pwrDwn.setOnAction(e);
		finishProg.setOnAction(e);
		programCards.setOnMouseClicked(me);
		playerProgram.setOnMouseClicked(me);
		getPlayerInfo.setOnAction(e);
		
		
		//Arrange token gridPanes
		damageTokens.setTranslateX(22);
		damageTokens.setTranslateY(-185-(2*CARD_SIZE_Y));
		damageTokens.setHgap(13.0);
		
		lifeTokens.setTranslateX(175);
		lifeTokens.setTranslateY(-200-(2*CARD_SIZE_Y));
		lifeTokens.setHgap(3.0);
		
		powerDownTokens.setTranslateX(23);
		powerDownTokens.setTranslateY(-166-(2*CARD_SIZE_Y));
		
		//playerHand.setTranslateY(0);
		//programCards.setTranslateY(-100);
		completeMapString = e.getGm().getBoardString("all"); 
		refreshScreen(e.getGm().getCurrentPlayerInfo(), e.getGm().getAllPlayerMapInfo());
		
	}
	
	public void setupPlayerName(String playerName, int spawnNumber) {
		playerNameLabel.setText(playerName + ", it's your turn!");
		switch(spawnNumber) {
		case 1:
			playerNameLabel.setStyle("-fx-background-color: black;");
			break;
		case 2:
			playerNameLabel.setStyle("-fx-background-color: blue;");
			break;
		case 3:
			playerNameLabel.setStyle("-fx-background-color: green;");
			break;
		case 4:
			playerNameLabel.setStyle("-fx-background-color: orange;");
			break;
		case 5:
			playerNameLabel.setStyle("-fx-background-color: #FF00FF;");
			break;
		case 6:
			playerNameLabel.setStyle("-fx-background-color: red;");
			break;
		case 7:
			playerNameLabel.setStyle("-fx-background-color: turquoise;");
			break;
		case 8:
			playerNameLabel.setStyle("-fx-background-color: #D3D3D3;");
			break;
		case 9:
			playerNameLabel.setStyle("-fx-background-color: yellow;");
			break;
			
		}
	}

	public void dealCards() {
		System.out.println("deal cards");
			ArrayList <ImageView> cardIcons = new ArrayList();
			for (int e = 0; e < 9; e++) {
				cardIcons.add(new ImageView(back_up));
				cardIcons.get(e).setFitHeight(130);
				cardIcons.get(e).setPreserveRatio(true);
				playerHand.add(cardIcons.get(e), e, 0);
			}
	}
	
	public ImageView getCardTexture(String type) {
		switch(type) {
		case "card_base":
			return new ImageView(cardBase);

		case "BACK_UP":
			return new ImageView(back_up);
			
		case "MOVE_ONE":
			return new ImageView(move_1);
			
		case "MOVE_TWO":
			return new ImageView(move_2);
			
		case "MOVE_THREE":
			return new ImageView(move_3);
			
		case "ROTATE_LEFT":
			return new ImageView(rot_left);
			
		case "ROTATE_RIGHT":
			return new ImageView(rot_right);
			
		case "U_TURN":
			return new ImageView(u_turn);
		}
		return new ImageView(new Image("file:../../GUI_Assets/MapWindow/cards/icons/" + type + ".png", CARD_SIZE_X, CARD_SIZE_Y, false, false));
	}
	
	/**
	 * This method sets up the program Sheet 
	 */
	private void setupProgramSheet() {
		programSheet.getChildren().add(new ImageView(program_sheet));
	}
	
	private void setupDockingBay() {
		for (int i = 0; i < 12; i++) {
			for (int e = 0; e < 4; e++) {
				dockingBay.add(new javafx.scene.image.ImageView(baseField), i, e);
			}
		}
		
		dockingBay.add(new javafx.scene.image.ImageView(spawn_1), 5, 3);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_2), 6, 3);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_3), 3, 2);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_4), 8, 2);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_5), 1, 1);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_6), 10, 1);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_7), 0, 0);
		dockingBay.add(new javafx.scene.image.ImageView(spawn_8), 11, 0);

		dockingBay.add(new javafx.scene.image.ImageView(wall), 2, 0);
		dockingBay.add(new javafx.scene.image.ImageView(wall), 4, 0);
		dockingBay.add(new javafx.scene.image.ImageView(wall), 7, 0);
		dockingBay.add(new javafx.scene.image.ImageView(wall), 9, 0);

		addImageViewsToList(dockingBay_walls_90_degree, wall, 3);
		rotateImage(dockingBay_walls_90_degree, 90);
		
		dockingBay.add(dockingBay_walls_90_degree.get(0), 7, 0);
		dockingBay.add(dockingBay_walls_90_degree.get(1), 9, 1);
		dockingBay.add(dockingBay_walls_90_degree.get(2), 10, 1);
		
		addImageViewsToList(dockingBay_walls_270_degree, wall, 5);
		rotateImage(dockingBay_walls_270_degree, 270);

		dockingBay.add(dockingBay_walls_270_degree.get(0), 4, 0);
		dockingBay.add(dockingBay_walls_270_degree.get(1), 1, 1);
		dockingBay.add(dockingBay_walls_270_degree.get(2), 2, 1);
		dockingBay.add(dockingBay_walls_270_degree.get(3), 6, 2);
		dockingBay.add(dockingBay_walls_270_degree.get(4), 6, 3);

		addImageViewsToList(dockingBay_belt_straight_90_degree, belt_straight, 4);
		rotateImage(dockingBay_belt_straight_90_degree, 90);

		dockingBay.add(dockingBay_belt_straight_90_degree.get(0), 0, 2);
		dockingBay.add(dockingBay_belt_straight_90_degree.get(1), 1, 2);
		dockingBay.add(dockingBay_belt_straight_90_degree.get(2), 3, 3);
		dockingBay.add(dockingBay_belt_straight_90_degree.get(3), 4, 3);

		addImageViewsToList(dockingBay_belt_straight_270_degree, belt_straight, 4);
		rotateImage(dockingBay_belt_straight_270_degree, 270);

		dockingBay.add(dockingBay_belt_straight_270_degree.get(0), 11, 2);
		dockingBay.add(dockingBay_belt_straight_270_degree.get(1), 10, 2);
		dockingBay.add(dockingBay_belt_straight_270_degree.get(2), 8, 3);
		dockingBay.add(dockingBay_belt_straight_270_degree.get(3), 7, 3);

		addImageViewsToList(dockingBay_belt_rot_left_180_degree, belt_rot_left, 1);
		rotateImage(dockingBay_belt_rot_left_180_degree, 180);

		dockingBay.add(dockingBay_belt_rot_left_180_degree.get(0), 2, 3);

		addImageViewsToList(dockingBay_belt_rot_left_270_degree, belt_rot_left, 1);
		rotateImage(dockingBay_belt_rot_left_270_degree, 270);

		dockingBay.add(dockingBay_belt_rot_left_270_degree.get(0), 9, 2);

		// belt_rot_right_180_degree

		addImageViewsToList(dockingBay_belt_rot_right_180_degree, belt_rot_right, 1);
		rotateImage(dockingBay_belt_rot_right_180_degree, 180);
		dockingBay.add(dockingBay_belt_rot_right_180_degree.get(0), 9, 3);

		addImageViewsToList(dockingBay_belt_rot_right_90_degree, belt_rot_right, 1);
		rotateImage(dockingBay_belt_rot_right_90_degree, 90);
		dockingBay.add(dockingBay_belt_rot_right_90_degree.get(0), 2, 2);
		
//		dockingBay.setTranslateY(-180);
		
	}

	/**
	 * Sets up the gameboard
	 */
	private void setUpGameboard() {
		// setup the base fields
				for (int x = 0; x < 12; x++) {
					for (int y = 0; y < 12; y++) {
						gameboard.add(new javafx.scene.image.ImageView(baseField), x, y);
					}
				}

				gameboard.add(new javafx.scene.image.ImageView(belt_rot_right_fast), 1, 1);
				gameboard.add(new javafx.scene.image.ImageView(belt_rot_right_fast), 7, 1);
				gameboard.add(new javafx.scene.image.ImageView(belt_rot_right_fast), 1, 7);
				gameboard.add(new javafx.scene.image.ImageView(belt_rot_right_fast), 7, 7);

				addImageViewsToList(belt_rot_right_fast_90_degree, belt_rot_right_fast, 4);
				rotateImage(belt_rot_right_fast_90_degree, 90);

				gameboard.add(belt_rot_right_fast_90_degree.get(0), 4, 1);
				gameboard.add(belt_rot_right_fast_90_degree.get(1), 10, 1);
				gameboard.add(belt_rot_right_fast_90_degree.get(2), 4, 7);
				gameboard.add(belt_rot_right_fast_90_degree.get(3), 10, 7);

				addImageViewsToList(belt_rot_right_fast_180_degree, belt_rot_right_fast, 4);
				rotateImage(belt_rot_right_fast_180_degree, 180);

				gameboard.add(belt_rot_right_fast_180_degree.get(0), 4, 4);
				gameboard.add(belt_rot_right_fast_180_degree.get(1), 10, 4);
				gameboard.add(belt_rot_right_fast_180_degree.get(2), 4, 10);
				gameboard.add(belt_rot_right_fast_180_degree.get(3), 10, 10);

				addImageViewsToList(belt_rot_right_fast_270_degree, belt_rot_right_fast, 4);

				rotateImage(belt_rot_right_fast_270_degree, 270);

				gameboard.add(belt_rot_right_fast_270_degree.get(0), 1, 4);
				gameboard.add(belt_rot_right_fast_270_degree.get(1), 7, 4);
				gameboard.add(belt_rot_right_fast_270_degree.get(2), 1, 10);
				gameboard.add(belt_rot_right_fast_270_degree.get(3), 7, 10);

				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 1, 2);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 1, 3);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 1, 8);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 1, 9);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 7, 2);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 7, 3);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 7, 8);
				gameboard.add(new javafx.scene.image.ImageView(belt_straight_fast), 7, 9);

				addImageViewsToList(belt_straight_fast_90_degree, belt_straight_fast, 8);

				rotateImage(belt_straight_fast_90_degree, 90);

				gameboard.add(belt_straight_fast_90_degree.get(0), 2, 1);
				gameboard.add(belt_straight_fast_90_degree.get(1), 3, 1);
				gameboard.add(belt_straight_fast_90_degree.get(2), 8, 1);
				gameboard.add(belt_straight_fast_90_degree.get(3), 9, 1);
				gameboard.add(belt_straight_fast_90_degree.get(4), 2, 7);
				gameboard.add(belt_straight_fast_90_degree.get(5), 3, 7);
				gameboard.add(belt_straight_fast_90_degree.get(6), 8, 7);
				gameboard.add(belt_straight_fast_90_degree.get(7), 9, 7);

				addImageViewsToList(belt_straight_fast_180_degree, belt_straight_fast, 8);

				rotateImage(belt_straight_fast_180_degree, 180);

				gameboard.add(belt_straight_fast_180_degree.get(0), 4, 2);
				gameboard.add(belt_straight_fast_180_degree.get(1), 4, 3);
				gameboard.add(belt_straight_fast_180_degree.get(2), 10, 2);
				gameboard.add(belt_straight_fast_180_degree.get(3), 10, 3);
				gameboard.add(belt_straight_fast_180_degree.get(4), 4, 8);
				gameboard.add(belt_straight_fast_180_degree.get(5), 4, 9);
				gameboard.add(belt_straight_fast_180_degree.get(6), 10, 8);
				gameboard.add(belt_straight_fast_180_degree.get(7), 10, 9);

				addImageViewsToList(belt_straight_fast_270_degree, belt_straight_fast, 8);

				rotateImage(belt_straight_fast_270_degree, 270);

				gameboard.add(belt_straight_fast_270_degree.get(0), 2, 4);
				gameboard.add(belt_straight_fast_270_degree.get(1), 3, 4);
				gameboard.add(belt_straight_fast_270_degree.get(2), 8, 4);
				gameboard.add(belt_straight_fast_270_degree.get(3), 9, 4);
				gameboard.add(belt_straight_fast_270_degree.get(4), 2, 10);
				gameboard.add(belt_straight_fast_270_degree.get(5), 3, 10);
				gameboard.add(belt_straight_fast_270_degree.get(6), 8, 10);
				gameboard.add(belt_straight_fast_270_degree.get(7), 9, 10);

				gameboard.add(new javafx.scene.image.ImageView(wall), 2, 0);
				gameboard.add(new javafx.scene.image.ImageView(wall), 4, 0);
				gameboard.add(new javafx.scene.image.ImageView(wall), 7, 0);
				gameboard.add(new javafx.scene.image.ImageView(wall), 9, 0);
				gameboard.add(new javafx.scene.image.ImageView(wall), 8, 5);
				gameboard.add(new javafx.scene.image.ImageView(wall), 8, 9);

				addImageViewsToList(walls_90_degree, wall, 6);

				rotateImage(walls_90_degree, 90);
				

				gameboard.add(walls_90_degree.get(0), 11, 2);
				gameboard.add(walls_90_degree.get(1), 11, 4);
				gameboard.add(walls_90_degree.get(2), 11, 7);
				gameboard.add(walls_90_degree.get(3), 11, 9);
				gameboard.add(walls_90_degree.get(4), 6, 3);
				gameboard.add(walls_90_degree.get(5), 6, 8);
			
				
				addImageViewsToList(walls_180_degree, wall, 6);

				rotateImage(walls_180_degree, 180);

				gameboard.add(walls_180_degree.get(0), 2, 11);
				gameboard.add(walls_180_degree.get(1), 4, 11);
				gameboard.add(walls_180_degree.get(2), 7, 11);
				gameboard.add(walls_180_degree.get(3), 9, 11);
				gameboard.add(walls_180_degree.get(4), 3, 2);
				gameboard.add(walls_180_degree.get(5), 3, 6);

				addImageViewsToList(walls_270_degree, wall, 6);

				rotateImage(walls_270_degree, 270);

				gameboard.add(walls_270_degree.get(0), 0, 2);
				gameboard.add(walls_270_degree.get(1), 0, 4);
				gameboard.add(walls_270_degree.get(2), 0, 7);
				gameboard.add(walls_270_degree.get(3), 0, 9);
				gameboard.add(walls_270_degree.get(4), 5, 8);
				gameboard.add(walls_270_degree.get(5), 5, 3);

				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 2, 2);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 3, 3);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 8, 2);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 9, 3);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 2, 8);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 3, 9);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 8, 8);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_right), 9, 9);

				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 5, 2);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 6, 4);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 4, 5);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 2, 6);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 5, 7);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 7, 6);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 9, 5);
				gameboard.add(new javafx.scene.image.ImageView(gear_rot_left), 6, 9);

				gameboard.add(new javafx.scene.image.ImageView(flag_1), 5, 4);
				gameboard.add(new javafx.scene.image.ImageView(flag_2), 10, 11);
				gameboard.add(new javafx.scene.image.ImageView(flag_3), 1, 6);

				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 3, 3);
				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 3, 4);
				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 3, 5);
				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 3, 6);
//				gameboard.add(new javafx.scene.image.ImageView(laser_source), 8, 5);
				gameboard.add(new javafx.scene.image.ImageView(laser_emitter), 8, 5);
				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 8, 6);
				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 8, 7);
				gameboard.add(new javafx.scene.image.ImageView(laser_beam), 8, 8);

//				addImageViewsToList(laserEmitter_180_degree, laser_source, 1);
				addImageViewsToList(laserEmitter_180_degree, laser_emitter, 1);

				rotateImage(laserEmitter_180_degree, 180);

				gameboard.add(laserEmitter_180_degree.get(0), 3, 6);
				gameboard.add(laserEmitter_180_degree.get(1), 3, 6);

				addImageViewsToList(laser_90_degree, laser_beam, 4);

				rotateImage(laser_90_degree, 90);

				gameboard.add(laser_90_degree.get(0), 5, 3);
				gameboard.add(laser_90_degree.get(1), 6, 3);
				gameboard.add(laser_90_degree.get(2), 5, 8);
				gameboard.add(laser_90_degree.get(3), 6, 8);

				gameboard.add(new javafx.scene.image.ImageView(repair_1), 2, 3);
				gameboard.add(new javafx.scene.image.ImageView(repair_1), 9, 8);
				gameboard.add(new javafx.scene.image.ImageView(repair_2), 8, 3);
				gameboard.add(new javafx.scene.image.ImageView(repair_2), 3, 8);
			
	}

	/**
	 * rotates alle the images in the list by the given degree.
	 * 
	 * @param listWithIVs
	 *            List contains imageViews with the images to be rotated
	 * @param degree
	 *            can only be 90 , 180 or 270
	 * @throws IOException
	 *             thrown if degree is not 90, 180 or 270
	 */
	private void rotateImage(ArrayList<ImageView> listWithIVs, int degree) {

		for (ImageView iv : listWithIVs) {
			iv.setRotate(degree);
		}
	}

	/**
	 * This method adds the given Image to the given ArrayList.
	 * 
	 * @param toBeFilledWithImageViews
	 *            The List you want to fill with Images
	 * @param image
	 *            The image you want to add
	 * @param amountOfImages
	 *            How many times do you want to add the picture
	 */
	private void addImageViewsToList(ArrayList<ImageView> toBeFilledWithImageViews, Image image, int amountOfImages) {
		for (int a = 0; a < amountOfImages; a++) {
			toBeFilledWithImageViews.add(new ImageView(image));
		}
	}

	/**
	 * Clears and redraws the whole screen
	 * @param playerString
	 * @param AllPlayerMapInfo
	 */
	public void refreshScreen(String playerString, String AllPlayerMapInfo) {
		updateCenterPane(playerString);
		updateLeftPane(AllPlayerMapInfo);
		if(!(gm.winningPlayer() == null)) {
			Alert win = new Alert(AlertType.INFORMATION);
			win.setContentText(gm.winningPlayer().getName() + " has won the Game!");
		}
	}
	
	/**
	 * Clears and redraws everything in the left pane of the window's border pane
	 * @param AllPlayerMapInfo
	 */
	private void updateLeftPane(String AllPlayerMapInfo) {
		String[] playerMapInfo = AllPlayerMapInfo.split("\\|");
		GridPane paneDrawTarget;
		dockingBay.getChildren().clear();
		gameboard.getChildren().clear();
		drawCourse();
		
		for(int i=0; i<playerMapInfo.length; i++) {
			String[] playerInfo = playerMapInfo[i].split(":");
			if(playerInfo[4].equals("dockingBay")) {
				paneDrawTarget = getDockingBay();
			} else /*if(playerMapInfo[4].equals("dizzyDash"))*/{
				paneDrawTarget = getGameboard();
			}
			String[] tempPos = playerInfo[0].split("_");
			
			ImageView botToDraw = createBotImageView(playerInfo[2], playerInfo[3]);
			if(botToDraw != null) {
				System.out.println("Drawing bot at: " + tempPos[0] + "|" + tempPos[1] + " in " + playerInfo[4]);
				paneDrawTarget.add(botToDraw, Integer.valueOf(tempPos[0]), Integer.valueOf(tempPos[1]));
			}
			
		}
		
	}
	
	/**
	 * Clears and redraws everything in the center pane of the window's border pane
	 * @param playerString
	 */
	private void updateCenterPane(String playerString) {
		//Clear Panes
		programCards.getChildren().clear();
		playerProgram.getChildren().clear();
		
		String[] playerInfo = playerString.split(",");
		setupPlayerName(playerInfo[0], Integer.valueOf(playerInfo[1]));
		
		//draw programSheet items

				//Life tokens
				for(int i=0; i< Integer.valueOf(playerInfo[4]); i++) {
					lifeTokens.add(new ImageView(lifeToken), 2-i, 0);
				}
				
				//DamageTokens
				for(int i=0; i<10; i++) {
					if(i<10-Integer.valueOf(playerInfo[5])) {
						damageTokens.add(new ImageView(notDamageToken), i, 0);
					} else {
						damageTokens.add(new ImageView(damageToken), i, 0);
					}
				}
				
				//power down
				if(Boolean.getBoolean(playerInfo[6]) == true) {
					powerDownTokens.add(new ImageView(powerDownToken), 0, 0);
				}
		//Draw hand
		drawCardsToPane(getProgramCards(), playerInfo[2].split(";"));
		
		//DrawProgram
		drawCardsToPane(getPlayerProgram(), playerInfo[7].split(";"));

		
	}
	
	/**
	 * Draws programcards to the target GridPane
	 * @param gp target
	 * @param cards to draw
	 */
	private void drawCardsToPane(GridPane gp, String[] cards) {
		
		
		//Player Hand
		for(int i=0; i<cards.length; i++) {
			//"." is a regex metacharacter which has to be escaped
			String[] card = cards[i].split("\\.");
			if(card[0].equals("null")) {
				ImageView base = new ImageView(noCardBase);
				gp.add(base, i, 0);
			} else {
				char[] digits = card[1].toCharArray();
				ImageView base = new ImageView(cardBase);
				
				gp.add(base, i, 0);
				ImageView typeImage = getCardTexture(card[0]);
				gp.add(typeImage, i, 0);
				
				//draw digits
				for(int j=digits.length-1; j>-1; j--) {
					//All these hardcoded numbers were obtained by trial and error. There probably is a better way.
					ImageView tempDigit = createPrioDigitIV(digits[j]);
					//20 -> Standart offset for centering. 
					//25 -> Distance between digits
					//All positins are relative to the position of the place of the GridPane
					tempDigit.setTranslateX(20+25*j);
					tempDigit.setTranslateY(-63);
					
					
					gp.add(tempDigit, i, 0);
	//				Fancy 0 in front of double digit priorities
	//				if(digits.length == 2) {
	//					tempDigit = new ImageView(new Image("file:../../GUI_Assets/MapWindow/cards/priority_digits/priority_0.png", 20, 40, false, false));
	//					//20 -> Standart offset for centering. 
	//					//25 -> Distance between digits
	//					//All positins are relative to the position of the place of the GridPane
	//					tempDigit.setTranslateX(20+25*0);
	//					tempDigit.setTranslateY(-63);
	//					myPane.add(tempDigit, i, 0);
	//				}
				}
			}
		}
	}
	
	/**
	 * Draws the whole course without robots
	 */
	private void drawCourse() {
		/* 
		 * This works in four phases
		 * 1: Draw all the Base Field texture
		 * 2: Draw the base Field Types
		 * 3: Draw lasers
		 * 4: Draw walls
		 */
		
		//Phase 1
		//Game Field
		for(int x=0; x<12; x++) {
			for(int y=0; y<12; y++) {
				gameboard.add(new ImageView(baseField), x, y);
			}
		}
		//Docking Bay
		for(int x=0; x<12; x++) {
			for(int y=0; y<4; y++) {
				dockingBay.add(new ImageView(baseField), x, y);
			}
		}
		
		//Split board string
		//;
		String[] splittedBoard = completeMapString.split(";");
		//Phase 2
		//Game Field
		
		String[] fields = splittedBoard[0].split(":");	
		
		for(int x=0; x<12; x++) {
			for(int y=0; y<12; y++) {
				String singleField[] = fields[(y*12)+x].split(",");
				ImageView tempIV = createFieldTypeImageView(singleField[0], singleField[1], singleField[4]);
				if(tempIV != null) {
					gameboard.add(tempIV, y, x);
				}
			}
		}
		
		//Docking Bay
		
		fields = splittedBoard[1].split(":");	
		
		for(int y=0; y<4; y++) {
			for(int x=0; x<12; x++) {
				String singleField[] = fields[y+(x*4)].split(",");
				ImageView tempIV = createFieldTypeImageView(singleField[0], singleField[1], singleField[4]);
				if(tempIV != null) {
					dockingBay.add(tempIV, x, y);
				}
			}
		}
		
		//Phase 3
		//Game Field
		fields = splittedBoard[0].split(":");	
		for(int x=0; x<12; x++) {
			for(int y=0; y<12; y++) {
				String singleField[] = fields[(y*12)+x].split(",");
				ImageView laserIVToDraw = createLaserImageView(singleField[3], singleField[2]);
				if(laserIVToDraw != null) {
					gameboard.add(laserIVToDraw, y, x);
				}
			}
		}
		
		//Docking Bay
		fields = splittedBoard[1].split(":");	
		for(int y=0; y<4; y++) {
			for(int x=0; x<12; x++) {
				String singleField[] = fields[y+(x*4)].split(",");
				ImageView laserIVToDraw = createLaserImageView(singleField[3], singleField[2]);
				if(laserIVToDraw != null) {
					dockingBay.add(createLaserImageView(singleField[3], singleField[2]), x, y);
				}
			}
		}
		
		//Phase 4
		//Game Field
		fields = splittedBoard[0].split(":");
		
		for(int y=0; y<12; y++) {
			for(int x=0; x<12; x++) {
				String singleField[] = fields[(y*12)+x].split(",");
				ImageView[] walls = createWallsImageViews(singleField[2]);
				for(int i=0; i<4; i++) {
					if(walls[i] != null) {
						gameboard.add(walls[i], y, x);
					}
				}
			}
		}
		
		//Docking Bay
		fields = splittedBoard[1].split(":");
		
		for(int y=0; y<4; y++) {
			for(int x=0; x<12; x++) {
				String singleField[] = fields[y+(x*4)].split(",");
				ImageView[] walls = createWallsImageViews(singleField[2]);
				for(int i=0; i<4; i++) {
					if(walls[i] != null) {
						dockingBay.add(walls[i], x, y);
					}
				}
			}
		}
	}

	/**
	 * Creates a ready-to-draw ImageView of the major FieldTypes.
	 * @param type String form of the FiedType Enum
	 * @param rotation String form of the Direction Enum
	 * @param index String form of the index Attribute of a field
	 * @return The resulting ImageView with the correct texture and proper rotation, that can immediately be drawn.
	 */
	private ImageView createFieldTypeImageView(String type, String rotation, String index) {
		double rotate;
		ImageView tempIV = new ImageView(missing_field);
		switch(rotation) {
			case "NORTH":
				rotate = 0;
				break;
			case "EAST":
				rotate = 90;
				break;
			case "SOUTH":
				rotate = 180;
				break;
			case "WEST":
				rotate = 270;
				break;
			case "NONE":
				rotate = 0;
				break;
			default:
				rotate = 0;
				System.err.println("The rotation" + rotation + "wasn't understood!");
		}
		switch(type) {
			case "EMPTY":
				return null;
				
			case "FLAG":
				switch(index) {
					case "1":
						tempIV =  new ImageView(flag_1);
						break;
					case "2":
						tempIV =  new ImageView(flag_2);
						break;
					case "3":
						tempIV =  new ImageView(flag_3);
						break;
					case "4":
						tempIV =  new ImageView(flag_4);
						break;
					default:
						System.err.println("The flag index " + index + " does not exist!");
				}
				break;
				
			case "CONVEYOR_BELT":
				tempIV = new ImageView(belt_straight);
				break;
				
			case "CONVEYOR_BELT_ROT_LEFT":
				tempIV = new ImageView(belt_rot_left);
				break;
				
			case "CONVEYOR_BELT_ROT_RIGHT":
				tempIV = new ImageView(belt_rot_right);
				break;
				
			case "CONVEYOR_BELT_FAST":
				tempIV = new ImageView(belt_straight_fast);
				break;
				
			case "CONVEYOR_BELT_ROT_LEFT_FAST":
				tempIV = new ImageView(belt_rot_left_fast);
				break;
				
			case "CONVEYOR_BELT_ROT_RIGHT_FAST":
				tempIV = new ImageView(belt_rot_right_fast);
				break;
				
			case "GEAR_ROT_LEFT":
				tempIV = new ImageView(gear_rot_left);
				break;
				
			case "GEAR_ROT_RIGHT":
				tempIV = new ImageView(gear_rot_right);
				break;
				
			case "REPAIR_ONE":
				tempIV = new ImageView(repair_1);
				break;
				
			case "REPAIR_TWO":
				tempIV = new ImageView(repair_2);
				break;
				
			case "PIT":
				tempIV = new ImageView(pit);
				break;
				
			case "SPAWN":
				rotate = 0;
				switch(index) {
				case "1":
					tempIV =  new ImageView(spawn_1);
					break;
				case "2":
					tempIV =  new ImageView(spawn_2);
					break;
				case "3":
					tempIV =  new ImageView(spawn_3);
					break;
				case "4":
					tempIV =  new ImageView(spawn_4);
					break;
				case "5":
					tempIV =  new ImageView(spawn_5);
					break;
				case "6":
					tempIV =  new ImageView(spawn_6);
					break;
				case "7":
					tempIV =  new ImageView(spawn_7);
					break;
				case "8":
					tempIV =  new ImageView(spawn_8);
					break;
				default:
					System.err.println("The spawn index " + index + "does not exist!");
				}
				break;
		}
		
		tempIV.setRotate(rotate);
		return tempIV;
	}
	
	/**
	 * Creates an array of ready-to-draw ImageViews of the walls of a field.
	 * @param String form of the walls Attribute of a field
	 * @return The resulting ImageView array with the wall textures correctly rotated that can immediately be drawn.
	 */
	private ImageView[] createWallsImageViews(String walls) {
		String[] wallsSplit = walls.split("\\.");
		ImageView[] retWalls = {new ImageView(missing_field),
								new ImageView(missing_field),
								new ImageView(missing_field),
								new ImageView(missing_field),
								};
		
		
		for(int i=0; i<wallsSplit.length; i++) {
			if(wallsSplit[i].equals("true")) {
				retWalls[i] = new ImageView(wall);
				retWalls[i].setRotate(i*90);
			}else {
				retWalls[i] = null;
			}
		}
		return retWalls;
	}
	
	/**
	 * 
	 * @param lasers
	 * @return
	 */
	private ImageView createLaserImageView(String lasers, String walls) {
		ImageView retIV = null;
		String[] lasersSplit = lasers.split("\\.");
		String[] wallsSplit = walls.split("\\.");
		
		for(int i=0; i<lasersSplit.length; i++) {
			if(!lasersSplit[i].equals("0")) {
				retIV = new ImageView(missing_field);
				if(wallsSplit[i].equals("true")) {
					retIV = new ImageView(laser_emitter);
					retIV.setRotate(i*90);
					return retIV;
				} else {
					retIV = new ImageView(laser_beam);
					retIV.setRotate(i*90);
					return retIV;
				}
				
			}
		}
		return retIV;
	}
	
	/**
	 * Creates an ImageView of a bot based on given paramteters
	 * @param spawnNumber determines the color of the bot
	 * @param rotation determines the rotation of the image
	 * @return
	 */
	private ImageView createBotImageView(String spawnNumber, String rotation) {
		ImageView retIV = new ImageView(missing_field);
		
		switch(spawnNumber) {
		case "1":
			retIV = new ImageView(bot_1);
			break;
			
		case "2":
			retIV = new ImageView(bot_2);
			break;
			
		case "3":
			retIV = new ImageView(bot_3);
			break;
			
		case "4":
			retIV = new ImageView(bot_4);
			break;
			
		case "5":
			retIV = new ImageView(bot_5);
			break;
			
		case "6":
			retIV = new ImageView(bot_6);
			break;
			
		case "7":
			retIV = new ImageView(bot_7);
			break;
			
		case "8":
			retIV = new ImageView(bot_8);
			break;
			
		case "9":
			retIV = new ImageView(bot_9);
			break;
		}
		
		switch(rotation) {
		case "NORTH":
			retIV.setRotate(0);
			break;
			
		case "EAST":
			retIV.setRotate(90);
			break;
			
		case "SOUTH":
			retIV.setRotate(180);
			break;
			
		case "WEST":
			retIV.setRotate(270);
			break;
					
		}
		
		return retIV;
	}
	
	/**
	 * Creates an ImageView of a priority digit via the given parameter
	 * @param digit decides the priority digit
	 * @return ImageView with the priority digit texture on it
	 */
	private ImageView createPrioDigitIV(char digit) {
		switch(digit) {
			case '0':
				return new ImageView(prio_0);
				
			case '1':
				return new ImageView(prio_1);
				
			case '2':
				return new ImageView(prio_2);
				
			case '3':
				return new ImageView(prio_3);
				
			case '4':
				return new ImageView(prio_4);
				
			case '5':
				return new ImageView(prio_5);
			
			case '6':
				return new ImageView(prio_6);
				
			case '7':
				return new ImageView(prio_7);
			
			case '8':
				return new ImageView(prio_8);
			
			case '9':
				return new ImageView(prio_9);
				
			default:
				return new ImageView(MISSINGNO);
		}
	}
	//=============================GETTER/SETTER======================
	public GridPane getGameboard() {
		return gameboard;
	}

	public void setGameboard(GridPane gameboard) {
		this.gameboard = gameboard;
	}

	public GridPane getDockingBay() {
		return dockingBay;
	}

	public void setDockingBay(GridPane dockingBay) {
		this.dockingBay = dockingBay;
	}
	
	public GridPane getProgramCards() {
		return programCards;
	}

	public void setProgramCards(GridPane programCards) {
		this.programCards = programCards;
	}

	public Image getProgram_sheet() {
		return program_sheet;
	}

	public void setProgram_sheet(Image program_sheet) {
		this.program_sheet = program_sheet;
	}

	public GridPane getPlayerHand() {
		return playerHand;
	}

	public void setPlayerHand(GridPane playerHand) {
		this.playerHand = playerHand;
	}

	public GridPane getPlayerProgram() {
		return playerProgram;
	}
	
	public void setPlayerProgram(GridPane playerProgram) {
		this.playerProgram = playerProgram;
	}
	public ImageView getBotImage(int n) {
		return createBotImageView(Integer.toString(n), "NORTH");
	}
}
