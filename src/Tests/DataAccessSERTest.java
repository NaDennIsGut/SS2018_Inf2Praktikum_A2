package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.GameMaster;
import Streams.DataAccessSER;

/**
 * 
 * @author Leila Taraman
 *
 */
class DataAccessSERTest {
	
	private static DataAccessSER ser;
	private static GameMaster gamemaster;
	
	/**
	 * prepare the game
	 */
	@BeforeAll
	public static void start() {
		ser = new DataAccessSER();
		gamemaster = new GameMaster();
		gamemaster.addPlayer("Dennis");
		gamemaster.addPlayer("Arthur");
		gamemaster.addPlayer("Filip");
		gamemaster.addPlayer("Leila");
		gamemaster.startGame();
	}
	
	/**
	 * tests if something was saved
	 */
	@Test
	public void testSave() {
		try {
			ser.save(gamemaster, "test.ser");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File file = new File("test.ser");
		assertTrue(file.exists());
	}
	
	/**
	 * tests if something was loaded and if it was correctly
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Test
	public void testLoad() throws FileNotFoundException, IOException {
		ser.save(gamemaster, "test.ser");
		GameMaster gm = (GameMaster) ser.load("test.ser");
		assertTrue(gamemaster.equals(gm));
	}

}
