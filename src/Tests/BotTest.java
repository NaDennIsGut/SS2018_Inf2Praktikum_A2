package Tests;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.Bot;
import Backend.ProgramCard;
import Enums.CardType;
import Enums.Direction;
import Exceptions.BotException;

public class BotTest {
	private static String maps = "EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:FLAG,NONE,false.false.false.false.,0.0.0.0.,3:CONVEYOR_BELT_ROT_RIGHT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.0.0.,0:REPAIR_ONE,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,SOUTH,false.false.true.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,SOUTH,false.false.true.false.,0.0.1.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.1.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.1.0.,0:EMPTY,NONE,false.false.false.false.,0.0.1.0.,0:EMPTY,SOUTH,false.false.true.false.,0.0.1.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:REPAIR_TWO,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,SOUTH,false.false.true.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.1.,0:FLAG,NONE,false.false.false.false.,0.0.0.0.,1:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.1.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,EAST,false.true.false.false.,0.1.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,EAST,false.true.false.false.,0.1.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,NORTH,false.false.false.false.,0.0.0.0.,0:EMPTY,SOUTH,false.false.true.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.0.0.,0:REPAIR_TWO,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,1.0.0.0.,0:EMPTY,NONE,false.false.false.false.,1.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,1.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,1.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,1.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_LEFT,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,EAST,false.false.false.false.,0.0.0.0.,0:REPAIR_ONE,NONE,false.false.false.false.,0.0.0.0.,0:GEAR_ROT_RIGHT,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,SOUTH,false.false.true.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_FAST,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT_FAST,WEST,false.false.false.false.,0.0.0.0.,0:FLAG,NONE,false.false.false.false.,0.0.0.0.,2:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,EAST,false.true.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,EAST,false.true.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,EAST,false.true.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,EAST,false.true.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:;SPAWN,NONE,false.false.false.false.,0.0.0.0.,7:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:SPAWN,WEST,false.false.false.true.,0.0.0.0.,5:CONVEYOR_BELT,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_LEFT,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:SPAWN,NONE,false.false.false.false.,0.0.0.0.,3:CONVEYOR_BELT,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.true.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT,EAST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:SPAWN,NONE,false.false.false.false.,0.0.0.0.,1:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:SPAWN,NONE,false.false.false.true.,0.0.0.0.,2:EMPTY,NORTH,true.true.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:SPAWN,NONE,false.false.false.false.,0.0.0.0.,4:CONVEYOR_BELT,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NORTH,true.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_LEFT,SOUTH,false.false.false.false.,0.0.0.0.,0:CONVEYOR_BELT_ROT_RIGHT,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:SPAWN,WEST,false.false.false.true.,0.0.0.0.,6:CONVEYOR_BELT,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:SPAWN,NONE,false.false.false.false.,0.0.0.0.,8:EMPTY,WEST,false.false.false.true.,0.0.0.0.,0:CONVEYOR_BELT,WEST,false.false.false.false.,0.0.0.0.,0:EMPTY,NONE,false.false.false.false.,0.0.0.0.,0:;";

	private static Bot b;
	private static ArrayList<ProgramCard> c9;
	private static ArrayList<ProgramCard> c10;
	private static ProgramCard testCard;
	private static ArrayList<String> tempStr;
	
	@BeforeAll
	static void init() {
		c9 = new ArrayList<ProgramCard>();
		c10 = new ArrayList<ProgramCard>();
		
		try {
			testCard = new ProgramCard(900, CardType.MOVE_ONE);
			

			b = new Bot(1, 3, 8, 8, "He-Man", Direction.NORTH, "GameBoard");
			c9.add(new ProgramCard(90, CardType.BACK_UP));
			c9.add(new ProgramCard(100, CardType.MOVE_ONE));
			c9.add(new ProgramCard(110, CardType.MOVE_TWO));
			c9.add(new ProgramCard(120, CardType.ROTATE_LEFT));
			c9.add(new ProgramCard(130, CardType.ROTATE_RIGHT));
			c9.add(new ProgramCard(140, CardType.U_TURN));
			c9.add(new ProgramCard(150, CardType.BACK_UP));
			c9.add(new ProgramCard(160, CardType.MOVE_THREE));
			c9.add(new ProgramCard(170, CardType.MOVE_TWO));
			
			c10.add(new ProgramCard(90, CardType.BACK_UP));
			c10.add(new ProgramCard(100, CardType.MOVE_ONE));
			c10.add(new ProgramCard(110, CardType.MOVE_TWO));
			c10.add(new ProgramCard(120, CardType.ROTATE_LEFT));
			c10.add(new ProgramCard(130, CardType.ROTATE_RIGHT));
			c10.add(new ProgramCard(140, CardType.U_TURN));
			c10.add(new ProgramCard(150, CardType.BACK_UP));
			c10.add(new ProgramCard(160, CardType.MOVE_ONE));
			c10.add(new ProgramCard(170, CardType.MOVE_TWO));
			c10.add(new ProgramCard(170, CardType.MOVE_TWO));
			
		} catch(NullPointerException e){
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		b.setProgramCards(c9);
		b.debugSetMaps(maps);
	}
	
	@Test
	void testRunRoutine(){
		try {
			b.setLastTouchedFlag(2);
			assertTrue(b.debugRunRoutine(5));
			b.setPos(new int[] {5, 5});
			b.setFaceDirection(Direction.WEST);
			assertTrue(b.debugRunRoutine(5));
		} catch(BotException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	//Public tests
	
//	@Test
//	void testGetNextFieldWalls() {
//		
//		try {
//			{
//				String[] expectedWalls = {"false.false.false.false.","false.false.false.false."};
//				String[] actualWalls = b.getNextFieldWalls(new int[] {1, 0}, Direction.NORTH);
//				
//				assertEquals(expectedWalls[0], actualWalls[0]);
//				assertEquals(expectedWalls[1], actualWalls[1]);
//			}
//			{
//				String[] expectedWalls = {"true.false.false.false.","false.false.false.false."};
//				String[] actualWalls = b.getNextFieldWalls(new int[] {0, 2}, Direction.NORTH);
//				
//				assertEquals(expectedWalls[0], actualWalls[0]);
//				assertEquals(expectedWalls[1], actualWalls[1]);
//			}
//		} catch (BotException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
//	@Test
//	void testLookUpWalls() {
//		
//		try {
//				assertEquals(3, b.lookUpWalls(new int[] {0, 0}, Direction.NORTH, 3));
//				assertEquals(0, b.lookUpWalls(new int[] {2, 3}, Direction.SOUTH, 3));
//				assertEquals(0, b.lookUpWalls(new int[] {8, 5}, Direction.WEST, 3));
//		} catch (BotException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
}
