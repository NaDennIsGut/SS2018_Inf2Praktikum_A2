package Tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.ProgramCard;
import Enums.CardType;

class ProgramCardTest {
	private static ProgramCard c;
	
	@BeforeAll
	static void initObject() {
		try {
			c = new ProgramCard(18279364, CardType.ROTATE_LEFT);
		} catch(Exception e){
			e.getMessage();
		}
	}
	
	@Test
	void testConstructor() {
		assertThrows(NullPointerException.class,() -> new ProgramCard(0, null));
		
		assertNotNull(c);
	}
	
	@Test
	void testGetters() {
		assertTrue(c.getType() == CardType.ROTATE_LEFT && c.getPriorityNumber() == 18279364);
	}
}
