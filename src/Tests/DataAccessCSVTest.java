package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.GameMaster;
import Streams.DataAccessCSV;
/**
 * 
 * @author Leila Taraman
 *
 */
class DataAccessCSVTest {

	private static DataAccessCSV csv;
	private static GameMaster gamemaster;
	
	/**
	 * prepare the game
	 */
	@BeforeAll
	public static void start() {
		csv = new DataAccessCSV();
		gamemaster = new GameMaster();
		gamemaster.addPlayer("Dennis");
		gamemaster.addPlayer("Arthur");
		gamemaster.addPlayer("Filip");
		gamemaster.addPlayer("Leila");
		gamemaster.startGame();
	}
	
	/**
	 * tests if something was saved
	 */
	@Test
	public void testSave() {
		try {
			csv.save(gamemaster, "test.csv");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		File file = new File("test.csv");
		assertTrue(file.exists());
	}
	
	/**
	 * tests if something was loaded and if it was correctly
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Test
	public void testLoad() throws FileNotFoundException, IOException {
		csv.save(gamemaster, "test.csv");
		GameMaster gm = new GameMaster();
		gm.load("test.csv");
		assertTrue(gamemaster.equals(gm));
	}


}
