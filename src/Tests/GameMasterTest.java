package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HandshakeCompletedEvent;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Backend.*;
import Enums.*;
import Exceptions.AmountOfProgramCardsException;
import Exceptions.DestructionException;
import Exceptions.IllegalAmountOfDamageTokensException;
import Exceptions.IllegalParameterException;
import Exceptions.IllegalProgramSizeException;

class GameMasterTest {

	private static ArrayList<Player> playerlist = new ArrayList();
	private static ArrayList<Player> destroyedPlayer = new ArrayList();
	private static ArrayList<ProgramCard> deck = new ArrayList();
	private static RuleBook rb = new RuleBook();
	private static ArrayList<ProgramCard> maybe = new ArrayList();
	private static ArrayList<ProgramCard> resetDeck = new ArrayList();
	private static GameBoard gb;
	private static GameMaster gm;
	private static int[] position = new int[2];
	private LinkedList<Integer> SpawnPoints = new LinkedList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
	private static ArrayList <ProgramCard> player1Cards = new ArrayList();
	private static ArrayList <ProgramCard> player2Cards = new ArrayList();
	Player player1;
	Player player2;
	Player player3;
	Player player4;
	
	private static ProgramCard one = new ProgramCard(20, CardType.MOVE_TWO);
	private static ProgramCard two = new ProgramCard(30, CardType.ROTATE_RIGHT);
	private static ProgramCard three = new ProgramCard(40, CardType.MOVE_ONE);
	private static ProgramCard four = new ProgramCard(50, CardType.ROTATE_LEFT);
	private static ProgramCard five = new ProgramCard(60, CardType.MOVE_THREE);
	
	private static ProgramCard six = new ProgramCard(70, CardType.MOVE_THREE);
	private static ProgramCard seven = new ProgramCard(80, CardType.ROTATE_LEFT);
	private static ProgramCard eight = new ProgramCard(90, CardType.MOVE_TWO);
	private static ProgramCard nine = new ProgramCard(100, CardType.U_TURN);
	private static ProgramCard ten = new ProgramCard(110, CardType.ROTATE_LEFT);
	
	@BeforeAll

	static void init() {
		gm = new GameMaster();

		gb = new GameBoard();

		Player player1 = new Player(1, 3, 1, 1, "Rick", Direction.NORTH, "dockingBay");
		Player player2 = new Player(2, 3, 10, 1, "Morty", Direction.NORTH, "dockingBay");
//		Player player3 = new Player(3, 3, 6, 5, "Summer", Direction.NORTH, "DockingBay");
//		Player player4 = new Player(4, 3, 6, 6, "Jerry", Direction.NORTH, "DockingBay");

		playerlist.add(player1);
		playerlist.add(player2);
//		playerlist.add(player3);
//		playerlist.add(player4);

		gm.createDeck(deck);
		gm.createDeck(resetDeck);
		
		
		
	}
	
	
	@BeforeEach
	public void beforeEach() {
		
		
		// Reset general parameters of the players
		for (Player player : playerlist) {
			player.setDamageTokens(0);
			player.setFaceDirection(Direction.NORTH);
			player.setLastTouchedFlag(0);
			player.setLifeTokens(3);
			player.setPowerDownToken(false);
			player.setSecondPowerdownToken(false);
			player.setLastTouchedFlag(0);
			player.setActiveMap("dockingBay");
			// reset the position needed for comparing
			position[0] = 0;
			position[1] = 0;
			// reset players programs
			try {
				player.clearRegisters();
			} catch (NullPointerException | IllegalProgramSizeException e) {
				e.printStackTrace();
			}

		}
		// reset Position for every Player
		playerlist.get(0).setPos(1, 1);
		playerlist.get(1).setPos(10, 1);
//		playerlist.get(2).setPos(5, 6);
//		playerlist.get(3).setPos(6, 6);

		// reset Deck
		maybe.clear();
		deck.clear();
		for (int i = 0; i < resetDeck.size(); i++) {
			deck.add(resetDeck.get(i));
		}
		
		gm.shuffleDeck(deck);

		
		
		player1Cards.add(one);
		player1Cards.add(two);
		player1Cards.add(three);
		player1Cards.add(four);
		player1Cards.add(five);
		
		player2Cards.add(six);
		player2Cards.add(seven);
		player2Cards.add(eight);
		player2Cards.add(nine);
		player2Cards.add(ten);
		
		
		
		
	}
	
	@Test
	void createDeckTest() {
		boolean sameCard = false;
		
		for (int i = 0; i < deck.size(); i++) {
			for (int e = 0; e < deck.size(); e++) {
					if (!(i == e) && deck.get(i).getPriorityNumber() == deck.get(e).getPriorityNumber()) {
						sameCard = true;
					}
			}
		}
		
		
		assertEquals(false, sameCard);
		assertEquals(84, deck.size());
	}

	
	@Test
	void dealTokensTest() {

		gm.dealTokens(playerlist.get(0), 1, 5, false, false);
		assertEquals(2, (playerlist.get(0).getLifeTokens()));
		assertEquals(5, (playerlist.get(0).getDamageTokens()));
		assertEquals(false, playerlist.get(0).isPowerDownToken());
		assertEquals(false, playerlist.get(0).isSecondPowerdownToken());
	}
	
	
	@Test
	void dealProgramCardsTest() {

		gm.dealTokens(playerlist.get(0), 1, 5, false, false);
		gm.dealTokens(playerlist.get(1), 1, 3, false, false);
//		gm.dealTokens(playerlist.get(2), 1, 7, false, false);
//		gm.dealTokens(playerlist.get(3), 1, 9, false, true);
		gm.dealProgramCards(playerlist, deck);

		assertEquals(4, playerlist.get(0).getProgramCards().size());
		assertEquals(6, playerlist.get(1).getProgramCards().size());
//		assertEquals(2, playerlist.get(2).getProgramCards().size());
//		assertEquals(0, playerlist.get(3).getProgramCards().size());
//		assertEquals(true, playerlist.get(3).isSecondPowerdownToken());
//		assertEquals(72, deck.size());

	}
	
	@Test
	void wipeRegisterTest() {

		gm.wipeRegister(playerlist);
		assertEquals(null, playerlist.get(0).getProgram().get(0));
		assertEquals(null, playerlist.get(0).getProgram().get(1));
		assertEquals(null, playerlist.get(1).getProgram().get(0));
		assertEquals(null, playerlist.get(1).getProgram().get(1));
//		assertEquals(null, playerlist.get(2).getProgram().get(0));
//		assertEquals(null, playerlist.get(2).getProgram().get(1));
//		assertEquals(null, playerlist.get(3).getProgram().get(0));
//		assertEquals(null, playerlist.get(3).getProgram().get(1));
		
	}
	
	@Test
	void setupNextTurnTest() {

		playerlist.get(0).setPowerDownToken(false);
		playerlist.get(0).setSecondPowerdownToken(true);
		playerlist.get(0).setDamageTokens(6);
//		gm.dealTokens(playerlist.get(0), 3, 6, false, true);
		assertEquals(true, playerlist.get(0).isSecondPowerdownToken());
		assertEquals(false, playerlist.get(0).isPowerDownToken());
		gm.setupForNextTurn(playerlist);
		assertEquals(0, playerlist.get(0).getDamageTokens());
	}
	
	//Test repair one
	@Test
	void touchCheckpointTest1() {

		position[0] = 2;
		position[1] = 3;
		playerlist.get(1).setPos(2, 3);
		playerlist.get(1).setActiveMap("dizzyDash");
		gm.touchCheckpoints(playerlist, gb);
		assertArrayEquals(position, playerlist.get(1).getArchiveTokenPos());
	}
	//Test repair two -archive token update
	@Test
	void touchCheckpointTest2() {

		position[0] = 8;
		position[1] = 3;
		playerlist.get(1).setPos(8, 3);
		playerlist.get(1).setActiveMap("dizzyDash");
		
		gm.touchCheckpoints(playerlist, gb);
		assertArrayEquals(position, playerlist.get(1).getArchiveTokenPos());
	
	
	}
	//Test flag repair - archive token update
	@Test
	void touchCheckpointTest3() {

		position[0] = 5;
		position[1] = 4;
		playerlist.get(0).setPos(5, 4);
		playerlist.get(0).setActiveMap("dizzyDash");
		
		gm.touchCheckpoints(playerlist, gb);
		assertArrayEquals(position, playerlist.get(0).getArchiveTokenPos());
	
	
	}
	
	
	@Test
	void fireLasersTest1() {
		playerlist.get(0).setPos(0, 8);
		playerlist.get(1).setPos(0, 10);
		
		playerlist.get(0).setFaceDirection(Direction.SOUTH);
		playerlist.get(1).setFaceDirection(Direction.NORTH);
		
		gm.fireLaser(playerlist, playerlist, gb);
		
		assertEquals(1, playerlist.get(0).getDamageTokens());
		assertEquals(1, playerlist.get(1).getDamageTokens());


	}
	
	@Test
	void fireLasersTest2() {
		playerlist.get(0).setPos(0, 8);
		playerlist.get(1).setPos(0, 10);
		
		playerlist.get(0).setFaceDirection(Direction.EAST);
		playerlist.get(1).setFaceDirection(Direction.NORTH);
		
		gm.fireLaser(playerlist, playerlist, gb);
		
		assertEquals(1, playerlist.get(0).getDamageTokens());
		assertEquals(0, playerlist.get(1).getDamageTokens());


	}
	
	
	@Test
	void fireLasersTest3() {
		playerlist.get(0).setPos(0, 8);
		playerlist.get(1).setPos(3, 8);
	
		
		playerlist.get(0).setFaceDirection(Direction.EAST);
		playerlist.get(1).setFaceDirection(Direction.NORTH);
		
		gm.fireLaser(playerlist, playerlist, gb);
		
		assertEquals(0, playerlist.get(0).getDamageTokens());
		assertEquals(1, playerlist.get(1).getDamageTokens());

	}
	
	@Test
	void movePlayerTest1() {
		System.out.println("movePlayerTest1");
		//give player pc 
		playerlist.get(0).setProgramCards(player1Cards);
		playerlist.get(1).setProgramCards(player2Cards);
		
		//add pc to player program
		for (Player player : playerlist) {
			for (int i = 0; i < 5; i++) {
				player.playCard(player.getProgramCards().get(0), i);
			}
		}
		
	
		//make move
		gm.movePlayer(playerlist, 0, gb);
		
		//init position array to compare for player1
		position[0] = 1;
		position[1] = 11;
		
		assertArrayEquals(position, playerlist.get(0).getPos());
		
		//init position array to compare for player2
		position[0] = 10;
		position[1] = 10;
		
		assertArrayEquals(position, playerlist.get(1).getPos());
		
		//assert active map
		assertEquals("dizzyDash", playerlist.get(0).getActiveMap());
		assertEquals("dizzyDash", playerlist.get(1).getActiveMap());
		
		

	}
	
	
	@Test
	void movedByBoardTest1() {
		System.out.println("movedByBoardTest1");
		playerlist.get(0).setPos(1, 11);
		playerlist.get(1).setPos(10, 10);
		gm.movedByBoard(playerlist, gb);
		
		position[0] = 1;
		position[1] = 11;
		assertArrayEquals(position, playerlist.get(0).getPos());
		
		position[0] = 9;
		position[1] = 10;
		assertArrayEquals(position, playerlist.get(1).getPos());
		
		assertEquals(Direction.NORTH, playerlist.get(0).getFaceDirection());
		assertEquals(Direction.EAST, playerlist.get(1).getFaceDirection());
		
	}
	
	@Test
	void movePlayerTest2() {
		System.out.println("movePlayerTest2");
		
		playerlist.get(0).setActiveMap("dizzyDash");
		playerlist.get(1).setActiveMap("dizzyDash");
		
		
		playerlist.get(0).setPos(1, 11);
		playerlist.get(1).setPos(9, 10);
		
		
		//give player pc 
		playerlist.get(0).setProgramCards(player1Cards);
		playerlist.get(1).setProgramCards(player2Cards);
		
		//add pc to player program
		for (Player player : playerlist) {
			for (int i = 0; i < 5; i++) {
				player.playCard(player.getProgramCards().get(0), i);
			}
		}
		
	
		
		//make move
		gm.movePlayer(playerlist, 1, gb);
		
		//init position array to compare for player1
		position[0] = 1;
		position[1] = 11;
		
		assertArrayEquals(position, playerlist.get(0).getPos());
		assertEquals(Direction.EAST, playerlist.get(0).getFaceDirection());
		
		//init position array to compare for player2
		position[0] = 9;
		position[1] =10;
		
		assertArrayEquals(position, playerlist.get(1).getPos());
		
		
		//assert active map
		assertEquals("dizzyDash", playerlist.get(0).getActiveMap());
		assertEquals("dizzyDash", playerlist.get(1).getActiveMap());
		
		

	}
	
	
	@Test
	void movePlayerTest3() {
		System.out.println("movePlayerTest3");
		
		playerlist.get(0).setActiveMap("dizzyDash");
		playerlist.get(1).setActiveMap("dizzyDash");
		
		
		playerlist.get(0).setPos(1, 11);
		playerlist.get(1).setPos(9, 10);
		
		playerlist.get(0).setFaceDirection(Direction.EAST);
		playerlist.get(1).setFaceDirection(Direction.EAST);
		
		//give player pc 
		playerlist.get(0).setProgramCards(player1Cards);
		playerlist.get(1).setProgramCards(player2Cards);
		
		//add pc to player program
		for (Player player : playerlist) {
			for (int i = 0; i < 5; i++) {
				player.playCard(player.getProgramCards().get(0), i);
			}
		}
		
		//make move
		gm.movePlayer(playerlist, 2, gb);
		
		//init position array to compare for player1
		position[0] = 2;
		position[1] = 11;
		
		assertArrayEquals(position, playerlist.get(0).getPos());
		
		//init position array to compare for player2
		position[0] = 11;
		position[1] =10;
		
		assertArrayEquals(position, playerlist.get(1).getPos());
		
		//assert active map
		assertEquals("dizzyDash", playerlist.get(0).getActiveMap());
		assertEquals("dizzyDash", playerlist.get(1).getActiveMap());
		
		

	}
	
	
	
	
	
	@Test
	void movePlayerTest4() {
		System.out.println("movePlayerTest3");
		
		playerlist.get(0).setActiveMap("dizzyDash");
		playerlist.get(1).setActiveMap("dizzyDash");
		
		
		playerlist.get(0).setPos(2, 11);
		playerlist.get(1).setPos(11, 10);
		
		playerlist.get(0).setFaceDirection(Direction.EAST);
		playerlist.get(1).setFaceDirection(Direction.EAST);
		
		//give player pc 
		playerlist.get(0).setProgramCards(player1Cards);
		playerlist.get(1).setProgramCards(player2Cards);
		
		//add pc to player program
		for (Player player : playerlist) {
			for (int i = 0; i < 5; i++) {
				player.playCard(player.getProgramCards().get(0), i);
			}
		}
		
		//make move
		gm.movePlayer(playerlist, 3, gb);
		
		//init position array to compare for player1
		position[0] = 2;
		position[1] = 11;
		
		assertArrayEquals(position, playerlist.get(0).getPos());
		
		//init position array to compare for player2
		position[0] = 11;
		position[1] =10;
		
		assertArrayEquals(position, playerlist.get(1).getPos());
		
		//assert active map
		assertEquals("dizzyDash", playerlist.get(0).getActiveMap());
		assertEquals("dizzyDash", playerlist.get(1).getActiveMap());
		
		assertEquals (Direction.NORTH, playerlist.get(0).getFaceDirection());
		assertEquals (Direction.WEST, playerlist.get(1).getFaceDirection());

	}
	
	
	
	@Test
	void movePlayerTest5() {
		System.out.println("movePlayerTest3");
		
		playerlist.get(0).setActiveMap("dizzyDash");
		playerlist.get(1).setActiveMap("dizzyDash");
		
		
		playerlist.get(0).setPos(2, 11);
		playerlist.get(1).setPos(11, 10);
		
		playerlist.get(0).setFaceDirection(Direction.NORTH);
		playerlist.get(1).setFaceDirection(Direction.WEST);
		
		//give player pc 
		playerlist.get(0).setProgramCards(player1Cards);
		playerlist.get(1).setProgramCards(player2Cards);
		
		//add pc to player program
		for (Player player : playerlist) {
			for (int i = 0; i < 5; i++) {
				player.playCard(player.getProgramCards().get(0), i);
			}
		}
		
		//make move
		gm.movePlayer(playerlist, 4, gb);
		
		//init position array to compare for player1
		position[0] = 2;
		position[1] = 8;
		
		assertArrayEquals(position, playerlist.get(0).getPos());
		
		//init position array to compare for player2
		position[0] = 11;
		position[1] =10;
		
		assertArrayEquals(position, playerlist.get(1).getPos());
		
		//assert active map
		assertEquals("dizzyDash", playerlist.get(0).getActiveMap());
		assertEquals("dizzyDash", playerlist.get(1).getActiveMap());
		
		assertEquals (Direction.NORTH, playerlist.get(0).getFaceDirection());
		assertEquals (Direction.SOUTH, playerlist.get(1).getFaceDirection());

	}
	
//	@Test
//	 void startPhase() {
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println("Start Phase TEST");
//		playerlist.get(0).setProgramCards(player1Cards);
//		playerlist.get(1).setProgramCards(player2Cards);
//		
//		
//		//add pc to player program
//		for (Player player : playerlist) {
//			for (int i = 0; i < 5; i++) {
//				player.playCard(player.getProgramCards().get(0), i);
//			}
//		}
//		
//		for (Player player : playerlist) {
//			System.out.println(player.getName());
//			for (int i = 0; i < 5; i++) {
//				System.out.println(player.getProgram().get(i).getInfo());
//			}
//		}
//		
//		
//		System.out.println("player1 pos before startPhase " + playerlist.get(0).getPos()[0]+ " " + playerlist.get(0).getPos()[1]);
//		System.out.println("player2 pos before startPhase " + playerlist.get(1).getPos()[0]+ " " + playerlist.get(1).getPos()[1]);
//		
//		gm.setPlayerList(playerlist);
//		System.out.println("playerlist size:" + playerlist.size());
//		gm.startPhase();
//		
//		System.out.println("player1 pos after startPhase " + playerlist.get(0).getPos()[0]+ " " + playerlist.get(0).getPos()[1]);
//		System.out.println("player2 pos after startPhase " + playerlist.get(1).getPos()[0]+ " " + playerlist.get(1).getPos()[1]);
//
//		
//		
//		position[0] = 2;
//		position[1] = 8;
//		
//		assertArrayEquals(position, playerlist.get(0).getPos());
//		
//		//init position array to compare for player2
//		position[0] = 11;
//		position[1] =10;
//		
//		assertArrayEquals(position, playerlist.get(1).getPos());
//		
//		//assert active map
//		assertEquals("dizzyDash", playerlist.get(0).getActiveMap());
//		assertEquals("dizzyDash", playerlist.get(1).getActiveMap());
//		
//		assertEquals (Direction.NORTH, playerlist.get(0).getFaceDirection());
//		assertEquals (Direction.SOUTH, playerlist.get(1).getFaceDirection());
//		
//	}
	
}
