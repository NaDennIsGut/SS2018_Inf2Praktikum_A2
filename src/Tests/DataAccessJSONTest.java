package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.GameMaster;
import Streams.DataAccessJSON;

/**
 * 
 * @author Leila Taraman
 *
 */
class DataAccessJSONTest {

	private static DataAccessJSON json;
	private static GameMaster gamemaster;
	
	/**
	 * prepare the game
	 */
	@BeforeAll
	public static void start() {
		
		gamemaster = new GameMaster();
		gamemaster.addPlayer("Dennis");
		gamemaster.addPlayer("Arthur");
		gamemaster.addPlayer("Filip");
		gamemaster.addPlayer("Leila");
		gamemaster.startGame();
	}
	
	/**
	 * tests if something was saved
	 */
	@Test
	public void testSave() {
		gamemaster.save("test.json");
//		try {
//			
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		File file = new File("test.json");
		assertTrue(file.exists());
	}
	
	/**
	 * tests if something was loaded and if it was correctly
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Test
	public void testLoad() throws FileNotFoundException, IOException {
//		json.save(gamemaster, "test.json");
//		GameMaster gm = (GameMaster) json.load("test.json");
//		assertTrue(gamemaster.equals(gm));
		gamemaster.save("test.json");
		GameMaster gm = new GameMaster();
		gm.load("test.json");
		gamemaster.equals(gm);
		assertTrue(gamemaster.equals(gm));
	}


}
