package Tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.Player;
import Backend.ProgramCard;
import Enums.CardType;
import Enums.Direction;


class PlayerTest {
	private static Player p;
	private static ArrayList<ProgramCard> c9;
	private static ArrayList<ProgramCard> c10;
	private static ProgramCard testCard;
	private static ArrayList<ArrayList<String>> getInfoExpect;
	private static ArrayList<String> tempStr;
	
	@BeforeAll
	static void init() {
		c9 = new ArrayList<ProgramCard>();
		c10 = new ArrayList<ProgramCard>();
		
		try {
			testCard = new ProgramCard(900, CardType.MOVE_ONE);
			

			
			
			p = new Player(1, 3, 4, 5, "He-Man", Direction.NORTH, "DockingBay");
			c9.add(new ProgramCard(90, CardType.BACK_UP));
			c9.add(new ProgramCard(100, CardType.MOVE_ONE));
			c9.add(new ProgramCard(110, CardType.MOVE_TWO));
			c9.add(new ProgramCard(120, CardType.ROTATE_LEFT));
			c9.add(new ProgramCard(130, CardType.ROTATE_RIGHT));
			c9.add(new ProgramCard(140, CardType.U_TURN));
			c9.add(new ProgramCard(150, CardType.BACK_UP));
			c9.add(new ProgramCard(160, CardType.MOVE_ONE));
			c9.add(new ProgramCard(170, CardType.MOVE_TWO));
			
			c10.add(new ProgramCard(90, CardType.BACK_UP));
			c10.add(new ProgramCard(100, CardType.MOVE_ONE));
			c10.add(new ProgramCard(110, CardType.MOVE_TWO));
			c10.add(new ProgramCard(120, CardType.ROTATE_LEFT));
			c10.add(new ProgramCard(130, CardType.ROTATE_RIGHT));
			c10.add(new ProgramCard(140, CardType.U_TURN));
			c10.add(new ProgramCard(150, CardType.BACK_UP));
			c10.add(new ProgramCard(160, CardType.MOVE_ONE));
			c10.add(new ProgramCard(170, CardType.MOVE_TWO));
			c10.add(new ProgramCard(170, CardType.MOVE_TWO));
			
		} catch(Exception e){
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		getInfoExpect = new ArrayList<ArrayList<String>>();
		tempStr = new ArrayList<String>();
		
		p.setProgramCards(c9);
		p.playCard(c9.get(0), 0);
		p.playCard(c9.get(1), 1);
		p.playCard(c9.get(2), 2);
		p.playCard(c9.get(3), 3);
		p.playCard(c9.get(4), 4);
		
		//name
		tempStr.add("He-Man");
		getInfoExpect.add(tempStr);
		
		//spawn number
		tempStr = new ArrayList<String>();
		tempStr.add("1");
		getInfoExpect.add(tempStr);
		
		//pos
		tempStr = new ArrayList<String>();
		tempStr.add("4 , 5");
		getInfoExpect.add(tempStr);
		
		//face dir
		tempStr = new ArrayList<String>();
		tempStr.add("NORTH");
		getInfoExpect.add(tempStr);
		
		//prog cards
		tempStr = new ArrayList<String>();
		tempStr.add("BACK_UP , 90");
		tempStr.add("BACK_UP , 90");
		tempStr.add("MOVE_ONE , 100");
		tempStr.add("MOVE_TWO , 110");
		tempStr.add("ROTATE_LEFT , 120");
		tempStr.add("ROTATE_RIGHT , 130");
		tempStr.add("U_TURN , 140");
		tempStr.add("MOVE_ONE , 150");
		tempStr.add("MOVE_TWO , 160");
		getInfoExpect.add(tempStr);
		
		//arch tok pos
		tempStr = new ArrayList<String>();
		tempStr.add("4 , 5");
		getInfoExpect.add(tempStr);

		//last flag
		tempStr = new ArrayList<String>();
		tempStr.add("0");
		getInfoExpect.add(tempStr);

		
		//life tok
		tempStr = new ArrayList<String>();
		tempStr.add("3");
		getInfoExpect.add(tempStr);
		
		//dmg tok
		tempStr = new ArrayList<String>();
		tempStr.add("0");
		getInfoExpect.add(tempStr);
				
		//pwr dwn tok
		tempStr = new ArrayList<String>();
		tempStr.add("false");
		getInfoExpect.add(tempStr);
		
		//prgrm
		tempStr = new ArrayList<String>();
		tempStr.add("BACK_UP , 90");
		tempStr.add("BACK_UP , 90");
		tempStr.add("MOVE_ONE , 100");
		tempStr.add("MOVE_TWO , 110");
		tempStr.add("ROTATE_LEFT , 120");
		getInfoExpect.add(tempStr);
		
	}
	
	@Test
	void testConstructor() {
		assertThrows(NullPointerException.class,() -> new Player(1, 1, 1, 1, null, Direction.NORTH, "DockingBay"));
		assertThrows(IllegalArgumentException.class,() -> new Player(1, 1, 1, 1, "", Direction.NORTH, "DockingBay"));
		assertThrows(NullPointerException.class,() -> new Player(1, 1, 1, 1, "Skeletor", null, "DockingBay"));
		assertThrows(IllegalArgumentException.class,() -> new Player(1, 1, 1, 1, "Orko", Direction.NONE, "DockingBay"));
	}
	
	@Test
	void testGetters() {
		assertEquals(0, p.getDamageTokens());
		assertEquals(Direction.NORTH, p.getFaceDirection());
		assertEquals(3, p.getLifeTokens());
		assertEquals("He-Man", p.getName());
		assertTrue(p.getName().equals("He-Man"));
		assertArrayEquals(new int[] {4, 5}, p.getPos());
		assertEquals(1,  p.getSpawnNumber());
		assertFalse(p.isPowerDownToken());
		assertArrayEquals(new int[] {4, 5}, p.getArchiveTokenPos());

		assertNotNull(p.getProgram());
		assertNotNull(p.getProgramCards());
	}
	
	@Test
	void testSetters(){
		assertThrows(NullPointerException.class, () -> p.setArchiveTokenPos(null));
		
		assertThrows(NullPointerException.class, () -> p.setFaceDirection(null));
		assertThrows(IllegalArgumentException.class, () -> p.setFaceDirection(Direction.NONE));
		
		assertThrows(NullPointerException.class, () -> p.setPos(null));
		
		assertThrows(NullPointerException.class, () -> p.setProgramCards(null));
	}
	

	@Test
	void testServices() {
		assertEquals(true, p.announcePowerDown());
		assertEquals(true, p.isPowerDownToken());
		assertEquals(false, p.announcePowerDown());
		
		assertThrows(NullPointerException.class,() -> p.playCard(null, 1));
		assertThrows(IndexOutOfBoundsException.class, () -> p.playCard(testCard, -1));
		assertThrows(IndexOutOfBoundsException.class, () -> p.playCard(testCard, 6));
		
		assertThrows(IllegalArgumentException.class, () -> p.playCard(testCard, 3));
		
		assertEquals(getInfoExpect.get(0).get(0), "He-Man");
		assertEquals(getInfoExpect.get(1).get(0), "1");
		assertEquals(getInfoExpect.get(2).get(0), "4 , 5");
		assertEquals(getInfoExpect.get(3).get(0), "NORTH");
		
		assertEquals(getInfoExpect.get(4).get(0), "BACK_UP , 90");
		assertEquals(getInfoExpect.get(4).get(1), "BACK_UP , 90");
		assertEquals(getInfoExpect.get(4).get(2), "MOVE_ONE , 100");
		assertEquals(getInfoExpect.get(4).get(3), "MOVE_TWO , 110");
		assertEquals(getInfoExpect.get(4).get(4), "ROTATE_LEFT , 120");
		assertEquals(getInfoExpect.get(4).get(5), "ROTATE_RIGHT , 130");
		assertEquals(getInfoExpect.get(4).get(6), "U_TURN , 140");
		assertEquals(getInfoExpect.get(4).get(7), "MOVE_ONE , 150");
		assertEquals(getInfoExpect.get(4).get(8), "MOVE_TWO , 160");
		
		assertEquals(getInfoExpect.get(5).get(0), "4 , 5");
		assertEquals(getInfoExpect.get(6).get(0), "0");
		assertEquals(getInfoExpect.get(7).get(0), "3");
		assertEquals(getInfoExpect.get(8).get(0), "0");
		assertEquals(getInfoExpect.get(9).get(0), "false");
		
		
		assertEquals(getInfoExpect.get(10).get(0), "BACK_UP , 90");
		assertEquals(getInfoExpect.get(10).get(1), "BACK_UP , 90");
		assertEquals(getInfoExpect.get(10).get(2), "MOVE_ONE , 100");
		assertEquals(getInfoExpect.get(10).get(3), "MOVE_TWO , 110");
		assertEquals(getInfoExpect.get(10).get(4), "ROTATE_LEFT , 120");
		
	}

}
