package Tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import Backend.GameBoard;
import Enums.Direction;
import Enums.FieldType;
import Exceptions.IllegalParameterException;
/**
 * 
 * @author Leila Taraman
 *
 */

class GameBoardTest {

	
	private static GameBoard gameboard;
	private static GameBoard dockingbay;
	
	/**
	 * Create New gameboard and dockingbay
	 * @throws IllegalParameterException
	 */
	@BeforeAll
	public static void init() throws IllegalParameterException {
		gameboard = new GameBoard();
		dockingbay = new GameBoard();
		
	}
	
	
	/**
	 * test several coordinates of the correct fieldType
	 */
	@Test
	public void testFieldTypeAt()  {
		assertEquals(gameboard.fieldTypeAt(2, 2, false), FieldType.GEAR_ROT_RIGHT);
		assertEquals(gameboard.fieldTypeAt(1, 1, false), FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST);
		assertEquals(gameboard.fieldTypeAt(1, 6, false), FieldType.FLAG);
		assertEquals(gameboard.fieldTypeAt(2, 0, false), FieldType.EMPTY);
		assertEquals(gameboard.fieldTypeAt(5, 4, false), FieldType.FLAG);
		assertEquals(gameboard.fieldTypeAt(1, 2, false), FieldType.CONVEYOR_BELT_FAST);
		assertEquals(gameboard.fieldTypeAt(2, 3, false), FieldType.REPAIR_ONE);
		assertEquals(gameboard.fieldTypeAt(3, 8, false), FieldType.REPAIR_TWO);
		assertEquals(gameboard.fieldTypeAt(2, 6, false), FieldType.GEAR_ROT_LEFT);
		assertEquals(gameboard.fieldTypeAt(10, 11, false), FieldType.FLAG);
		assertEquals(gameboard.fieldTypeAt(11, 6, false), FieldType.EMPTY);
		assertEquals(gameboard.fieldTypeAt(8, 3, false), FieldType.REPAIR_TWO);
		assertEquals(gameboard.fieldTypeAt(4, 1, false), FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST);
		assertEquals(gameboard.fieldTypeAt(0, 0, false), FieldType.EMPTY);
		assertEquals(gameboard.fieldTypeAt(6, 4, false), FieldType.GEAR_ROT_LEFT);
		
		assertThrows(IndexOutOfBoundsException.class,() -> gameboard.fieldTypeAt(-1, -1, false));
		
	}
	
	/**
	 * test several coordinates of the correct fieldType of the Dockingbay
	 */
	@Test
	public void testFieldTypeAtDockingbay()  {
		assertEquals(gameboard.fieldTypeAt(0, 0, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(1, 0, true), FieldType.EMPTY);
		assertEquals(gameboard.fieldTypeAt(2, 0, true), FieldType.EMPTY);
		assertEquals(gameboard.fieldTypeAt(0, 2, true), FieldType.CONVEYOR_BELT);
		assertEquals(gameboard.fieldTypeAt(2, 2, true), FieldType.CONVEYOR_BELT_ROT_RIGHT);
		assertEquals(gameboard.fieldTypeAt(9, 2, true), FieldType.CONVEYOR_BELT_ROT_LEFT);
		assertEquals(gameboard.fieldTypeAt(1, 1, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(11, 0, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(10, 1, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(3, 2, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(8, 2, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(5, 3, true), FieldType.SPAWN);
		assertEquals(gameboard.fieldTypeAt(6, 3, true), FieldType.SPAWN);
		
		assertThrows(IndexOutOfBoundsException.class,() -> dockingbay.fieldTypeAt(-1, -1, true));
		
	}
	
	/**
	 * test several coordinates of the correct fieldDirection
	 */
	@Test
	public void testFieldDirectionAt() {
		assertEquals(gameboard.fieldDirectionAt(2, 4, false), Direction.WEST);
		assertEquals(gameboard.fieldDirectionAt(2, 3, false), Direction.NONE);
		assertEquals(gameboard.fieldDirectionAt(2, 1, false), Direction.EAST);
		assertEquals(gameboard.fieldDirectionAt(4, 3, false), Direction.SOUTH);
		assertEquals(gameboard.fieldDirectionAt(1, 2, false), Direction.NORTH);
		assertEquals(gameboard.fieldDirectionAt(5, 4, false), Direction.NONE);
		assertEquals(gameboard.fieldDirectionAt(8, 1, false), Direction.EAST);
		assertEquals(gameboard.fieldDirectionAt(9, 4, false), Direction.WEST);
		assertEquals(gameboard.fieldDirectionAt(10, 2, false), Direction.SOUTH);
		assertEquals(gameboard.fieldDirectionAt(7, 3, false), Direction.NORTH);
		assertEquals(gameboard.fieldDirectionAt(10, 8, false), Direction.SOUTH);
		assertEquals(gameboard.fieldDirectionAt(1, 8, false), Direction.NORTH);
		assertEquals(gameboard.fieldDirectionAt(9, 7, false), Direction.EAST);
		assertEquals(gameboard.fieldDirectionAt(2, 10, false), Direction.WEST);
		assertEquals(gameboard.fieldDirectionAt(0, 1, false), Direction.NONE);
		
		assertThrows(IndexOutOfBoundsException.class,() -> gameboard.fieldDirectionAt(-1, -1, false));
		
	}
	
	/**
	 * test several coordinates of the correct fieldDirection of the Dockingbay
	 */
	@Test
	public void testFieldDirectionAtDockingbay() {
		assertEquals(dockingbay.fieldDirectionAt(2, 0, true), Direction.NORTH);
		assertEquals(dockingbay.fieldDirectionAt(1, 0, true), Direction.NONE);
		assertEquals(dockingbay.fieldDirectionAt(4, 0, true), Direction.NORTH);
		assertEquals(dockingbay.fieldDirectionAt(7, 0, true), Direction.NORTH);
		assertEquals(dockingbay.fieldDirectionAt(1, 0, true), Direction.NONE);
		assertEquals(dockingbay.fieldDirectionAt(2, 1, true), Direction.WEST);
		assertEquals(dockingbay.fieldDirectionAt(2, 2, true), Direction.SOUTH);
		assertEquals(dockingbay.fieldDirectionAt(10, 2, true), Direction.WEST);
		assertEquals(dockingbay.fieldDirectionAt(0, 3, true), Direction.NONE);
		
		assertThrows(IndexOutOfBoundsException.class,() -> dockingbay.fieldDirectionAt(-1, -1, true));

	}
	
	/**
	 * test several coordinates of the correct fieldWall
	 */
	@Test
	public void testFieldWallsAt() {
		assertArrayEquals(gameboard.fieldWallsAt(0, 4, false), new boolean[] {false, false, false, true});
		assertArrayEquals(gameboard.fieldWallsAt(7, 0, false), new boolean[] {true, false, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(0, 2, false), new boolean[] {false, false, false, true});
		assertArrayEquals(gameboard.fieldWallsAt(0, 7, false), new boolean[] {false, false, false, true});
		assertArrayEquals(gameboard.fieldWallsAt(0, 9, false), new boolean[] {false, false, false, true});
		assertArrayEquals(gameboard.fieldWallsAt(2, 0, false), new boolean[] {true, false, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(4, 0, false), new boolean[] {true, false, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(9, 0, false), new boolean[] {true, false, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(11, 2, false), new boolean[] {false, true, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(7, 11, false), new boolean[] {false, false, true, false});
		assertArrayEquals(gameboard.fieldWallsAt(3, 2, false), new boolean[] {false, false, true, false});
		assertArrayEquals(gameboard.fieldWallsAt(5, 3, false), new boolean[] {false, false, false, true});
		assertArrayEquals(gameboard.fieldWallsAt(6, 3, false), new boolean[] {false, true, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(8, 5, false), new boolean[] {true, false, false, false});
		assertArrayEquals(gameboard.fieldWallsAt(8, 9, false), new boolean[] {true, false, false, false});
		
		assertThrows(IndexOutOfBoundsException.class,() -> gameboard.fieldWallsAt(-1, -1, false));
		
	}
	
	/**
	 * test several coordinates of the correct fieldWall of the Dockingbay
	 */
	@Test
	public void testFieldWallsAtDockingbay() {
		assertArrayEquals(new boolean[] {true, false, false, true}, dockingbay.fieldWallsAt(4, 0, true));
		assertArrayEquals(new boolean[] {true, true, false, false}, dockingbay.fieldWallsAt(7, 0, true));
		assertArrayEquals(new boolean[] {true, false, false, false}, dockingbay.fieldWallsAt(2, 0, true));
		assertArrayEquals(new boolean[] {true, false, false, false}, dockingbay.fieldWallsAt(9, 0, true));
		assertArrayEquals(new boolean[] {false, false, false, true}, dockingbay.fieldWallsAt(1, 1, true));
		assertArrayEquals(new boolean[] {false, false, false, true}, dockingbay.fieldWallsAt(2, 1, true));
		assertArrayEquals(new boolean[] {false, false, false, true}, dockingbay.fieldWallsAt(10, 1, true));
		assertArrayEquals(new boolean[] {false, false, false, true}, dockingbay.fieldWallsAt(11, 1, true));
		assertArrayEquals(new boolean[] {false, false, false, true}, dockingbay.fieldWallsAt(6, 2, true));
		assertArrayEquals(new boolean[] {false, false, false, true}, dockingbay.fieldWallsAt(6, 3, true));
		
		assertThrows(IndexOutOfBoundsException.class,() -> dockingbay.fieldWallsAt(-1, -1, true));
	}
	
	
	//FIELDLASERAT - NOCH FERTIG TESTEN
	@Test
	public void tesFieldLaserAt() {
		assertArrayEquals(new int[] {0, 0, 0, 0}, gameboard.fieldLasersAt(0, 0, false));
		assertArrayEquals(new int[] {0, 0, 0, 1}, gameboard.fieldLasersAt(5, 3, false));
		
		assertThrows(IndexOutOfBoundsException.class,() -> gameboard.fieldLasersAt(-1, -1, false));
		
	}
	
	/**
	 * test several coordinates of the correct fieldIndex
	 */
	@Test
	public void testFieldIndexAt() {
		assertEquals(3, gameboard.fieldIndexAt(1, 6, false));
		assertEquals(2, gameboard.fieldIndexAt(10, 11, false));
		assertEquals(1, gameboard.fieldIndexAt(5, 4, false));
		
		assertThrows(IndexOutOfBoundsException.class,() -> gameboard.fieldIndexAt(-1, -1, false));
		
	}
	
	/**
	 * test several coordinates of the correct fieldIndex of the dockingbay
	 */
	@Test
	public void testFieldIndexAtDockingbay() {
		assertEquals(7, dockingbay.fieldIndexAt(0, 0, true));
		assertEquals(5, dockingbay.fieldIndexAt(1, 1, true));
		assertEquals(3, dockingbay.fieldIndexAt(3, 2, true));
		assertEquals(1, dockingbay.fieldIndexAt(5, 3, true));
		assertEquals(2, dockingbay.fieldIndexAt(6, 3, true));
		assertEquals(4, dockingbay.fieldIndexAt(8, 2, true));
		assertEquals(6, dockingbay.fieldIndexAt(10, 1, true));
		assertEquals(8, dockingbay.fieldIndexAt(11, 0, true));
		
		assertThrows(IndexOutOfBoundsException.class,() -> dockingbay.fieldIndexAt(-1, -1, true));
	}
	
	/**
	 * test several coordinates of the correct spawnPosition
	 */
	@Test
	public void testGetSpawnPos() {
		
		assertArrayEquals(new int[] {0,0}, dockingbay.getSpawnPos(7));
		assertArrayEquals(new int[] {1,1}, dockingbay.getSpawnPos(5));
        assertArrayEquals(new int[] {3,2}, dockingbay.getSpawnPos(3));
		assertArrayEquals(new int[] {5,3}, dockingbay.getSpawnPos(1));
		assertArrayEquals(new int[] {6,3}, dockingbay.getSpawnPos(2));
		assertArrayEquals(new int[] {8,2}, dockingbay.getSpawnPos(4));
		assertArrayEquals(new int[] {10,1}, dockingbay.getSpawnPos(6));
		assertArrayEquals(new int[] {11,0}, dockingbay.getSpawnPos(8));
		
	}
	

}
