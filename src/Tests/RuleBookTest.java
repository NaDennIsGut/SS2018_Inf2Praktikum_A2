package Tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.plaf.synth.SynthSeparatorUI;

import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import Backend.GameBoard;
import Backend.GameMaster;
import Backend.Player;
import Backend.ProgramCard;
import Backend.RuleBook;
import Enums.CardType;
import Enums.Direction;
import Enums.FieldType;
import Exceptions.AmountOfPlayersException;
import Exceptions.AmountOfProgramCardsException;
import Exceptions.DestructionException;
import Exceptions.GameOverException;
import Exceptions.IllegalAmountOfDamageTokensException;
import Exceptions.IllegalParameterException;
import Exceptions.IllegalPositionException;
import Exceptions.IllegalProgramCardException;
import Exceptions.IllegalProgramSizeException;
import Exceptions.IllegalRegisterPhaseException;
import Exceptions.OutsideBoardException;
import Exceptions.RegisterLockedException;

class RuleBookTest {

	private static ArrayList<Player> playerlist = new ArrayList();
	private static ArrayList<ProgramCard> pc = new ArrayList();
	private static RuleBook rb = new RuleBook();
	private static ArrayList<ProgramCard> maybe = new ArrayList();
	private static ArrayList<ProgramCard> resetDeck = new ArrayList();
	private static GameBoard gb;
	private static GameMaster gm;
	private static int[] position = new int[2];

	@BeforeAll

	static void init() {
		gm = new GameMaster();

		gb = new GameBoard();

		Player player1 = new Player(1, 3, 5, 5, "Rick", Direction.NORTH, "DockingBay");
		Player player2 = new Player(2, 3, 5, 6, "Morty", Direction.NORTH, "DockingBay");
		Player player3 = new Player(3, 3, 6, 5, "Summer", Direction.NORTH, "DockingBay");
		Player player4 = new Player(4, 3, 6, 6, "Jerry", Direction.NORTH, "DockingBay");

		playerlist.add(player1);
		playerlist.add(player2);
		playerlist.add(player3);
		playerlist.add(player4);

		for (int i = 10; i <= 60; i += 10) {
			pc.add(new ProgramCard(i, CardType.U_TURN));
			resetDeck.add(new ProgramCard(i, CardType.U_TURN));

		}
		for (int i = 70; i <= 410; i += 20) {
			pc.add(new ProgramCard(i, CardType.ROTATE_LEFT));
			resetDeck.add(new ProgramCard(i, CardType.ROTATE_LEFT));

		}
		for (int i = 80; i <= 420; i += 20) {
			pc.add(new ProgramCard(i, CardType.ROTATE_RIGHT));
			resetDeck.add(new ProgramCard(i, CardType.ROTATE_RIGHT));

		}
		for (int i = 430; i <= 480; i += 10) {
			pc.add(new ProgramCard(i, CardType.BACK_UP));
			resetDeck.add(new ProgramCard(i, CardType.BACK_UP));

		}
		for (int i = 490; i <= 660; i += 10) {
			pc.add(new ProgramCard(i, CardType.MOVE_ONE));
			resetDeck.add(new ProgramCard(i, CardType.MOVE_ONE));

		}
		for (int i = 670; i <= 780; i += 10) {
			pc.add(new ProgramCard(i, CardType.MOVE_TWO));
			resetDeck.add(new ProgramCard(i, CardType.MOVE_TWO));

		}
		for (int i = 790; i <= 840; i += 10) {
			pc.add(new ProgramCard(i, CardType.MOVE_THREE));
			resetDeck.add(new ProgramCard(i, CardType.MOVE_THREE));

		}
		Collections.shuffle(pc);

	}

	@BeforeEach
	public void beforeEach() {
		// Reset general parameters of the players
		for (Player player : playerlist) {
			player.setDamageTokens(0);
			player.setFaceDirection(Direction.NORTH);
			player.setLastTouchedFlag(0);
			player.setLifeTokens(3);
			player.setPowerDownToken(false);
			player.setSecondPowerdownToken(false);
			// reset the position needed for comparing
			position[0] = 0;
			position[1] = 0;
			// reset players programs
			try {
				player.clearRegisters();
			} catch (NullPointerException | IllegalProgramSizeException e) {
				e.printStackTrace();
			}

		}
		// reset Position for every Player
		playerlist.get(0).setPos(5, 5);
		playerlist.get(1).setPos(6, 5);
		playerlist.get(2).setPos(5, 6);
		playerlist.get(3).setPos(6, 6);

		// reset Deck
		maybe.clear();
		pc.clear();
		for (int i = 0; i < resetDeck.size(); i++) {
			pc.add(resetDeck.get(i));
		}

	}

	// check card order if every player is allowed to play a card

	@Test
	void testCheckCardOrder1() {
		gm.dealProgramCards(playerlist, pc);

		for (int e = 0; e < 5; e++) {
			playerlist.get(0).playCard(playerlist.get(0).getProgramCards().get(e), e);
			playerlist.get(1).playCard(playerlist.get(1).getProgramCards().get(e), e);
			playerlist.get(2).playCard(playerlist.get(2).getProgramCards().get(e), e);
			playerlist.get(3).playCard(playerlist.get(3).getProgramCards().get(e), e);
		}

		// take the cards that possibly can be played
		for (int i = 0; i < playerlist.size(); i++) {
			maybe.add(playerlist.get(i).getProgram().get(2));
		}

		try {
			assertTrue(rb.checkCardOrder(2, playerlist, maybe));
		} catch (IllegalProgramCardException | AmountOfProgramCardsException | RegisterLockedException
				| AmountOfPlayersException | IllegalRegisterPhaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	// Test when Jerry is powered down
	void testCheckCardOrder2() {

		gm.dealProgramCards(playerlist, pc);
		playerlist.get(3).setPowerDownToken(true);
		for (int e = 0; e < 5; e++) {
			playerlist.get(0).playCard(playerlist.get(0).getProgramCards().get(e), e);
			playerlist.get(1).playCard(playerlist.get(1).getProgramCards().get(e), e);
			playerlist.get(2).playCard(playerlist.get(2).getProgramCards().get(e), e);
			playerlist.get(3).playCard(playerlist.get(3).getProgramCards().get(e), e);
		}

		// take the cards that possibly can be played
		for (int i = 0; i < playerlist.size(); i++) {
			maybe.add(playerlist.get(0).getProgram().get(2));
		}

		assertThrows(RegisterLockedException.class, () -> rb.checkCardOrder(2, playerlist, maybe));
	}

	// test although summer has 7 damage tokens

	@Test
	void testCheckCardOrder3() {

		gm.dealProgramCards(playerlist, pc);
		playerlist.get(2).setDamageTokens(7);
		for (int e = 0; e < 5; e++) {
			playerlist.get(0).playCard(playerlist.get(0).getProgramCards().get(e), e);
			playerlist.get(1).playCard(playerlist.get(1).getProgramCards().get(e), e);
			playerlist.get(2).playCard(playerlist.get(2).getProgramCards().get(e), e);
			playerlist.get(3).playCard(playerlist.get(3).getProgramCards().get(e), e);
		}

		// take the cards that possibly can be played
		for (int i = 0; i < playerlist.size(); i++) {
			maybe.add(playerlist.get(0).getProgram().get(2));
		}

		assertThrows(RegisterLockedException.class, () -> rb.checkCardOrder(2, playerlist, maybe));

	}

	// test if the wrong register phase is passed

	@Test
	void testCheckCardOrder4() {
		assertThrows(IllegalRegisterPhaseException.class, () -> rb.checkCardOrder(8, playerlist, maybe));
	}

	@Test
	void testCheckCardOrder5() {
		gm.dealProgramCards(playerlist, pc);
		playerlist.get(2).setDamageTokens(7);
		for (int e = 0; e < 5; e++) {
			playerlist.get(0).playCard(playerlist.get(0).getProgramCards().get(e), e);
			playerlist.get(1).playCard(playerlist.get(1).getProgramCards().get(e), e);
			playerlist.get(2).playCard(playerlist.get(2).getProgramCards().get(e), e);
			playerlist.get(3).playCard(playerlist.get(3).getProgramCards().get(e), e);
		}
		// take the cards that possibly can be played

		for (int i = 0; i < playerlist.size(); i++) {
			maybe.add(playerlist.get(i).getProgram().get(2));

		}

		assertThrows(IllegalProgramCardException.class, () -> rb.checkCardOrder(3, playerlist, maybe));
	}

	@Test
	void checkAmountProgramCardsTest() {
		gm.dealProgramCards(playerlist, pc);
		playerlist.get(2).setDamageTokens(7);
		playerlist.get(2).getProgramCards().remove(0);
		playerlist.get(2).getProgramCards().remove(0);

		try {
			assertTrue(rb.checkAmountOfProgramCards(playerlist.get(0)));
			// assertTrue(rb.checkAmountOfProgramCards(playerlist.get(2)));
			assertThrows(AmountOfProgramCardsException.class, () -> rb.checkAmountOfProgramCards(playerlist.get(2)));
		} catch (AmountOfProgramCardsException e) {
			e.printStackTrace();
		}

	}

	@Test
	void checkAmountProgramCardsTest2() {
		gm.dealProgramCards(playerlist, pc);
		playerlist.get(2).setDamageTokens(2);
		playerlist.get(2).getProgramCards().remove(0);
		playerlist.get(2).getProgramCards().remove(0);
		try {
			assertTrue(rb.checkAmountOfProgramCards(playerlist.get(2)));
		} catch (AmountOfProgramCardsException e) {
			e.printStackTrace();
		}

	}

	@Test
	void checkAmountProgramCardsTest3() {
		gm.dealProgramCards(playerlist, pc);
		playerlist.get(3).setDamageTokens(5);
		for (int i = 0; i < 5; i++) {
			playerlist.get(3).getProgramCards().remove(0);
		}

		try {
			assertTrue(rb.checkAmountOfProgramCards(playerlist.get(3)));
		} catch (AmountOfProgramCardsException e) {
			e.printStackTrace();
		}

	}

	@Test
	void allowedPlayersTest() {
		assertEquals(2, rb.lookUpAllowedPlayers()[0]);
		assertEquals(8, rb.lookUpAllowedPlayers()[1]);
	}



	@Test
	void checkSettings2() {
		
			assertTrue(rb.checkSettings(playerlist));
		
	}

	@Test
	void checkPowerDownTest1() {
		playerlist.get(0).setPowerDownToken(true);
		playerlist.get(1).setPowerDownToken(false);

		assertEquals(false, rb.CheckPowerDown(playerlist.get(1)));
		assertTrue(rb.CheckPowerDown(playerlist.get(0)));

	}

	@Test
	void checkPowerDownTest2() {
		playerlist.get(2).setDamageTokens(0);
		assertEquals(false, rb.CheckPowerDown(playerlist.get(2)));
	}

	

	@Test
	void amountProgramcardsTest() {
		playerlist.get(0).setDamageTokens(6);
		playerlist.get(1).setDamageTokens(3);
		playerlist.get(2).setDamageTokens(7);
		playerlist.get(3).setDamageTokens(1);

		try {
			assertEquals(3, rb.amountProgramCards(playerlist.get(0)));
			assertEquals(6, rb.amountProgramCards(playerlist.get(1)));
			assertEquals(2, rb.amountProgramCards(playerlist.get(2)));
			assertEquals(8, rb.amountProgramCards(playerlist.get(3)));

		} catch (IllegalAmountOfDamageTokensException | DestructionException | AmountOfProgramCardsException e) {
			e.printStackTrace();
		}
	}

	// playerlist.get(0).setDamageTokens(3);
	//
	// assertThrows(AmountOfProgramCardsException.class,() ->
	// rb.amountProgramCards(playerlist.get(0)));

	/*
	 * @Test void checkAmountOfProgramCardsTest() {
	 * 
	 * }
	 */

	@Test
	void checkLifeTokensTest() {
		playerlist.get(0).setLifeTokens(3);
		assertEquals(3, rb.amountLifeTokens(playerlist.get(0)));
	}

	@Test // Player is on a NONE field
	void checkMovedByBoardElement() {
		try {
			assertArrayEquals(playerlist.get(0).getPos(),
					(int[]) rb.movedByBoardElement(playerlist.get(0), playerlist, gb).get(0).get(0));
			assertEquals((Direction) playerlist.get(0).getFaceDirection(),
					(Direction) rb.movedByBoardElement(playerlist.get(0), playerlist, gb).get(0).get(1));
			assertEquals(false, rb.movedByBoardElement(playerlist.get(0), playerlist, gb).get(0).get(2));
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IllegalPositionException e) {
			e.printStackTrace();
		} catch (OutsideBoardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test // Player is on a conveyer belt thats facing north
	void checkMovedByBoardElement2() {
		position[0] = 1;
		position[1] = 2;
		playerlist.get(1).setPos(1, 3);

		assertEquals(FieldType.CONVEYOR_BELT_FAST, gb.fieldTypeAt(1, 3, false));
		assertEquals(Direction.NORTH, gb.fieldDirectionAt(1, 3, false));
		try {
			assertArrayEquals(position,
					(int[]) rb.movedByBoardElement(playerlist.get(1), playerlist, gb).get(0).get(0));
		} catch (NullPointerException | IllegalPositionException | OutsideBoardException e) {
			e.printStackTrace();
		}

	}

	@Test // player collides with another player - not blocked by a wall
	void checkMovedByBoardElement3() {
		playerlist.get(0).setPos(3, 4);
		playerlist.get(1).setPos(2, 4);
		position[0] = 2;
		position[1] = 4;
		try {
			assertArrayEquals(position,
					(int[]) rb.movedByBoardElement(playerlist.get(0), playerlist, gb).get(0).get(0));
			assertEquals(Direction.NORTH, rb.movedByBoardElement(playerlist.get(0), playerlist, gb).get(0).get(1));
			assertEquals(true, rb.movedByBoardElement(playerlist.get(0), playerlist, gb).get(0).get(2));
		} catch (NullPointerException | IllegalPositionException | OutsideBoardException e) {
			e.printStackTrace();
		}

	}

	@Test // around the corner
	void checkMovedByBoardElement4() {
		playerlist.get(3).setPos(7, 4);
		position[0] = 7;
		position[1] = 3;
		// check for field type
		assertEquals(FieldType.CONVEYOR_BELT_ROT_RIGHT_FAST, gb.fieldTypeAt(7, 4, false));
		assertEquals(Direction.NORTH, gb.fieldDirectionAt(7, 4, false));

		try {
			assertArrayEquals(position,
					(int[]) rb.movedByBoardElement(playerlist.get(3), playerlist, gb).get(0).get(0));
		} catch (NullPointerException | IllegalPositionException e) {
			e.printStackTrace();
		} catch (OutsideBoardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void checkFlagTouchTest1() {
		playerlist.get(0).setPos(5, 4);
		assertEquals(FieldType.FLAG, gb.fieldTypeAt(5, 4, false));
		assertEquals(1, gb.fieldIndexAt(5, 4, false));

		try {
			assertTrue(rb.checkFlagTouch(playerlist.get(0), gb));
		} catch (NullPointerException | GameOverException e) {
			e.printStackTrace();
		}
	}

	@Test
	void checkFlagTouchTest2() {
		playerlist.get(2).setPos(1, 6);
		try {
			assertEquals(false, rb.checkFlagTouch(playerlist.get(2), gb));
		} catch (NullPointerException | GameOverException e) {
			e.printStackTrace();
		}
	}

	@Test
	void checkFlagTouch3() {
		playerlist.get(3).setLastTouchedFlag(2);
		playerlist.get(3).setPos(1, 6);
		assertThrows(GameOverException.class, () -> rb.checkFlagTouch(playerlist.get(3), gb));
	}

	@Test
	void damageByLaserTest1() {
		playerlist.get(1).setPos(5, 3);
		try {
			assertEquals(1, rb.damageByLaser(playerlist.get(1), gb));
		} catch (NullPointerException | OutsideBoardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertEquals(0, rb.damageByLaser(playerlist.get(2), gb));
		} catch (NullPointerException | OutsideBoardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void checkGameOverTest() {
		playerlist.get(3).setLastTouchedFlag(2);
		playerlist.get(3).setPos(1, 6);
		assertTrue(rb.checkGameOver(gb, playerlist));
	}

	// on a repair field but 0 damage tokens
	@Test
	void checkRepairTest() {
		playerlist.get(3).setPos(2, 3);
		try {
			assertEquals(0, rb.checkRepair(playerlist.get(3), 4, gb));
		} catch (NullPointerException | IllegalRegisterPhaseException | IllegalAmountOfDamageTokensException e) {
			e.printStackTrace();
		}
	}

	// on a repair one field with damage tokens
	@Test
	void checkRepairTest2() {
		playerlist.get(3).setPos(2, 3);
		playerlist.get(3).setDamageTokens(2);
		try {
			assertEquals(1, rb.checkRepair(playerlist.get(3), 4, gb));
		} catch (NullPointerException | IllegalRegisterPhaseException | IllegalAmountOfDamageTokensException e) {
			e.printStackTrace();
		}
	}

	// on a repair two field
	@Test
	void checkRepairTest3() {
		playerlist.get(2).setPos(3, 8);
		playerlist.get(2).setDamageTokens(3);
		playerlist.get(1).setPos(3, 8);
		playerlist.get(1).setDamageTokens(1);
		try {
			assertEquals(2, rb.checkRepair(playerlist.get(2), 4, gb));
			assertEquals(1, rb.checkRepair(playerlist.get(1), 4, gb));
		} catch (NullPointerException | IllegalRegisterPhaseException | IllegalAmountOfDamageTokensException e) {
			e.printStackTrace();
		}
	}

	

	@Test // move one - no collision - no walls
	void movementTest1() {
		// expected pos : 5,4
		ProgramCard a = new ProgramCard(10, CardType.MOVE_ONE);
		try {
			assertArrayEquals(new int[] { 5, 4 },
					(int[]) rb.checkMovement(playerlist.get(0), a, playerlist, gb).get(0).get(0));
		} catch (IllegalProgramCardException | OutsideBoardException e) {
			e.printStackTrace();
		}
	}

	@Test
	void movementTest2() {
		// expected pos : 5,4
		ProgramCard a = new ProgramCard(10, CardType.MOVE_TWO);
		try {
			assertArrayEquals(new int[] { 5, 4 },
					(int[]) rb.checkMovement(playerlist.get(0), a, playerlist, gb).get(0).get(0));
		} catch (IllegalProgramCardException | OutsideBoardException e) {
			e.printStackTrace();
		}
	}

	@Test
	void movementTest3() {
		// expected pos : 5,4
		ProgramCard a = new ProgramCard(10, CardType.MOVE_THREE);
		try {
			assertArrayEquals(new int[] { 5, 4 },
					(int[]) rb.checkMovement(playerlist.get(0), a, playerlist, gb).get(0).get(0));
		} catch (IllegalProgramCardException | OutsideBoardException e) {
			e.printStackTrace();
		}
	}

	@Test
	// push another player off the field
	void movementTest4() {
		ProgramCard a = new ProgramCard(10, CardType.MOVE_ONE);
		playerlist.get(0).setPos(5, 1);
		playerlist.get(1).setPos(5, 0);
		try {
			assertArrayEquals(new int[] { 5, -1 },
					(int[]) rb.checkMovement(playerlist.get(0), a, playerlist, gb).get(1).get(1));
		} catch (IllegalProgramCardException | OutsideBoardException e) {
			e.printStackTrace();
		}
	}

	@Test
	// Player moves off board by himself
	void movementTest5() {
		ProgramCard b = new ProgramCard(10, CardType.MOVE_ONE);
		playerlist.get(1).setPos(5, 0);
		try {
			assertArrayEquals(new int[] { 5, -1 },
					(int[]) rb.checkMovement(playerlist.get(1), b, playerlist, gb).get(0).get(0));
		} catch (IllegalProgramCardException | OutsideBoardException e) {
			e.printStackTrace();
		}
	}
	
}
