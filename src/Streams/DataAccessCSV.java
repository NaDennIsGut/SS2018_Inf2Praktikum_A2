package Streams;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import Backend.GameMaster;
import Interface.iDataAccess;
/**
 * 
 * @author Leila Taraman
 *
 */

public class DataAccessCSV implements iDataAccess{
	
	/**
	 * Method for saving the game
	 */
	@Override
	public void save(Object object, String path) throws FileNotFoundException, IOException {
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(new FileWriter(path));
			GameMaster gm = (GameMaster) object;
			printWriter.println(gm.createCSVString());
			printWriter.flush();
			
		} catch(IOException e) {
			e.printStackTrace();
		}finally {
			printWriter.close();
		}
		
	}
	
	/**
	 * Method for loading the game
	 */
	@Override
	public Object load(String path) throws FileNotFoundException, IOException {
		String line = null;
		StringBuffer stringBuffer = null;
		BufferedReader bufferedReader = null;
		try {
			 stringBuffer = new StringBuffer();
			 bufferedReader = new BufferedReader(new FileReader(path));
			line = bufferedReader.readLine();
			while(line != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");
				line = bufferedReader.readLine();
			} 

			return stringBuffer.toString();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
			
		} catch(IOException e) {
			e.printStackTrace();
			
		} finally {
			
				bufferedReader.close();
			
		}
		return stringBuffer;
	}

}
