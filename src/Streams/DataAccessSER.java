package Streams;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Interface.iDataAccess;
/**
 * 
 * @author Leila Taraman
 *
 */
public class DataAccessSER implements iDataAccess{
	
	/**
	 * Method for saving the game
	 */
	@Override
	public void save(Object object, String path) throws FileNotFoundException, IOException {
		ObjectOutputStream objectoutputStream = null;
		try {
			objectoutputStream = new ObjectOutputStream(new FileOutputStream(path));
			objectoutputStream.writeObject(object);
		} catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				objectoutputStream.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * Method for loading the game
	 */
	@Override
	public Object load(String path) throws FileNotFoundException, IOException {
		ObjectInputStream objectinputStream = null;
		try {
			File file = new File(path);
			objectinputStream = new ObjectInputStream(new FileInputStream(file));
			return objectinputStream.readObject();
		}
		catch(IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(objectinputStream != null) {
				try {
					objectinputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return objectinputStream;
	}

}
