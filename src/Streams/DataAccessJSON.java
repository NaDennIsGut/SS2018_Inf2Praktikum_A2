package Streams;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;

import Interface.iDataAccess;
/**
 * 
 * @author Leila Taraman
 *
 */

public class DataAccessJSON implements iDataAccess{
	
	/**
	 * Method for saving the game
	 */
	@Override
	public void save(Object object, String path) throws FileNotFoundException, IOException {
		JsonWriter jsonWriter = null;
		try {
			jsonWriter = Json.createWriter(new FileOutputStream(path));
			jsonWriter.write(((JsonArrayBuilder)object).build());
			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			jsonWriter.close();
		}
		
	}
	
	/**
	 * Method for loading the game
	 */
	@Override
	public Object load(String path) throws FileNotFoundException, IOException {
		
		JsonArray jsonArray = null;
		try {
			jsonArray = Json.createReader(new FileInputStream(path)).readArray();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		return jsonArray;
		
	}

}
